(function() { var req = {"global":true,"csrfToken":"0G8MPIWk-GMgOb5Dd-OFFMuFHOHQXaHWoGkA"};
if(window._backend_) { angular.module("backend").factory("global",["context",function(context){
  var own={}.hasOwnProperty,key;
  for (key in req) if (own.call(req, key)) context[key] = req[key];
  return req;
}]); }else{
  angular.module("backend",[]).factory("global",[function(){return req;}]);
}})()