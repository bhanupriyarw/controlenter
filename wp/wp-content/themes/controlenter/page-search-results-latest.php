<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Insights | Control Enter</title>
    </head>

    <body>
        <?php get_header(); ?>
        
        <?php 
            $query = $_GET['query'];
            $query = str_replace("%20"," ",$query);
        ?>
        <div class="body insights-page search-results-page">
            <div class="container">
                <div class="static-nav">
                    <div class="container">
                        <div class="search-box">
                            <div class="form-field">
                                <input type="text" name="query" id="search_query" class="input-field" value="" placeholder="Search" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="insights">
                    <div class="heading">You are browsing: <?php echo $query; ?></div>
                    <div class="back-button links"><a href="<?php echo bloginfo( "url" ) ?>/insights/">Back</a></div>
                    <div class="clearfix"></div>
                    <?php 
                    $i = 1;
//                    if ($insights_posts = $wpdb->get_results(("SELECT * FROM wp_posts t1 where t1.post_type = 'post' and t1.post_title like '%$query%')" AND ("SELECT * FROM wp_terms t2  where t2.name like '%$query%')" )){
//                    echo '<pre>' . print_r($insights_posts, true) . '</pre>'; die;
//                    if ($insights_posts = $wpdb->get_results( "SELECT * FROM (" . $table_prefix . "posts where post_type = 'post' and ((post_title like '%$query%') or (post_content like '%$query%'))) or (" . $table_prefix . "terms where (name like '%$query%') or (slug like '%$query%'))"  )) {
//                    if ($insights_posts = $wpdb->get_results( "SELECT * FROM wp_posts  where post_type = 'post' and post_title like '%$query%'")) {
                    if ($insights_posts = $wpdb->get_results( "SELECT * 
  FROM wp_posts
 INNER JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
 INNER JOIN wp_terms ON wp_term_relationships.term_taxonomy_id = wp_terms.term_id
 where (post_title like %$query%) or (name like %$query%)")) {
                        foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                    <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($insights_post->ID), 'full'); ?>
                    <div class="each-insight-container active <?php foreach (get_the_tags($insights_post->ID) as $tag) { echo "$tag->slug"." "; } ?> <?php foreach (get_the_category($insights_post->ID) as $category) { echo "$category->slug"." "; } ?> <?php echo $author_slug; ?>">
                        <div onclick="window.location='<?php echo get_the_permalink($insights_post->ID); ?>'" class="each-insight <?php echo $i; ?>" style="background-image: url('<?php echo $featured_image_url[0]; ?>');">
                            <div class="display-table">
                                <div class="vertical-align middle">
                                    <div class="content">
                                        <div class="text-container">
                                            <div class="heading"><a href="<?php echo get_the_permalink($insights_post->ID); ?>"><?php echo $insights_post->post_title; ?></a></div>
                                            <div class="insights-excerpt">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                            <div class="read-more">
                                                <a href="<?php echo get_the_permalink($insights_post->ID); ?>">Read More <img src="<?php echo bloginfo("template_directory") ?>/img/insights/read-more.png" alt="Read More Icon" /></a>
                                            </div>
                                        </div>
                                        <div class="text">
                                            <div class="tags">
                                                <?php
                                                $posttags = get_the_tags($insights_post->ID);
                                                if ($posttags) {
                                                  foreach($posttags as $tag) {
                                                    echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>";
                                                  }
                                                }
                                                
//                                                foreach (get_tags($insights_post->ID) as $tag)
//                                                {
//                                                   echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>"; 
//                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="author-and-date">
                                                <div class="author-name"><a href="javascript: void(0);"><?php the_author(); ?></a></div>
                                                <div class="insights-date">
                                                    <?php $post_date = $insights_post->post_date; ?>
                                                    <?php echo date("d.m.Y", strtotime("$post_date")); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay"></div>
                        </div>
                    </div>    
                        
                    <!--<div class="divider"></div>-->
                    <?php  $i++; } wp_reset_postdata(); } else { ?>
                        <div class="not-found">
                            <h3>Nothing Found</h3>
                            <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
</html>