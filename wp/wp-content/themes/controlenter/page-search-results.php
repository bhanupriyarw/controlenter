<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Insights | Control Enter</title>
    </head>

    <body>
        <?php get_header(); ?>
        
        <?php 
            $query = $_GET['query'];
            $query = str_replace("%20","",$query);
        ?>
        <div class="body insights-page search-results-page">
            <div class="container">
                <div class="static-nav">
                    <div class="container">
                        <div class="search-box">
                            <div class="form-field">
                                <input type="text" name="query" id="search_query" class="input-field" value="<?php echo $query; ?>" placeholder="Search" />
                            </div>
                            <div class="back-button links"><a href="<?php echo bloginfo( "url" ) ?>/insights/">Back</a></div>
                        </div>
                    </div>
                </div>
                <div class="insights">
                    <div class="heading browsing">You are browsing: <?php echo $query; ?></div>
                    <div class="clearfix"></div>
                    <?php 
                    $i = 1;
                    if ($insights_posts = $wpdb->get_results( "select $table_prefix" ."posts.id, $table_prefix" ."posts.post_title, $table_prefix" . "posts.post_content, $table_prefix" . "posts.post_excerpt, $table_prefix" . "posts.post_name, $table_prefix" . "posts.guid, $table_prefix" . "posts.post_date, $table_prefix" . "posts.post_author from $table_prefix" . "posts left join $table_prefix" . "term_relationships on $table_prefix" . "posts.id=object_id left join $table_prefix" . "terms on term_taxonomy_id = term_id left join $table_prefix" . "users on $table_prefix" . "users.id=post_author where post_type='post' and ( INSTR(" . $table_prefix . "posts.post_title, '$query') > 0 or INSTR(" . $table_prefix . "terms.name, '$query') > 0 or INSTR(" . $table_prefix . "users.display_name, '$query') > 0 ) group by $table_prefix" . "posts.id")) {
//                    echo '<pre>' . print_r($insights_posts, true) . '</pre>'; die;
//                    if ($insights_posts = $wpdb->get_results( "SELECT * FROM (" . $table_prefix . "posts where post_type = 'post' and ((post_title like '%$query%') or (post_content like '%$query%'))) or (" . $table_prefix . "terms where (name like '%$query%') or (slug like '%$query%'))"  )) {
                    foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                    <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($insights_post->id), 'full'); ?>
                    <div class="each-insight-container active <?php foreach (get_the_tags($insights_post->id) as $tag) { echo "$tag->slug"." "; } ?> <?php foreach (get_the_category($insights_post->id) as $category) { echo "$category->slug"." "; } ?> <?php echo $author_slug; ?>">
                        <div onclick="window.location='<?php echo get_the_permalink($insights_post->id); ?>'" class="each-insight" style="background-image: url('<?php echo $featured_image_url[0]; ?>');">
                            <div class="display-table">
                                <div class="vertical-align middle">
                                    <div class="content">
                                        <div class="text-container">
                                            <div class="heading"><a href="<?php echo get_the_permalink($insights_post->id); ?>"><?php echo $insights_post->post_title; ?></a></div>
                                            <div class="industry-name">
                                                Industries:
                                                <?php
                                                foreach (get_the_category($insights_post->id) as $category)
                                                {
                                                    if($category->cat_name !== 'Uncategorized'){
                                                        echo "<a href='".get_category_link($category->term_id)."'>".$category->cat_name."</a>";
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="insights-excerpt">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                            <div class="read-more">
                                                <a href="<?php echo get_the_permalink($insights_post->id); ?>">Read More <img src="<?php echo bloginfo("template_directory") ?>/img/insights/read-more.png" alt="Read More Icon" /></a>
                                            </div>
                                        </div>
                                        <div class="text">
                                            <div class="tags">
                                                <?php
                                                foreach (get_the_tags($insights_post->id) as $tag)
                                                {
                                                    echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>";
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="author-and-date">
                                                <div class="author-name"><a href="<?php echo get_the_permalink($insights_post->id); ?>"><?php the_author(); ?></a></div>
                                                <div class="insights-date">
                                                    <?php $post_date = $insights_post->post_date; ?>
                                                    <?php echo date("d.m.Y", strtotime("$post_date")); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="share-button">
                                            <img src="<?php echo bloginfo("template_directory") ?>/img/insights/share.png" alt="Book Mark Icon" /> Share
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay"></div>
                        </div>
                        
                        <!--<div class="divider"></div>-->
                    </div>   
                        
                    <!--<div class="divider"></div>-->
                    <?php  $i++; }  wp_reset_postdata(); } else { ?>
                        <div class="not-found">
                            <h3>Nothing Found</h3>
                            <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
</html>