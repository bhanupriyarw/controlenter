<?php $this_page = 'insights'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Insights | Control Enter</title>
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <?php
        $args = array(
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'post',
            'posts_per_page' => -1
        );
        $insights_posts = get_posts( $args );
//        echo '<pre>' . print_r($insights_posts, true) . '</pre>'; die;
        ?>
        <div class="body insights-page">
            <div class="container">
                <div class="static-nav">
                    <div class="subscribe">
                        <div class="form">
                            <div class="form-field">
                                <input type="email" name="subscriber_email" id="subscriber_email" class="input-field" value="" placeholder="Enter your email to stay in touch on our latest thinking!" />
                                <!--<input class="visible-xs" type="email" name="subscriber_email" id="subscriber_email" class="input-field" value="" placeholder="Enter your email !" />-->
                                <span class="subscriber-email-error-msg error-msg" style="display: none;">Enter your Email Id</span>
                                <span class="subscriber-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid Email Id (Ex: example@example.com)</span>
                            </div>
                            <div class="submit">

                                <div class="links">
                                    <a href="javascript: void(0);" class="subscribe-button" id="subscribe_button">Subscribe</a>
                                </div>
                                <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="success" id="success_subscriber"></div>
                        </div>
                    </div>
                    <div class="search-box">
                        <div class="form-field">
<!--                            <div class="search-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23">
                                    <defs>
                                      <style>
                                        .cls-1 {
                                          fill: #1f3863;
                                          fill-rule: evenodd;
                                        }
                                      </style>
                                    </defs>
                                    <path id="search.svg" class="cls-1" d="M8620,2200.7l-6.12-6.11a9.514,9.514,0,0,0,2.14-6.03v-0.06a9.5,9.5,0,1,0-3.43,7.4l6.11,6.1Zm-13.53-4.44a7.705,7.705,0,1,1,7.72-7.7v0.04A7.7,7.7,0,0,1,8606.47,2196.26Z" transform="translate(-8597 -2179)"/>
                                </svg>
                            </div>-->
                            <input type="text" name="query" id="search_query" class="input-field" value="" placeholder="Search" />
                        </div>
                    </div>
                    <div class="filters-button">
                        <div class="display-table">
                            <div class="vertical-align middle">
                                <div class="filter-icon filters-open-button">
                            <span>Filters</span>
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="30" height="30" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 32 36" style="enable-background:new 0 0 32 36;" xml:space="preserve">
                           <style type="text/css">
                                   .st0{fill:#FFFFFF;}
                           </style>
                           <path id="FiltersorTags.svg" class="st0" d="M30.5,16.5h-2.5c-0.8-3.2-4.1-5.2-7.4-4.3c-2.1,0.6-3.8,2.2-4.3,4.3H1.5
                                   C0.7,16.5,0,17.2,0,18c0,0.8,0.7,1.5,1.5,1.5c0,0,0,0,0,0h14.7c0.8,3.2,4.1,5.2,7.3,4.4c2.1-0.5,3.8-2.2,4.4-4.4h2.5
                                   c0.8,0,1.5-0.6,1.5-1.4c0,0,0,0,0,0C32,17.2,31.3,16.5,30.5,16.5z M22.1,21.6c-2,0-3.6-1.6-3.6-3.6c0-2,1.6-3.6,3.6-3.6
                                   c2,0,3.6,1.6,3.6,3.6C25.6,20,24,21.6,22.1,21.6z M1.5,7.5h2.7c0.8,3.2,4.1,5.2,7.4,4.3c2.1-0.6,3.8-2.2,4.3-4.3h14.6
                                   C31.3,7.5,32,6.9,32,6c0-0.8-0.7-1.5-1.5-1.5c0,0,0,0,0,0H15.9C15.2,1.9,12.8,0,10,0C7.3,0,4.9,1.9,4.2,4.5H1.5C0.7,4.5,0.1,5.2,0,6
                                   C0,6,0,6,0,6C0,6.9,0.7,7.5,1.5,7.5C1.5,7.5,1.5,7.5,1.5,7.5z M10,2.4c2,0,3.6,1.6,3.5,3.6S11.9,9.6,10,9.6C8,9.6,6.4,8,6.4,6
                                   C6.4,4,8,2.4,10,2.4z M30.5,28.5H15.9c-0.8-3.2-4.1-5.2-7.4-4.3c-2.1,0.6-3.8,2.2-4.3,4.3H1.5C0.7,28.5,0,29.1,0,30
                                   c0,0.8,0.7,1.5,1.5,1.5c0,0,0,0,0,0h2.7c0.8,3.2,4.1,5.2,7.4,4.3c2.1-0.6,3.8-2.2,4.3-4.3h14.6c0.8,0,1.5-0.6,1.5-1.4c0,0,0,0,0-0.1
                                   C32,29.1,31.3,28.5,30.5,28.5C30.5,28.5,30.5,28.5,30.5,28.5z M10,33.6c-2,0-3.6-1.6-3.5-3.6s1.6-3.6,3.6-3.5c2,0,3.5,1.6,3.5,3.6
                                   C13.6,32,12,33.6,10,33.6z"/>
                           </svg>
                        </div>
                        <div class="close-button filters-close-button">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 42.3 42.3" style="enable-background:new 0 0 42.3 42.3;" xml:space="preserve">
                            <style type="text/css">
                                   .st0{fill:#FFFFFF;}
                            </style>
                            <polygon class="st0" points="22.6,20.9 39.4,4.1 38.2,2.9 21.4,19.7 4.4,2.6 3.2,3.8 20.3,20.9 2.6,38.5 3.8,39.7 21.5,22.1 
                                   38.5,39.2 39.7,38 "/>
                            </svg>
                        </div>
                            </div>
                        </div>
                        <!--<img class="filters-open-button" src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/tags-options.png" alt="Filter Icon" />-->
                        <!--<img class="filters-close-button" src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/tags-options-close.png" alt="Close Icon" />-->
                    </div>
                    <div class="clearfix"></div>
                </div>
                
                <div class="filters-section">
                    <div class="container">
                        <div class="filter-container">
                            <div class="content">
                                <div class="text">
                                    Select your areas of interest and we will show you content tailored your interests.
                                </div>
                                <div class="hidden-xs clear-all links">
                                    <a href="javascript: void(0);">Clear All</a>
                                </div>
                                <div class="hidden-xs go-button links orange">
                                    <a href="javascript: voic(0);">Go</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="tabs">
                                <div class="each-tab subject-areas active" onclick="showFilters('subject-areas-list', 'subject-areas');">Subject Areas <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-down.png" alt="Arrow Icon" /></div>
                                <div class="each-tab industries" onclick="showFilters('industries-list', 'industries');">Industries <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-right.png" alt="Arrow Icon" /></div>
                                <div class="each-tab authors" onclick="showFilters('authors-list', 'authors');">Authors <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-right.png" alt="Arrow Icon" /></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="each-list subject-areas-list">
                                <ul>
                                <?php
                                foreach (get_tags() as $tag)
                                    { ?>
                                    <li class="<?php echo $tag->slug; ?>" onclick="showFiltersResults('<?php echo $tag->slug; ?>')"><?php echo $tag->name; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                                <?php } ?>
                                    <li class="clearfix"></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="each-list industries-list">
                                <ul>
                                    <?php
                                    foreach (get_categories(array('hide_empty' => false, 'exclude' => 1)) as $category)
                                    { ?>
                                    <li class="<?php echo $category->slug; ?>" onclick="showFiltersResults('<?php echo $category->slug; ?>')"><?php echo $category->cat_name; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                                    <?php } ?>
                                    <li class="clearfix"></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="each-list authors-list">
    <!--                            wp_list_authors('exclude_admin=0');-->
    <!--                            <ul>
                                    <?php foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                                    <?php $author_slug = get_the_author();
                                    $author_slug = strtolower($author_slug);
                                    $author_slug = str_replace(" ","-",$author_slug);
                                    ?>
                                    <li class="<?php echo $author_slug; ?>" onclick="showFiltersResults('<?php echo $author_slug; ?>')"><?php echo get_the_author(); ?></li>
                                    <?php } wp_reset_postdata(); ?>
                                    <li class="clearfix"></li>
                                </ul>-->
                                <?php $authors = get_users();
    //                             echo '<pre>' . print_r($authors, true) . '</pre>';;
                                foreach($authors as $author) { 
                                    $author_name = $author->display_name;
                                    $author_name = strtolower($author_name);
                                    $author_name = str_replace(" ","-",$author_name);
                                    ?>
                                    <li class="<?php echo $author_name; ?>" onclick="showFiltersResults('<?php echo $author_name; ?>');"><?php echo $author->nickname; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                                <?php } ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="filter-buttons visible-xs">
                        <div class="clear-all links">
                            <a href="javascript: void(0);">Clear All</a>
                        </div>
                        <div class="go-button links orange">
                            <a href="javascript: voic(0);">Go</a>
                        </div>
                    </div>
                </div>
                
                <div class="insights">
                    <?php foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                    <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($insights_post->ID), 'full'); ?>
                    <?php 
                        $author_slug = get_the_author();
                        $author_slug = strtolower($author_slug);
                        $author_slug = str_replace(" ","-",$author_slug);
                    ?>
                    <div class="each-insight-container <?php foreach (get_the_tags($insights_post->ID) as $tag) { echo "$tag->slug"." "; } ?> <?php foreach (get_the_category($insights_post->ID) as $category) { echo "$category->slug"." "; } ?> <?php echo $author_slug; ?>">
                        <div onclick="window.location='<?php echo get_the_permalink($insights_post->ID); ?>'" class="each-insight" style="background-image: url('<?php echo $featured_image_url[0]; ?>');">
                            <div class="display-table">
                                <div class="vertical-align middle">
                                    <div class="content">
                                        <div class="text-container">
                                            <div class="heading"><a href="<?php echo get_the_permalink($insights_post->ID); ?>"><?php echo $insights_post->post_title; ?></a></div>
                                            <div class="industry-name">
                                                Industries:
                                                <?php
                                                foreach (get_the_category($insights_post->ID) as $category)
                                                {
                                                    if($category->cat_name !== 'Uncategorized'){
                                                        echo "<a href='".get_category_link($category->term_id)."'>".$category->cat_name."</a>";
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="insights-excerpt">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                            <div class="read-more">
                                                <a href="<?php echo get_the_permalink($insights_post->ID); ?>">Read More <img src="<?php echo bloginfo("template_directory") ?>/img/insights/read-more.png" alt="Read More Icon" /></a>
                                            </div>
                                        </div>
                                        <div class="text">
                                            <div class="tags">
                                                <?php
                                                foreach (get_the_tags($insights_post->ID) as $tag)
                                                {
                                                    echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>";
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="author-and-date">
                                                <div class="author-name"><a href="<?php echo get_the_permalink($insights_post->ID); ?>"><?php the_author(); ?></a></div>
                                                <div class="insights-date">
                                                    <?php $post_date = $insights_post->post_date; ?>
                                                    <?php echo date("d.m.Y", strtotime("$post_date")); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay"></div>
                        </div>
                        
                        <!--<div class="divider"></div>-->
                    </div>
                        
                    <?php } wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
//            function showFilters(showList, filterButton) {
//                $('.each-list').hide();
//                $('.' + showList).show();
//                $('.each-tab').removeClass("active");
//                $('.' + filterButton).addClass("active");
//                $('.each-tab img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-right.png");
//                $('.each-tab.active img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-down.png");
//            } 
//            function showFiltersResults(keyWord) {
//                $('.each-insight-container').addClass("inactive");
//                $('.each-insight-container.' + keyWord).fadeIn();
//                $('.each-insight-container.' + keyWord).addClass("active");
//                $('.' + keyWord).addClass("active");
//            }
            $(document).ready(function() {
//                $('.filters-open-button').click(function(){
//                    $('.filters-section').show();
//                    $('.filters-open-button').hide();
//                    $('.filters-close-button').show();
//                });
//                $('.filters-close-button').click(function(){
//                    $('.filters-section').hide();
//                    $('.filters-open-button').show();
//                    $('.filters-close-button').hide();
////                    $('.each-tab').removeClass("active");
////                    $('.each-tab.subject-areas').addClass("active");
////                    $('.each-tab img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-right.png");
//                    $('.each-tab.subject-areas img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-down.png");
////                    $('.each-list').css("display", "none");
////                    $('.each-list.subject-areas-list').css("display", "block");
//                });
//                $('.clear-all').click(function() {
//                    $('.each-insight-container').removeClass("inactive");
//                    $('.each-insight-container').fadeIn();
//                    $('.each-insight-container').removeClass("active");
//                    $('.each-list li').removeClass("active");
//                });
                $(".share-button").hover(function() {
                    $(".share-container").addClass("animated fadeIn");
                    $(".share-button").css("display", "none");
                }, function() {
                    $(".share-container").removeClass("animated fadeIn");
                    $(".share-button").css("display", "block");
                });
                
                if($(window).width() < 768) {
                    $('#subscriber_email').attr("placeholder", "Enter your email !");
                    $('.filters-section').height($(window).height() - 90);
                }
            });
        </script>
    </body>
</html>