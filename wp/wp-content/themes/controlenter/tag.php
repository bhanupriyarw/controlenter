<?php $this_page = 'insights'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Insights | Control Enter</title>
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <?php
        $tag_name = single_tag_title( "", false );
        $tag_name = strtolower($tag_name);
        $tag_name = str_replace(" ","-",$tag_name);
        $args = array(
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_id' => $post->ID,
            'tag' => $tag_name
        );
        $insights_posts = get_posts( $args );
//        echo '<pre>' . print_r($insights_posts, true) . '</pre>'; die;
        ?>
        <div class="body insights-page tags-page">
            <div class="container">
                <div class="insights">
                    <div class="heading browsing"><?php single_tag_title("You are browsing: ","true"); ?></div>
                    <div class="back-button links"><a href="<?php echo bloginfo( "url" ) ?>/insights/">Back</a></div>
                    <div class="clearfix"></div>
                    <?php foreach ($insights_posts as $insights_post) { setup_postdata($blog_post); ?>
                    <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($insights_post->ID), 'full'); ?>
                    <div class="each-insight-container active <?php foreach (get_the_tags($insights_post->ID) as $tag) { echo "$tag->slug"." "; } ?> <?php foreach (get_the_category($insights_post->ID) as $category) { echo "$category->slug"." "; } ?> <?php echo $author_slug; ?>">
                        <div onclick="window.location='<?php echo get_the_permalink(); ?>'" class="each-insight" style="background-image: url('<?php echo $featured_image_url[0]; ?>');">
                            <div class="display-table">
                                <div class="vertical-align middle">
                                    <div class="content">
                                        <div class="text-container">
                                            <div class="heading"><a href="<?php echo get_the_permalink($insights_post->ID); ?>"><?php echo $insights_post->post_title; ?></a></div>
                                            <div class="industry-name">
                                                Industries:
                                                <?php
                                                foreach (get_the_category($insights_post->ID) as $category)
                                                {
                                                    if($category->cat_name !== 'Uncategorized'){
                                                        echo "<a href='".get_category_link($category->term_id)."'>".$category->cat_name."</a>";
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="insights-excerpt">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                            <div class="read-more">
                                                <a href="<?php echo get_the_permalink($insights_post->ID); ?>">Read More <img src="<?php echo bloginfo("template_directory") ?>/img/insights/read-more.png" alt="Read More Icon" /></a>
                                            </div>
                                        </div>
                                        <div class="text">
                                            <div class="tags">
                                                <?php
                                                foreach (get_the_tags($insights_post->ID) as $tag)
                                                {
                                                    echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>";
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="author-and-date">
                                                <div class="author-name"><a href="javascript: void(0);"><?php the_author(); ?></a></div>
                                                <div class="insights-date">
                                                    <?php $post_date = $insights_post->post_date; ?>
                                                    <?php echo date("d.m.Y", strtotime("$post_date")); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay"></div>
                        </div>
                    </div>
                        
                    <!--<div class="divider"></div>-->
                    <?php } wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
</html>