<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="icon" type="image/png" sizes="60x60" href="<?php echo bloginfo( "template_directory" ); ?>/img/favicon.png" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo( "template_directory" ); ?>/css/custom-styles.css" />
<link rel="stylesheet" type="text/css" href="<?php echo bloginfo( "template_directory" ); ?>/css/animate.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />