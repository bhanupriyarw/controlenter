<?php $this_page = 'contact-us'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Contact | Control Enter</title>
        
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtaSazt2SCvMvF_ByrPjjYtFbRQ4RLA3o"></script>
        
        
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <?php
            $success = "";
            //      echo '<pre>' . var_dump($_POST). '</pre>'; die;
            if (!empty($_POST)) {
                $submission_message = '';
                if ($_POST['submit_file']) {
//                    if(bloginfo("url") == "http://www.tiramisumedia.com/testbed/controlenter/wp/") {
//                        $file_directory = '/home/tiramisunewmedia/tiramisumedia.com/testbed/controlenter/wp/wp-content/themes/controlenter/resumes';
//                    } else if(bloginfo("url") == "http://localhost/controlenter/wp/") {
//                        $file_directory = '/home/tiramisunewmedia/tiramisumedia.com/testbed/controlenter/wp/wp-content/themes/controlenter/resumes';
//                    }
                    $file_directory = 'C:\wamp64\www\controlenter\wp\wp-content\themes\controlenter\resumes';
                    $file_path = $file_directory . '/' . basename($_FILES['resume']['name']);
                    $file_path_url = $file_directory.'/resumes/' .  basename($_FILES['resume']['name']);
                    
                    $join_us_name = $_POST['join_us_name'];
                    $join_us_email = $_POST['join_us_email'];
                    $join_us_subject = $_POST['join_us_subject'];
                    $join_us_link = $_POST['join_us_link'];
                    $join_us_message = $_POST['join_us_message'];
                    if (!file_exists($file_directory)) {
                        if (!mkdir($file_directory, 0777, true)) {
                            $submission_message .= 'There was some error while creating folder. Kindly try again after some time.<br />';
                        }
                    }

                    if ($_FILES['resume']['error'] > 0) {
                        $submission_message .= 'There was some error while uploading the file. Kindly try again after some time.<br />';
                    } else if ($_FILES['resume']['size'] > 5000000) {
                        $submission_message .= 'The uploaded file size exceeds the limit. Kindly reduce the file size and try again or use some other file.<br />';
                    }
                    else {
                        if (move_uploaded_file($_FILES['resume']['tmp_name'], $file_path)) {
                            $success = "true";
                        } else {
                            $submission_message .= 'There was some error while uploading the file. Kindly try again after some time.<br />';
                        }
                    }
                }
            }
            ?>
        
        <div class="body contact-page">
            <div class="address-map-container">
                <div class="address-container">
                    <div class="display-table">
                        <div class="vertical-align middle">
                            <div class="each-address">
                                <div class="address">
                                    <div class="icon ireland" id="map_ireland_button" onclick="showMap('ireland', 'ireland-map-container');">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/map-icon-small.png" alt="Map Icon" />
                                        <b>Ireland (Gobal HQ)</b>
                                        <div class="see-map">see map</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="text">
                                        12 Fitzharris House,<br/>
                                        James Joyce Street, Dublin 1
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="phone-number">
                                    <a href="tel://35312549187"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+353 1 254 9187</a>
                                    <a class="email" href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                </div>
                            </div>
                            <div class="each-address">
                                <div class="address">
                                    <div class="icon india" id="map_india_button" onclick="showMap('india', 'india-map-container');">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/map-icon-small.png" alt="Map Icon" />
                                        <b>India (Asia Services)</b>
                                        <div class="see-map">see map</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="text">
                                        210 Beneka Towers, 16D Main HAL II Stage, Kodihalli,<br/>
                                        Indiranagar, Bangalore, Karnataka 560008
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="phone-number">
                                    <a href="tel://917760004444"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+91 776 000 4444</a>
                                    <a class="email" href="mailto:suresh@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />suresh@controlentergroup.com</a>
                                </div>
                            </div>
                            <div class="each-address">
                                <div class="address">
                                    <div class="icon america" id="map_america_button" onclick="showMap('america', 'america-map-container');">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/map-icon-small.png" alt="Map Icon" />
                                        <b>United States (Americas Services)</b>
                                        <div class="see-map">see map</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="text">
                                        2711  Centerville Road, Suite 400<br/>
                                        Wilmington, Delaware 19808
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="phone-number">
                                    <a href="tel://16173987978"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+1 617 398 7978</a>
                                    <a class="email" href="mailto:info@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />info@controlentergroup.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="map-container">
                    <div class="map ireland-map-container">
                        <div id="map_ireland" class="each-map"></div>
                        <!--<div class="layer"  onclick="window.open('https://goo.gl/maps/4NUAHXQP9my', '_blank');"></div>-->
                        <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.1590900413107!2d77.64434581430456!3d12.96166989086238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14041ae4539b%3A0x64a6b0a9386f8e97!2sKodihalli+Main+Rd%2C+HAL+2nd+Stage%2C+Kodihalli%2C+Bengaluru%2C+Karnataka+560008!5e0!3m2!1sen!2sin!4v1495387982268" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>-->
                    </div>
                    <div class="map india-map-container">
                        <div id="map_india" class="each-map"></div>
<!--                        <div class="layer"  onclick="window.open('https://goo.gl/maps/4NUAHXQP9my', '_blank');"></div>-->
<!--                        <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.1590900413107!2d77.64434581430456!3d12.96166989086238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14041ae4539b%3A0x64a6b0a9386f8e97!2sKodihalli+Main+Rd%2C+HAL+2nd+Stage%2C+Kodihalli%2C+Bengaluru%2C+Karnataka+560008!5e0!3m2!1sen!2sin!4v1495387982268" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>-->
                    </div>
                    <div class="map america-map-container">
                        <div id="map_america" class="each-map"></div>
<!--                        <div class="layer" onclick="window.open('https://goo.gl/maps/4NUAHXQP9my', '_blank');"></div>-->
<!--                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.1590900413107!2d77.64434581430456!3d12.96166989086238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14041ae4539b%3A0x64a6b0a9386f8e97!2sKodihalli+Main+Rd%2C+HAL+2nd+Stage%2C+Kodihalli%2C+Bengaluru%2C+Karnataka+560008!5e0!3m2!1sen!2sin!4v1495387982268" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>-->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-container">
                <div class="container">
                    <div class="text talk-to-us-text">
                        If you are looking to enhance your current strategies and make your business future ready, or if you just have a quick question, please fill in the form below and we will contact you shortly.
                    </div>
                    <div class="text join-us-text">
                        We are always looking for exceptional new talent! If you want to apply for a position at Control Enter, please fill in the form below with a link to your Linkedin profile, and we will respond to you soon.
                    </div>
                    <div class="all-forms">
                        <div class="form-tabs">
                            <div class="links talk-to-us-button active" onclick="showForm('talk-to-us-button', 'talk-to-us', 'talk-to-us-text');">
                                <a href="javascript: void(0);">Talk to us</a>
                            </div>
<!--                            <div class="links work-with-us-button" onclick="showForm('work-with-us-button', 'talk-to-us');">
                                <a href="javascript: void(0);">Work with us</a>
                            </div>-->
                            <div class="links join-us-button" onclick="showForm('join-us-button', 'join-us', 'join-us-text');">
                                <a href="javascript: void(0);">Join Us</a>
                            </div>
                            
                            <div class="follow-us">
                                <div class="heading">Follow us</div>
                                <div class="social-links">
                                    <a href="javascript: void(0);"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/linkedin.png" alt="Linkedin Icon" /></a>
                                </div>
                            </div>
                        </div>
                        <div class="form talk-to-us" style="display: block;">
                            <div class="form-field half-width">
                                <input type="text" name="your_name" id="your_name" class="input-field" value="" placeholder="Name" />
                                <span class="enquiry-name-error-msg error-msg" style="display: none;">Enter your Name</span>
                            </div>
                            <div class="form-field half-width">
                                <input type="email" name="your_email" id="your_email" class="input-field" value="" placeholder="Email" />
                                <span class="enquiry-email-error-msg error-msg" style="display: none;">Enter your Email Id</span>
                                <span class="enquiry-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid Email Id (Ex: example@example.com)</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-field">
                                <input type="text" name="your_subject" id="your_subject" class="input-field" value="" placeholder="Subject" />
                            </div>
                            <div class="form-field">
                                <textarea name="your_message" rows="15" id="your_message" class="form-field message" value="" placeholder="Message"></textarea>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="success" id="enquiry_success"></div>
                            <div class="clearfix"></div>
                            <div class="submit">
        <!--                        <input type="button" name="submit" class="submit-button" id="submit_button" value="Submit" />-->
                                <div class="links orange">
                                    <a class="submit-button" id="enquiry_submit_button">Submit</a>
                                </div>
                                <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                            </div>
                        </div>
                        
                        <div class="form join-us">
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="form-field half-width">
                                <input type="text" name="join_us_name" id="join_us_name" class="input-field" value="<?php
                                if ($join_us_name == "") {
                                    echo "";
                                } else {
                                    echo $join_us_name;
                                }
                                ?>" placeholder="Name" />
                                <span class="join-us-name-error-msg error-msg" style="display: none;">Enter your Name</span>
                            </div>
                            <div class="form-field half-width">
                                <input type="email" name="join_us_email" id="join_us_email" class="input-field" value="<?php
                                if ($join_us_email == "") {
                                    echo "";
                                } else {
                                    echo $join_us_email;
                                }
                                ?>" placeholder="Email" />
                                <span class="join-us-email-error-msg error-msg" style="display: none;">Enter your Email Id</span>
                                <span class="join-us-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid Email Id (Ex: example@example.com)</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-field half-width">
                                <input type="text" name="join_us_subject" id="join_us_subject" class="input-field" value="<?php
                                if ($join_us_subject == "") {
                                    echo "";
                                } else {
                                    echo $join_us_subject;
                                }
                                ?>" placeholder="Subject" />
                            </div>
                            <div class="form-field half-width">
                                <input type="url" name="join_us_link" id="join_us_link" class="input-field" value="<?php
                                if ($join_us_link == "") {
                                    echo "";
                                } else {
                                    echo $join_us_link;
                                }
                                ?>" placeholder="LinkedIn" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-field">
                                <textarea name="join_us_message" rows="15" id="join_us_message" class="form-field message" value="" placeholder="Message"><?php if ($join_us_message == "") {
                                    echo "";
                                } else {
                                    echo $join_us_message;
                                } ?></textarea>
                            </div>
                            <div class="clearfix"></div>
                            <div class="success" id="join_us_success"></div>
                            <div class="clearfix"></div>
                            <div class="file-name"></div>
                            <div class="clearfix"></div>
                            <div class="submit">
                                <div class="links orange">
                                    <a class="submit-button" id="join_us_submit_button">Submit</a>
                                    <input type="submit" name="submit_file" class="submit-file" id="submit_file" value="Submit" style="display: none;" />
                                </div>
                                <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                            </div>
                            <div class="inputfile">
                                <div class="attachment-button" onclick="$('#resume').click();">
                                    <img src="<?php echo bloginfo( "template_directory" ) ?>/img/contact-us/attachment.png" alt="Icon" />
                                </div>
                                <div class="file-valid-error-msg error-msg" style="display: none;">Invalid file type</div>
                                <input type="file" name="resume" id="resume" onchange="readURL();" value="" style="opacity: 0;" /> <br />
                                <input type="text" name="file_link" id="file_link" value="" style="display: none;" /> <br />
                            </div>
                            <div class="clearfix"></div>
                            
                            </form>
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
            
        </script>
        
        <script type="text/javascript">
            $(document).ready(function () {
               
            });
            // When the window has finished loading create our google map below
            var ireland = document.getElementById('map_ireland_button');
            var india = document.getElementById('map_india_button');
            var america = document.getElementById('map_america_button');
            google.maps.event.addDomListener(window, 'load', init1);
            google.maps.event.addDomListener(ireland, 'click', init1);
            google.maps.event.addDomListener(india, 'click', init2);
            google.maps.event.addDomListener(america, 'click', init3);
        
            function init1() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 18,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(12.960823, 77.646535), //Control Enter Location
                    //
                    // How you would like to style the map.
                    styles: [
                        {elementType: 'geometry', stylers: [{color: '#657389'}]},
                        {elementType: 'labels.text.stroke', stylers: [{color: '#dee3e9'}]},
                        {elementType: 'labels.text.fill', stylers: [{color: '#28323e'}]},
                        {
                          featureType: 'administrative.locality',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                        },
                        {
                          featureType: 'poi',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#ffffff'}]
                        },
                        {
                          featureType: 'poi.park',
                          elementType: 'geometry',
                          stylers: [{color: '#263c3f'}]
                        },
                        {
                          featureType: 'poi.park',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#6b9a76'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'geometry',
                          stylers: [{color: '#e6ebf1'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#fff'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#1f3863'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'geometry',
                          stylers: [{color: '#746855'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#1f2835'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#f3d19c'}]
                        },
                        {
                          featureType: 'transit',
                          elementType: 'geometry',
                          stylers: [{color: '#2f3948'}]
                        },
                        {
                          featureType: 'transit.station',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'geometry',
                          stylers: [{color: '#89919e'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#28323e'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'labels.text.stroke',
                          stylers: [{color: '#28323e'}]
                        }
                    ]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map_ireland');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var iconPath = '<?php echo bloginfo ( "template_directory" ); ?>/img/contact-us/';
                var labels = 'HAL 2nd Stage, Kodihalli, Bengaluru, Karnataka 560008';
                var labelIndex = 0;
                
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(12.960823, 77.646535),
                    map: map,
                    icon: iconPath + 'map-icon-big.png',
                    title: 'Control Enter'
                });
            }
            
            function init2() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 18,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(12.960823, 77.646535), //Control Enter
                    //
                    // How you would like to style the map.
                    styles: [
                        {elementType: 'geometry', stylers: [{color: '#657389'}]},
                        {elementType: 'labels.text.stroke', stylers: [{color: '#dee3e9'}]},
                        {elementType: 'labels.text.fill', stylers: [{color: '#28323e'}]},
                        {
                          featureType: 'administrative.locality',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                        },
                        {
                          featureType: 'poi',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#ffffff'}]
                        },
                        {
                          featureType: 'poi.park',
                          elementType: 'geometry',
                          stylers: [{color: '#263c3f'}]
                        },
                        {
                          featureType: 'poi.park',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#6b9a76'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'geometry',
                          stylers: [{color: '#e6ebf1'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#fff'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#1f3863'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'geometry',
                          stylers: [{color: '#746855'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#1f2835'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#f3d19c'}]
                        },
                        {
                          featureType: 'transit',
                          elementType: 'geometry',
                          stylers: [{color: '#2f3948'}]
                        },
                        {
                          featureType: 'transit.station',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'geometry',
                          stylers: [{color: '#89919e'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#28323e'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'labels.text.stroke',
                          stylers: [{color: '#28323e'}]
                        }
                    ]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map_india');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var iconPath = '<?php echo bloginfo ( "template_directory" ); ?>/img/contact-us/';
                var labels = 'HAL 2nd Stage, Kodihalli, Bengaluru, Karnataka 560008';
                var labelIndex = 0;
                
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(12.960823, 77.646535),
                    map: map,
                    icon: iconPath + 'map-icon-big.png',
                    title: 'Control Enter'
                });
            }
            
            function init3() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions3 = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 18,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(12.960823, 77.646535), //Control Enter
                    //
                    // How you would like to style the map.
                    styles: [
                        {elementType: 'geometry', stylers: [{color: '#657389'}]},
                        {elementType: 'labels.text.stroke', stylers: [{color: '#dee3e9'}]},
                        {elementType: 'labels.text.fill', stylers: [{color: '#28323e'}]},
                        {
                          featureType: 'administrative.locality',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                        },
                        {
                          featureType: 'poi',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#ffffff'}]
                        },
                        {
                          featureType: 'poi.park',
                          elementType: 'geometry',
                          stylers: [{color: '#263c3f'}]
                        },
                        {
                          featureType: 'poi.park',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#6b9a76'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'geometry',
                          stylers: [{color: '#e6ebf1'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#fff'}]
                        },
                        {
                          featureType: 'road',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#1f3863'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'geometry',
                          stylers: [{color: '#746855'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#1f2835'}]
                        },
                        {
                          featureType: 'road.highway',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#f3d19c'}]
                        },
                        {
                          featureType: 'transit',
                          elementType: 'geometry',
                          stylers: [{color: '#2f3948'}]
                        },
                        {
                          featureType: 'transit.station',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'geometry',
                          stylers: [{color: '#89919e'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#28323e'}]
                        },
                        {
                          featureType: 'water',
                          elementType: 'labels.text.stroke',
                          stylers: [{color: '#28323e'}]
                        }
                    ]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement3 = document.getElementById('map_america');

                // Create the Google Map using our element and options defined above
                var map3 = new google.maps.Map(mapElement3, mapOptions3);

                // Let's also add a marker while we're at it
                var iconPath3 = '<?php echo bloginfo ( "template_directory" ); ?>/img/contact-us/';
                var labels3 = 'HAL 2nd Stage, Kodihalli, Bengaluru, Karnataka 560008';
                var labelIndex3 = 0;
                
                var marker3 = new google.maps.Marker({
                    position: new google.maps.LatLng(12.960823, 77.646535),
                    map: map3,
                    icon: iconPath3 + 'map-icon-big.png',
                    title: 'Control Enter'
                });
            }
        </script>
    </body>
</html>