<div class="footer">
    <div class="primary-footer">
        <div class="container">
            <div class="address-section">
<!--                <div class="footer-logo">
                    <a href="<?php echo bloginfo( "url" ); ?>/">
                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/control-enter-logo-white.png" title="Control Enter" alt="Control Enter Logo" />
                    </a>
                </div>-->
                <div class="each-address">
                    <b>Ireland (Global HQ)</b><br/>
                    <a class="first-number" href="tel://35312549187"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+353 (0)1 254 9187</a>
                    <a class="email" href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                </div>
                <div class="each-address">
                    <b>India (Asia Services)</b><br/>
                    <a href="tel://917760004444"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+91 776 000 4444</a>
                    <a class="email" href="mailto:suresh@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />suresh@controlentergroup.com</a>
                </div>
                <div class="each-address">
                    <b>United States (Americas Services)</b><br/>
                    <a href="tel://16173987978"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+1 617 398 7978</a>
                    <a class="email" href="mailto:info@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />info@controlentergroup.com</a>
                </div>
            </div>
            <div class="talk-to-us-section">
                <div class="heading talk-to-us-tab active talk-to-us">Talk to us</div>
                <div class="divider">|</div>
                <div class="heading join-us-tab join-us">Join us</div>
                <div class="clearfix"></div>
<!--                <div class="text">
                    Atetuer adipiscing elit. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
                </div>-->
                <div class="form talk-to-us half-width">
                    <div class="form-field half-width">
                        <input type="text" name="name" id="name" class="input-field" value="" placeholder="Name" />
                        <span class="name-error-msg error-msg" style="display: none;">Enter your name</span>
                    </div>
                    <div class="form-field half-width">
                        <input type="email" name="email" id="email" class="input-field" value="" placeholder="Email" />
                        <span class="email-error-msg error-msg" style="display: none;">Enter your email ID</span>
                        <span class="email-valid-error-msg error-msg" style="display: none;">You have entered an invalid email ID (Ex: example@example.com)</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-field">
                        <input type="text" name="subject" id="subject" class="input-field" value="" placeholder="Subject" />
                        <span class="subject-error-msg error-msg" style="display: none;">Please enter subject</span>
                    </div>
                    <div class="form-field message-field">
                        <textarea name="message" rows="5" id="message" class="form-field message" value="" placeholder="Send us a message or ask us a question here!"></textarea>
                        <span class="message-error-msg error-msg" style="display: none;">Please enter a brief message</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="success" id="success"></div>
                    <div class="clearfix"></div>
                    <div class="submit">
<!--                        <input type="button" name="submit" class="submit-button" id="submit_button" value="Submit" />-->
                        <div class="links">
                            <a class="submit-button" id="submit_button">Submit</a>
                        </div>
                        <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                    </div>
                </div>
                
                <div class="form join-us">
                    <form>
                        <div class="form-field half-width">
                        <input type="text" name="join_us_name" id="join_us_name" class="input-field" value="" placeholder="Name" />
                        <span class="join-us-name-error-msg error-msg" style="display: none;">Enter your name</span>
                    </div>
                    <div class="form-field half-width">
                        <input type="email" name="join_us_email" id="join_us_email" class="input-field" value="" placeholder="Email" />
                        <span class="join-us-email-error-msg error-msg" style="display: none;">Enter your email ID</span>
                        <span class="join-us-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid email ID (Ex: example@example.com)</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-field half-width">
                        <input type="text" name="join_us_subject" id="join_us_subject" class="input-field" value="" placeholder="Subject" />
                        <span class="join-us-subject-error error-msg" style="display: none;">Please enter subject</span>
                    </div>
                    <div class="form-field half-width">
                        <input type="url" name="join_us_link" id="join_us_link" class="input-field" value="" placeholder="LinkedIn" />
                        <span class="join-us-linkedin-error error-msg" style="display: none;">Please enter LinkedIn URL</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-field message-field">
                        <textarea name="join_us_message" rows="5" id="join_us_message" class="form-field message" value="" placeholder="Send us a message or ask us a question here!"></textarea>
                        <span class="join-us-message-error error-msg" style="display: none;">Please enter a brief message</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="success" id="join_us_success"></div>
                    <div class="clearfix"></div>
                    <div class="submit">
<!--                        <input type="button" name="submit" class="submit-button" id="submit_button" value="Submit" />-->
                        <div class="links orange">
                            <a class="submit-button" id="join_us_submit_button">Submit</a>
                        </div>
                        <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                    </div>
                    </form>

                </div>
            </div>
            <div class="social-media-section">
<!--                <div each-icon>
                    <svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" id="svg8" xml:space="preserve" enable-background="new 0 0 100 100" viewBox="0 0 100 125" y="0px" x="0px" version="1.1" width="30" height="30"><metadata id="metadata14"><rdf:RDF><cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/></cc:Work></rdf:RDF></metadata><defs id="defs12"/><path fill="#fff" id="path2" d="M8.647,54.296c2.151,10.248,7.35,18.779,15.034,24.671c6.93,5.313,15.715,8.24,24.737,8.24  c11.777,0,22.934-4.878,30.613-13.382c5.891-6.525,12.513-18.218,10.604-36.862c-0.119-1.164-0.293-2.309-0.521-3.428  c1.336-1.785,2.336-2.914,2.345-2.924c2.028-2.263,2.599-5.481,1.472-8.304c-1.126-2.823-3.755-4.763-6.784-5.009  c-1.286-0.104-2.58-0.157-3.846-0.157c-0.835,0-1.667,0.023-2.497,0.068c-4.873-4.201-11.097-6.545-17.633-6.545  c-7.301,0-13.938,2.996-18.21,8.217c-4.282,5.234-5.848,12.325-4.437,20.022c-4.021-0.966-7.789-2.487-9.366-3.229  c-1.092-0.518-2.264-0.772-3.429-0.772c-1.764,0-3.514,0.583-4.952,1.716c-2.388,1.881-3.503,4.953-2.879,7.928  c0.037,0.173,0.074,0.346,0.112,0.518c-0.825-0.275-1.68-0.411-2.531-0.411c-1.764,0-3.514,0.583-4.952,1.716  C9.138,48.249,8.023,51.32,8.647,54.296z M16.477,52.652c0,0,8.609,4.087,16.865,4.778c-3.059-3.891-5.395-8.713-6.615-14.528  c0,0,10.266,4.872,19.127,4.872c0.749,0,1.488-0.035,2.21-0.11c0.356-2.593,0.212-5.803-0.587-9.762  c-2.545-12.6,5.664-19.237,14.693-19.237c5.26,0,10.795,2.25,14.648,6.885c1.755-0.267,3.583-0.408,5.481-0.408  c1.046,0,2.112,0.043,3.2,0.131c0,0-2.482,2.771-5.079,6.727c0.608,1.75,1.04,3.674,1.256,5.778  c2.8,27.346-15.294,41.43-33.258,41.43C34.279,79.207,20.22,70.489,16.477,52.652z"/><text id="text4" font-family="'Helvetica Neue', Helvetica, Arial-Unicode, Arial, Sans-serif" font-weight="bold" font-size="5px" fill="#000000" y="115" x="0">Created by Thomas Le Bas</text>
                        <text id="text6" font-family="'Helvetica Neue', Helvetica, Arial-Unicode, Arial, Sans-serif" font-weight="bold" font-size="5px" fill="#000000" y="120" x="0">from the Noun Project</text>
                    </svg>
                </div>-->
                <div class="each-icon rss">
                    <a href="<?php bloginfo( "url" ) ?>/insights/" target="_blank">
                        <div class="link">
                           <img src="<?php echo bloginfo( "template_directory" ); ?>/img/rss.png" alt="Twitter Icon" />
                        </div>
                        <div class="text">Subscribe<br/>to RSS Feed</div>
                    </a>
                    <div class="clearfix"></div>
                </div>
                <div class="each-icon">
                    <div class="link">
                        <a href="https://www.linkedin.com/company/control-enter" target="_blank"><img src="<?php echo bloginfo( "template_directory" ) ?>/img/linkedin.png" alt="Linkedin" /></a>
                    </div>
                </div>
                <div class="clearfix hidden-xs"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="secondary-footer">
        <div class="container">
            <p>&copy; Copyright <?php echo date("Y"); ?> - Control Enter</p>
            <div class="tiramisu-logo">
                <a href="http://www.tiramisumedia.com/" target="_blank"><img src="<?php echo bloginfo( "template_directory" ) ?>/img/tiramisu-logo.png" alt="Tiramisu New Media Solutions Logo" /></a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo bloginfo( "template_directory" ); ?>/js/jquery-1.11.3.min.js"></script>

<script type="text/javascript">
    function readURL() {
        var filename = $('#resume').val().replace(/C:\\fakepath\\/i, '');
        $('.file-name').html("Selected file: " + filename);
        $('#file_link').val('<?php echo bloginfo("template_directory") ?>/resumes/' + filename);
    }
    function showMap(mapButton, eachMap){
        $('.map').hide();
        $('.' + eachMap).show();
        $('.each-address .address .icon').removeClass('active');
        $('.' + mapButton).addClass('active');
    }
    function showForm(formButton, form) {
        $('.footer .form').fadeOut();
        $('.' + form).fadeIn();
        $('.footer .primary-footer .talk-to-us-section .heading').removeClass("active");
        $('.' + formButton).addClass("active");
    }
    $(window).scroll(function () {
//        console.log($('.footer').offset().top - $(document).scrollTop());
//        if ($('.footer').offset().top - $(document).scrollTop() < 400) {
//            $('.insights-page .static-nav').hide();
//            $('.filters-section.show').hide();
//        }
//        else {
//            $('.insights-page .static-nav').show();
//            $('.filters-section.show').show();
//        }
    });
    $(document).ready(function() {
        $('.join-us').click(function() {
            $('.footer .primary-footer .talk-to-us-section .form.talk-to-us').hide();
            $('.footer .primary-footer .talk-to-us-section .form.join-us').show();
            $('.footer .primary-footer .talk-to-us-section .heading.talk-to-us-tab').removeClass("active");
            $('.footer .primary-footer .talk-to-us-section .heading.join-us-tab').addClass("active");
        });
        $('.talk-to-us').click(function() {
            $('.footer .primary-footer .talk-to-us-section .form.join-us').hide();
            $('.footer .primary-footer .talk-to-us-section .form.talk-to-us').show();
            $('.footer .primary-footer .talk-to-us-section .heading.join-us-tab').removeClass("active");
            $('.footer .primary-footer .talk-to-us-section .heading.talk-to-us-tab').addClass("active");
        });
        if($(window).width() < 768) {
            $('.body .banner').height($(window).height());
        }
        
        var addressContainerPadding = $('.header .logo').offset().left;
        var addressContainerWidth = ((50 * $('.address-map-container').width()) / 100) - addressContainerPadding;
        $('.body.contact-page .address-container').css("padding-left", addressContainerPadding);
        $('.body.contact-page .address-container').css("width", addressContainerWidth);
        $('.body.home-page .banner .content .text').addClass('animated fadeInLeft');
        $('.body.home-page .banner .content .vector-object').addClass('animated fadeInRight');
        $('.body.home-page .insights .subscriber').addClass('animated fadeIn');
        $('.body.home-page .insights .insights-listing .each-row.header-section').addClass('animated fadeInDown');
        var isClosed = true;
        if($(window).width() < 768) {
            $('.hamburger-cross').click(function(){
                if(isClosed == true) {
                    $('.menu-container').addClass("animated fadeInRight");
                    $('.menu-container').addClass("menu-opened");
                    $('#pathA').css('d','path("M 12.972944,50.936147 51.027056,12.882035")');
                    $('#pathB').css('opacity', '0');
                    $('#pathC').css('d','path("M 12.972944,12.882035 51.027056,50.936147")');
                    $('body').css("overflow", "hidden");
                    $('.mobile-menu').addClass("menu-opened");
                    isClosed = false;
                } else {
                    $('.menu-container').removeClass("animated fadeInRight");
                    $('.menu-container').removeClass("menu-opened");
                    $('#pathA').css('d','path("m 5.0916789,20.818994 53.8166421,0")');
                    $('#pathB').css('opacity', '1');
                    $('#pathC').css('d','path("m 5.0916788,42.95698 53.8166422,0")');
                    $('body').css("overflow", "auto");
                    $('.mobile-menu').removeClass("menu-opened");
                    isClosed = true;
                }
            });
        }
        
//        $('#enquiry_submit_button').click(function() {
//            var error = 'Error:\n';
//            if ($('#your_message').val() == '') { 
//                $('#your_message').focus();
//                $('.talk-to-us-message-error').show();
//                error = '';
//            }
//            else {
//                $('.talk-to-us-message-error').hide();
//            }
//            if ($('#your_subject').val() == '') { 
//                $('#your_subject').focus();
//                $('.talk-to-us-subject-error-msg').show();
//                error = '';
//            }
//            else {
//                $('.join-us-subject-error-msg').hide();
//            }
//            if ($('#your_email').val() == '') { 
//                $('#your_email').focus();
//                $('.enquiry-email-error-msg').show();
//                error = '';
//            }
//            else {
//                $('.enquiry-email-error-msg').hide();
//            }
//
//            if ($('#your_email').val() != '') {
//                if ($('#your_email').val().search('@') < 1 || $('#your_email').val().lastIndexOf('.') < 1 || $('#your_email').val().split('@').length != 2 || $('#your_email').val().search('@') >= $('#your_email').val().lastIndexOf('.')) {
//                    $('#your_email').focus();
//                    $('.enquiry-email-valid-error-msg').show();
//                    error = '';
//                }
//                else {
//                    $('.email-valid-error-msg').hide();
//                }
//            }
//            if ($('#your_name').val() == '') {
//                $('#your_name').focus();
//                $('.enquiry-name-error-msg').show();
//                error = '';
//            }
//            else {
//                $('.enquiry-name-error-msg').hide();
//            }
//            if(error == "") {
//                var error = '';
//            }
//            if (error != 'Error:\n') {
//                $('#enquiry_success').html(error);
//                $('#enquiry_success').slideDown(); // .delay(50000).hide('slow');
//            } else {
//                $('.loader').show();
//                $.post('<?php echo bloginfo( "template_directory" ); ?>/submit-talk-to-us.php', $('#your_name, #your_email, #your_subject, #your_message').serialize(), function (response) {
//                    $('#your_name').val('');
//                    $('#your_email').val('');
//                    $('#your_message').val('');
//                    $('#your_subject').val('');
//                    $('#enquiry_success').html(response);
//                    $('#enquiry_success').slideDown(); // .delay(50000).hide('slow');
//                    $('.loader').hide();
//                });
//            }
//        });

        $('#join_us_submit_button').click(function() {
            var error = 'Error:\n';
            if ($('#join_us_message').val() == '') { 
                $('#join_us_message').focus();
                $('.join-us-message-error').show();
                error = '';
            }
            else {
                $('.join-us-message-error').hide();
            }
            if ($('#join_us_subject').val() == '') { 
                $('#join_us_subject').focus();
                $('.join-us-subject-error').show();
                error = '';
            }
            else {
                $('.join-us-subject-error').hide();
            }
            if ($('#join_us_link').val() == '') { 
                $('#join_us_link').focus();
                $('.join-us-linkedin-error').show();
                error = '';
            }
            else {
                $('.join-us-linkedin-error').hide();
            }
            if ($('#join_us_email').val() == '') { 
                $('#join_us_email').focus();
                $('.join-us-email-error-msg').show();
                error = '';
            }
            else {
                $('.join-us-email-error-msg').hide();
            }

            if ($('#join_us_email').val() != '') {
                if ($('#join_us_email').val().search('@') < 1 || $('#join_us_email').val().lastIndexOf('.') < 1 || $('#join_us_email').val().split('@').length != 2 || $('#join_us_email').val().search('@') >= $('#join_us_email').val().lastIndexOf('.')) {
                    $('#join_us_email').focus();
                    $('.join-us-email-valid-error-msg').show();
                    error = '';
                }
                else {
                    $('.join-us-valid-error-msg').hide();
                }
            }
            if ($('#join_us_name').val() == '') {
                $('#join_us_name').focus();
                $('.join-us-name-error-msg').show();
                error = '';
            }
            else {
                $('.join-us-name-error-msg').hide();
            }
            if(error == "") {
                var error = '';
            }
            if (error != 'Error:\n') {
                $('#join_us_success').html(error);
                $('#join_us_success').slideDown(); // .delay(50000).hide('slow');
            } else {
                $('.loader').show();
                $.post('<?php echo bloginfo( "template_directory" ); ?>/submit-join-us.php', $('#join_us_name, #join_us_email, #join_us_subject, #join_us_link, #join_us_message, #file_link').serialize(), function (response) {
                    $('#join_us_name').val('');
                    $('#join_us_email').val('');
                    $('#join_us_message').val('');
                    $('#join_us_subject').val('');
                    $('#join_us_link').val('');
                    $('#join_us_success').html(response);
                    $('#join_us_success').slideDown(); // .delay(50000).hide('slow');
                    $('.loader').hide();
                });
            }
        });
        
        $('#submit_button').click(function() {
            var error = 'Error:\n';
            if ($('#message').val() == '') { 
                $('#message').focus();
                $('.message-error-msg').show();
                error = '';
            }
            if ($('#subject').val() == '') { 
                $('#subject').focus();
                $('.subject-error-msg').show();
                error = '';
            }
            else {
                $('.subject-error-msg').hide();
            }
            if ($('#email').val() == '') { 
                $('#email').focus();
                $('.email-error-msg').show();
                error = '';
            }
            else {
                $('.email-error-msg').hide();
            }

            if ($('#email').val() != '') {
                if ($('#email').val().search('@') < 1 || $('#email').val().lastIndexOf('.') < 1 || $('#email').val().split('@').length != 2 || $('#email').val().search('@') >= $('#email').val().lastIndexOf('.')) {
                    $('#email').focus();
                    $('.email-valid-error-msg').show();
                    error = '';
                }
                else {
                    $('.email-valid-error-msg').hide();
                }
            }
            if ($('#name').val() == '') {
                $('#name').focus();
                $('.name-error-msg').show();
                error = '';
            }
            else {
                $('.name-error-msg').hide();
            }
            if(error == "") {
                var error = '';
            }
            if (error != 'Error:\n') {
                $('#success').html(error);
                $('#success').slideDown(); // .delay(50000).hide('slow');
            } else {
                $('.loader').show();
                $.post('<?php echo bloginfo( "template_directory" ) ?>/submit-enquiry.php', $('#name, #email, #subject, #message').serialize(), function (response) {
                    $('#name').val('');
                    $('#email').val('');
                    $('#message').val('');
                    $('#success').html(response);
                    $('#success').slideDown(); // .delay(50000).hide('slow');
                    $('.loader').hide();
                });
            }
        });
        $('#search_query').keypress(function(e) {
            if(e.which == 13) {
                if ($('#search_query').val()) {
                    var queryString = $('#search_query').val();
                    var queryString = queryString.replace("%20"," ");
                    window.location = '<?php bloginfo( 'url' ); ?>/search-results/?query=' + queryString;
                }
            }
        });
        
        $('.filters-open-button').click(function(){
            $('.filters-section').show();
            $('.filters-section').addClass("show");
            $('.filters-open-button').hide();
            $('.filters-close-button').show();
        });
        $('.filters-close-button, .go-button').click(function(){
            $('.filters-section').hide();
            $('.filters-section').removeClass("show");
            $('.filters-open-button').show();
            $('.filters-close-button').hide();
        });
        $('.clear-all').click(function() {
            $('.each-insight-container').removeClass("inactive");
            $('.each-insight-container').fadeIn();
            $('.each-insight-container').removeClass("active");
            $('.each-list li').removeClass("active");
        });
        
        $('#subscribe_button').click(function() {
            var error = 'Error:\n';
            if ($('#subscriber_email').val() == '') { 
                $('#subscriber_email').focus();
                $('.subscriber-email-error-msg').show();
                error = '';
            }
            else {
                $('.subscriber-email-error-msg').hide();
            }

            if ($('#subscriber_email').val() != '') {
                if ($('#subscriber_email').val().search('@') < 1 || $('#subscriber_email').val().lastIndexOf('.') < 1 || $('#subscriber_email').val().split('@').length != 2 || $('#subscriber_email').val().search('@') >= $('#subscriber_email').val().lastIndexOf('.')) {
                    $('#subscriber_email').focus();
                    $('.subscriber-email-valid-error-msg').show();
                    error = '';
                }
                else {
                    $('.subscriber-email-valid-error-msg').hide();
                }
            }
            if (error == 'Error:\n') {
                $('.loader').show();
                $.post('<?php echo bloginfo( "template_directory" ); ?>/submit-subscriber.php', $('#subscriber_email').serialize(), function (response) {
                    $('#subscriber_email').val('');
                    $('#success_subscriber').html(response);
                    $('#success_subscriber').slideDown(); // .delay(50000).hide('slow');
                    $('.loader').hide();
                });
            }
       });
       
        var scrollTo = localStorage.getItem('scrollTo');
        var scrollToMargin = localStorage.getItem('scrollToMargin');
        if (scrollTo !== '' && $(scrollTo).length > 0) {
            $('html, body').animate({
                scrollTop: $(scrollTo).offset().top - scrollToMargin - 15
            }, 1500, function () {
                localStorage.setItem('scrollTo', '');
                localStorage.setItem('scrollToMargin', 0);
            });
        }
    });
    
    function showFilters(showList, filterButton) {
        $('.each-list').hide();
        $('.' + showList).show();
        $('.each-tab').removeClass("active");
        $('.' + filterButton).addClass("active");
        $('.each-tab img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-right.png");
        $('.each-tab.active img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-down.png");
    } 
//    var filters = [];
    function showFiltersResults(keyWord) {
        $('.each-insight-container').toggleClass("inactive");
        $('.each-insight-container.' + keyWord).removeClass("inactive");
        $('.each-insight-container.' + keyWord).addClass("active");
        $('.filter-container .each-list .' + keyWord).toggleClass("active");
//        filters.push(keyWord);
//        if(jQuery.inArray(keyWord, filters)) {
            
//        }
    }
    function scrollToSection(scrollTo, scrollToMargin, link) {
        localStorage.setItem('scrollTo', scrollTo);
        localStorage.setItem('scrollToMargin', scrollToMargin);

        if (link !== '') {
            setTimeout(function () {
                window.location = link;
            }, 50);
        } else {
            $('html, body').animate({
                scrollTop: $(scrollTo).offset().top - scrollToMargin - 15
            }, 1000, function () {
                localStorage.setItem('scrollTo', '');
                localStorage.setItem('scrollToMargin', 0);
            });
        }
    }
    
</script>