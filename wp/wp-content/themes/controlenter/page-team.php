<?php $this_page = 'team'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>About | Control Enter</title>
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <div class="body about-page">
            <div class="section banner">
                <div class="image">
                    <img class="hidden-xs hidden-md" src="<?php echo bloginfo( "template_directory" ); ?>/img/about/control-enter-team-banner.jpg" alt="About Control Enter" />
                    <img class="visible-xs hidden-md" src="<?php echo bloginfo( "template_directory" ); ?>/img/responsive-imgs/control-enter-about-banner.png" alt="About Control Enter" />
                    <img class="visible-md" src="<?php echo bloginfo( "template_directory" ); ?>/img/responsive-imgs/control-enter-about-banner-tablet.jpg" alt="About Control Enter" />
                </div>
                <div class="container">
                    <div class="content">
                        <div class="links orange">
                            <a href="javascript: scrollToSection('.footer', -10, '')" class="talk-to-us">Talk to us</a>
                        </div>
                        <div class="text">
                            We bring together strong talent that is collectively focused on designing and delivering the best solutions for your business.  Our vision, mission and values are our foundation for delivering exceptional results.
                        </div>
                    </div>
                </div>
                <div class="next-section visible-xs"><a href="javascript: scrollToSection('.section.introduction', 30, '');"><img src="<?php echo bloginfo( "template_directory" ) ?>/img/responsive-imgs/downarrowblue.png" alt="Down Arrow" /></a></div>
            </div>
            
            <div class="introduction section">
                <div class="container">
                    <div class="content">
                        <div class="text">
                            <span>Our Vision:</span>
                            We are passionate about helping companies advance problem solving to the next level by bringing the best of artificial and human intelligence together in well-designed, context-relevant solutions.
                        </div>
                        <div class="text">
                            <span>Our Mission:</span>
                            <ul>
                                <li>We help companies around the world advance decision making through blended strategy, science, and design.</li>
                                <li class="half-width">We create measurable financial value through customized, resilient product and service solutions.</li>
                                <li class="half-width">We are resourceful and tenacious in getting to the right answer to complex problems.</li>
                                <li class="half-width">We are fearless in experimenting and innovating to drive future solutions.</li>
                                <li class="half-width">We are trusted partners who empower our clients and take care of our team.</li>
                                <li class="clearfix"></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="skills section">
                <div class="container">
                    <div class="listing">
                        <div class="section-heading">
                            <span>Our Values</span>
                        </div>
                        <ul>
                            <li>
                                <div class="head">Growth</div>
                                <div class="text">Continuously learning and developing across disciplines</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Accountability</div>
                                <div class="text">Owning the business problem beginning to end</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Creativity</div>
                                <div class="text">Experimenting and taking calculated risks to add value</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Partnership</div>
                                <div class="text">Guiding problem solving as a partner through inquiry</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Delivery</div>
                                <div class="text">Consistently delivering effort and quality to help the team and client succeed</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Equality</div>
                                <div class="text">Mutually respecting and recognizing contributions</div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="content">
                        <div class="section-heading">
                            <span>Our Talent</span>
                        </div>
<!--                        <div class="heading">
                            Generalist Skills
                        </div>
                        <div class="heading vertical">Specialist Skills</div>-->
                        <div class="t-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="274" height="292" viewBox="0 0 274 292">
                                <image id="T.svg" width="274" height="292" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAARIAAAEkCAMAAADUyk6hAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAC61BMVEUAAAAgOWUgOWUxSHBygp6irb+2vsy3v82lr8F5iKM0S3NDWH2QnLKJlq0uRW+Kl64lPmmCkKlRZId7iqQ8UniMma9xgZ01THOEkqqPnLInP2pmd5UjPGdldpWfqr24wM62v82gqr1rfJltfZojO2dic5KdqLuhrL5sfZopQWtfcZB9i6VwgJx0hJ8iO2ZgcpGbprqjrcAqQmwmPmmTn7X19vj////7+/wrQ23y8/a5wc/k5+zW2+NYa4yYo7hPY4Z1hKCstcY/VXvZ3uXu7/N3hqEhOma6ws/v8fR+jKaeqbxGW4BKXoLIztk4T3bS1+Dx8/UoQGrGzdjz9PfM0tzHzdjn6e7l6O2ttsb5+vvL0Nv8/P3+/v6cp7uvuMfV2uL29/nO1N18i6Swucng5Or4+frJz9rr7fEkPWhQZIb6+vvP1N7f4ulUaIn8/f03TXVXaovL0dsvRm/Z3eRHXIAxSXFbbo7X2+MySXJoeZfY3OQzSnLU2eFWaYo6UHeSnrRqe5i/xtOFk6uapbmNmrDEy9eVobaFkqrCydX09fd2haDs7vLi5etLX4OUoLXm6O1OYoXp7PBTZoh4h6I2THRabI2zu8otRW7w8vWut8eDkamXo7dFWn6Om7GLmK9JXYGRnbNMYINkdpT9/f5pepihq77u8PTa3ubR1t9SZYcwR3D3+PpneJaGlKx+jabb3+ZwgJ1bbY1ecJCrtMWps8Tt7/Ld4eexuslUZ4nj5uxjdZTn6u/Eytbo6+9Za4zFzNe9xdI5UHeAjqfAx9SZpLiTn7Q9U3nQ1d5/jqemsMHByNRCV33DydW9xNG8w9C0vMtIXYEsRG2nscI+U3pzg57K0NrT2OGMmbC+xtJGWn9NYYTh5epEWX5AVnuaprpidJOHlaxBV3yossM/VHpdb49cb49VaIp3h6GqtMQ7UXi1vcxhcpKBj6jg4+ne4eikrsB6iaOoscOWorfN093q7PFufptpeZevuMgAAADi9mTsAAAAAnRSTlMAMFBK/ZQAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH4QYIDgkpwtax5wAADJ5JREFUeNrt3XtcFVUCB/A6lppcCQhTwbeYDwQV4gcIaqZdH2GGlEWiZqaC5WNtVcyyFu2lpWYEmpkVhiW91LCX1a5aW9tDa+2xumVb7rZtbbtb+/h3z2Nm7mO4lxtb6X7m9/uDmQNz7wxfhnPOzJw7c4pgwnLKid6Aky8kcYUkrpDEFZK4QhJXSOIKSVwhiSskcYUkrpDElegkrU47vXWbtme0M6U4n532shTvOzPBXi7R51OTJGeBs2Qp2cx2OLtj0Dt28nVOsefbm1fZ752qZ7p07da9R89eKSFv5/P1dpZM63NOmx59+/XXhQE+X5yeSff5BgrR0efLUKUMn89ZabzZGiuZgwb3aDNkYFYLSbLPzYFO7ul5qpwKO/nqNwaGpllLFgBaxlmgUJaG2YXc4fZyYoQsnmcX8s2rhPXeI9X0/FHmNaMv8Ae9HTDGXvD8QvONseP8sjQe0JLpQM6FQhQBE1RxAlBkvyDZbI3JRRPNqy9OzGwJSfEk+dqSSy6dDEyySXLzdS4zq8Ll/nCSUrPAFYZkipwtk8tNtd9zmixMj0KSJ9/pyhlXjAYmZoqr8s3LS+Rkpr1RVwOzZsyeA4wqD5BU5AK9RPMkCRJ87jXq1demiEiJQtJN/oLpar3z+qaE/CWdVQE9w0kC/wqKRP1t+58JzF9gvtUuV71oYWSSs4CfLRLCP+i6nwd2wPigjTpbYi2WCyxZOkI4JJXyrzZYxECyDLh+udq0G26M/HtHJlkhN79ryHeaIMFNzZIIMRu42XzLB/wCaBOZpC1Qpct++wdhJCuBVXrG7PiaJO0W4NK0WEjOBW4NffvvRXIbcHvod1wko+8AOjVPMhNYrWcS1qC08lrMXxGRRO6Zd94VstIwkrXAuvWBoia5FZhcKWIh6QDcvUE0k8gkQ4F7XCSTUnWqrVXdO8v8tgGSGrNAbRDJRRsBs5/KvX6TuA/YHJHkXrXnXdPh/ogkSWqBiVvsfytJ0vMBWT9YSs2RbFUV2w0PPtRCkhnAvdZaVVYHtzip9qoenoK6bU20OEWG5LrExE6P1APbzW7+KPCY2NGAscWRSNS/lm6xHo9AIp6o0wuM6ieCNm6ciI1EPDlfL//U0y0l2Rm01tOaJBEDZSu0KyKJlcn3239htXG7gQsikogRu0sQVHG7SETRMxv1AkOCNm6oP0YScX/bRv2KaZFrk8gkbQDdxVmYmJi4BxigN/vZeJ3ywKpkj6DhuTsdks1mgbRgkudrzTs+C5whJ+vl3hOZRNY4Ox+RP8ELEUiEKB82Ve556G1IUpe/CHSIlUR29eatbAzuHX0Pkk7AXmu2SyPKdjRRvepVyX/lksbI1esi2QkYqMsLZBP2UoGM3KCXo5DI5L0CzIxIIpP1S1NR6ep1Qy7qdsVMIlPcw9V0xETyq1yrZyjEPmC/iEQiDuhdIRKJ6C1bJt0evBrUGR0aicT008XNsrVumsRUQ6IP8IpNorZv42sxkWSZiazFf90CEjFY1uSvZwuRUjUfZRsik/jfiEoifgO0lpNsuSttNgcsd6AuOQLJpE1vyq+t3gLWNk3ydo2qmGrfAcY7JKJGbktxLCQHux+SX99tDbzXEpIE+bsg55oZ6kCnytpsq0OfvyJ4VWk1CO/Q5weR/LYBkA3IYWtzZfoCBwyJWXiIQ7JTVT3vfzBXHoUsaJJENcLr3p9YKrfstQBJ1nZZYYaS5NgHFnI768xqrhJbVV1/xUTZdWiIawmJKP8w3+zm63rZf0k7ySH6CR+FtzgIIhHtgTmtxAdAP2v538kdvb8mMRnpkKStso409xwRTZJktrdedVR3RezDvt/Ll70eQuIcfiY725Qo/PeZ1gqXtahDr1Lbu1vNsytfsE4CBE4O+OJDD7r7PxB2ckAVk+1D+vIOPt9ztT7fFudkwj6f72N9csAkNejkQPzTPQ/WfHKsi71oos8XeoSWJ7fp4NSu5brgnBzY6fN1yA4+OeCcpIh3tilJ7U/bptYcPGdZdpRfmqeQXCGJKyRxhSSukMQVkrhCEldI4gpJXCGJKyRxhSSukMSVU05ATo198049EdtHkpMhEUgqxw3+NI4kMtkHzaW6Jep0WV1nkgjxIeoOy8kfrjSnADNIktCIEjXKaag6U3onMDnN8yTzzNWH6jpguCifDTzneZJ04JjQV6cuzhKiX+CSt3dJVgFqQMleYLecVNgjarxMUgUsEWLHFHNBZoQ9CsnLJF2BN4S4CbhS/t+Ip80wPG+TLGoApn0mK9caVWoNXOR5EjHcujxZIee3zsck9ktEyn4t8qqa35frjAD1MonwD+w+Y8wyM/9x2OAgj5JEC0lI4k6GMxiMJCQhCUlIQhKS/Mgk8yYEZw9JQgbTBj7FQRKShJB8nhiWzz1PEj0kIYk7HYcNW0wS9ktIQhKSkIQkJDkxJDzGIQlJWkBSWxSWWs+TRA9JSEISksRGcrwgLMc9T8JGmCTNk1SnhqXa8ySsXknSPMn5O0IR/vgnz5OkFhwJKvk/rGP1morSL5zCorfZ4phG+M/WDaC2HmUjLPPlHslwg7533uFZcvb0bM+TiLy/SIiNX4nsturOdr1DfuZREiG+zgXKhj8qRbbzc8JWkiabvvymLPZL7Cx+S4KUnsGuWlAya6zbMpNEiP7JOuOBeWauv+dJeHKAJCRpAUl5fFjKPU8SPSQhCUkikFiPgMpML8w5uvJdkggRN6utHiywSbc31+0giehunnQx0GqDnyFJZRmW+oXw/xVYc1bnXORywPgy4Gs5eQz64Sqf6AcbeJzkNnNDqKnmCTG7eJcbIbYA6qrFN8AWoZ/AwHshpevH5xyBuUvUMCDd8yQVQOGivAnAUVV6AhjmeZJM2dSUNli3ufE/hZwUz5OIQeYRa2taCX3DuamCJGJbvXqQj7o7lDh0+5xKksgUD8j4ynqElgh71JhXSaKEJCQhCUlIQhKS/GgktUVFO0gSfrUvgyQkIQlJfgCSquTgcMgNB1OQhCQtI0nnkBu2OCQhCUl+cJLoIQlJTNqZif+qwvyrD3CsmlBj1f6mHS7nWDU7bYC/i8BYtX+QpLIO16uxat8Ajemdp2BuK8+TPG7GqiXJHWSQHqvGY5zAWLVRgmPVdAJj1dQTUe8yQ9a8TRIYq/aQLC3kWDVdiUyPzwoaq/ay50lCx6pt51g1ETpW7XXz2Fivk5ixai+asWpvcayaTvGg1X3ssWqLBEmihyQkIUmTJHG+sMR5noSXtkjSPEnFmLBUeJ6E1StJSPI/k/CZFiQhCUlIQhKS/GQkfJ6wi4SHfSRpnoTPE3aRRA9JSNJEMrseIklwdjzxPKvX4Gz9rIEtTlC69Hpft8FlS0iis3zVSxpkztovWb2qVEzLtfpp5WE/8SZJ9mmPao3Gby8B2AjLrG/UIBP6JYgxJNFRxzi5499UsyQJkOCpxHiSOHltqB6r1jCygiROvtw8VlcnhVeTxEnKedOtRriIJE421JiuyXcZxSSxU/3gP3lWLTwJ//qOJK48tLuUJOFZvIAkUUMSkqiEXu2Tmed5El7aIknzJLza5yJh9UoSkrSE5PDIkc7TyY+MHHmYJMU5WJppFzKX4g6eHPgKSAyUEvXNGDxO0gm4MVC6EejkeZLO5sE4Vu4yd2PwNsk4oE+g1AcY53mSY0DfQGkIcMzzJHlTMMv50GfcLEzJ8zyJqAEKrGsVRQVADfslIq4UmN9j9cKFq3vMB0rjSCK7r2WBMwNloZ1Xr5KI1HpbpD5VkEQnvmp2A9AwuypekCSQrKymvutpkqZDEpHA22kLcbygYJucVBYVqbMC/NSWcG6UnGjGlpCEJCQhCUlIQpKfkqS+oKBgI3C3nNSThCMHSBILSXVqWKo9TxI9JCGJky7ZJAlOwm3bgXXnLCeJI7LXtDQvvkkSK1vs1rfQTxKdtLHALak71+YCx0micyEwsYuc7gO+JYnOceBTNW0FzCSJjvOQOmAMSUhCkthJ1hWqACV6yofB8OQASZonWZERlhWeJ4kekpCEJM2QJHfevTKJJDJZr+zS037qPnPo5ieJuBW4R04OzTVt8JMkSbkY9Wog437JsXc6sIYDs4YBB1T3pAxYJfwSZqHnScYBA4R+0uXoYiH+Hfaf40mSLWbIwASgrZyM4EeU9POEZYvTrs58EKeCH2TTH3f8jxCDgRL1PNQM4N+eJ8nLAfaPkc3NJlV6B6j0PIn6vKNKnapRksrwEfslIu1yTVKl5r/IqVtPEpnj7739mfVZ4Y4PC5JED0lIQhKSkIQkJCEJSUhCkpMtJCEJSUhCEpKQhCQnS0jy/0byX150j6T8LVa4AAAAAElFTkSuQmCC"/>
                            </svg>
                        </div>
                        <div class="text">
                            <p>
                                We hire talent with exceptional generalist problem solving skills on top of a solid technical foundation. Team members typically come from foundational backgrounds in analytics, data science, engineering, computer science, business, and design.
                            </p>
                            <div class="links">
                                <b>Think you’re a good fit?</b>
                                <a href="javascript: scrollToSection('.talk-to-us-section', -10, '');" class="join-us">Join Us</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            
            <div class="section modes-of-management">
                <div class="container">
                    <div class="team-container">
                        <div class="container">
                            <div class="heading"><span>Meet our Team</span></div>
                            <div class="text">
                                Get to know more about our expertise and areas of specialization by selecting a profile below.
                            </div>
                            <div class="links orange">
                                <a href="javascript: scrollToSection('.footer', -10, '')" class="talk-to-us">Talk to us</a>
                            </div>
                            <div class="team-content">
                                <div class="team">
                                    <div class="each-team-member caroline-conway-thumbnail">
                                        <div class="introduction">
                                            <div class="name">Caroline Conway</div>
                                            <div class="designation">Founder & CEO</div>
                                        </div>
                                       <div class="member-container first-member">
                                            <div class="image">
                                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/about/caroline.png"  alt="Control Enter Team" />
                                            </div>
                                            <a href="https://www.linkedin.com/in/caroline-conway-683a22/" target="_blank">
                                                <div class="linkedin member1-linkein">
                                                    <i class="fa fa-linkedin"></i>
                                                </div>
                                            </a>
                                        </div>
<!--                                        <div class="image">
                                            <img src="<?php echo bloginfo( "template_directory" ) ?>/img/about/caroline.png" alt="Caroline Conway" />
                                        </div>-->
                                        <div class="view-bio links orange" onclick="viewBio('caroline-conway');"><a href="javascript: void(0);">View Bio</a></div>
                                     </div>
                                    <div class="each-team-member suresh-kumar-thumbnail">
                                        <div class="introduction">
                                            <div class="name">Suresh Kumar</div>
                                            <div class="designation">CTO & Asia President</div>
                                        </div>
                                        <div class="member-container">
                                            <div class="image">
                                                <img src="<?php echo bloginfo( "template_directory" ) ?>/img/about/suresh.png" alt="Suresh Kumar" />
                                            </div>
                                            <a href="https://www.linkedin.com/in/telecomtech/" target="_blank">
                                                <div class="linkedin member1-linkein">
                                                    <i class="fa fa-linkedin"></i>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="view-bio links orange" onclick="viewBio('suresh-kumar');"><a href="javascript: void(0);">View Bio</a></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="team-description">
                                    <div class="description caroline-conway">
                                        <div class="close-button" onclick="hideBio('caroline-convay');"><img src="<?php echo bloginfo( "template_directory" ) ?>/img/about/cross.png" alt="Close Icon" /></div>
                                        <div class="image">
                                            <img src="<?php echo bloginfo( "template_directory" ) ?>/img/about/caroline.png" alt="Caroline Conway" />
                                        </div>
                                        <div class="content">
                                            <div class="display-table">
                                                <div class="vertical-align middle">
                                                    <div class="name">
                                                        Caroline Conway
                                                        <span>Founder & CEO</span>
                                                    </div>
                                                    <div class="linkedin">
                                                        <a href="https://www.linkedin.com/in/caroline-conway-683a22/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                    </div>
                                                    <div class="email">
                                                        <a href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/about/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="text">
                                                        Caroline has 15+ years of retail, manufacturing, and energy experience at Global 500 enterprises and small firms.  At Walmart over the past 5 years, she developed and led an internal consulting team that delivered strategy, analytics, and process tools and solutions to all functions across the company. She holds degrees in design, business, and environmental analytics from the University of Michigan and Carnegie Mellon, bringing a multidisciplinary perspective to the team.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    
                                    <div class="description suresh-kumar">
                                        <div class="close-button" onclick="hideBio('suresh-kumar');"><img src="<?php echo bloginfo( "template_directory" ) ?>/img/about/cross.png" alt="Close Icon" /></div>
                                        <div class="image">
                                            <img src="<?php echo bloginfo( "template_directory" ) ?>/img/about/suresh.png" alt="Suresh Kumar" />
                                        </div>
                                        <div class="content">
                                            <div class="display-table">
                                                <div class="vertical-align middle">
                                                    <div class="name">
                                                        Suresh Kumar
                                                        <span>CTO & Asia President</span>
                                                    </div>
                                                    <div class="linkedin">
                                                        <a href="https://www.linkedin.com/in/telecomtech/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                    </div>
                                                    <div class="email">
                                                        <a href="mailto:suresh@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/about/email.png" alt="Email Icon" />suresh@controlentergroup.com</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="text">
                                                        Suresh has 24+ years of telecom, transportation, and healthcare experience at Global 500 enterprises and unicorn startups.  He has worked in customer and operations roles at AT&T, Telcordia Technologies, TWA, T-Systems International, Ola Cabs, and several early stage analytics startups. Suresh has training in business and technology management and has successfully managed client relationships, networks and operations around the world.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
            function viewBio(viewBio) {
                $('.description').hide();
                $('.' + viewBio).show();
//                $('.team-container .team .each-team-member .introduction').hide();
                $('.team-container .team').hide();
            }
            function hideBio(hideBio) {
                $('.description').hide();
                $('.' + hideBio).hide();
//                $('.team-container .team .each-team-member .introduction').show();
                $('.team-container .team').show();
            }
            $(document).ready(function() {
                $('#talk_to_us_now_button').click(function() {
                    var error = 'Error:\n';
                    if ($('#message').val() == '') { 
                        $('#message').focus();
                        $('.error-msg').show();
                        error = '';
                    }
                    else {
                        $('.error-msg').hide();
                    }
                    if (error == 'Error:\n') {
                        $('.loader').show();
                        $.post('<?php echo bloginfo( "template_directory" ); ?>/submit-about.php', $('#message').serialize(), function (response) {
                            $('#message').val('');
                            $('#success_talk_to_us_now').html(response);
                            $('#success_talk_to_us_now').slideDown(); // .delay(50000).hide('slow');
                            $('.loader').hide();
                        });
                    }
               });
            });
        </script>
    </body>
</html>