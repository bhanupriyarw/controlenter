<?php $this_page = 'about'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>About | Control Enter</title>
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <div class="body about-page">
            <div class="section banner">
                <div class="image">
                    <img src="<?php echo bloginfo( "template_directory" ); ?>/img/about/control-enter-about-banner.png" alt="About Control Enter" />
                </div>
                <div class="container">
                    <div class="content">
                        <div class="links orange">
                            <a href="javascript: scrollToSection('.talk-to-us', 120, '<?php echo bloginfo('url'); ?>/contact-us/');">Talk to us</a>
                        </div>
                        <div class="text">
                            We value strong talent that is collectively focused on coming up with the best solutions for your business.  Our vision, mission, values and hiring are the foundation for our delivery of exceptional results.
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="introduction section">
                <div class="container">
                    <div class="content">
                        <div class="text">
                            <span>Our Vision:</span>
                            We are passionate about advancing problem-solving to the next level by bringing the best of artificial and human intelligence together in long-term, sustainable solutions.
                        </div>
                        <div class="text">
                            <span>Our Mission:</span>
                            <ul>
                                <li>We help companies around the world advance their decision making through blended strategy, science, and design.</li>
                                <li>We create measurable financial value through sustainable product and service solutions.</li>
                                <li>We are resourceful and tenacious in getting to the right answer to complex problems.</li>
                                <li>We are fearless in experimenting and innovating to drive future solutions.</li>
                                <li>We are trusted partners who empower our clients and take care of our team.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="skills section">
                <div class="container">
                    <div class="heading"><span>Our Talent</span></div>
                    <div class="content">
                        <div class="heading">
                            Generalist Skills
                        </div>
                        <div class="heading vertical">Specialist Skills</div>
                        <div class="text">
                            <div class="heading">
                                T is for TEAM
                            </div>
                            <p>
                                We hire talent with exceptional generalist and cross-functional problem-solving skills on top of a solid technical foundation. Team members typically come from foundational backgrounds in analytics, data science, engineering, computer science, business, and design.
                            </p>
                            <div class="links">
                                <b>Think you’re a good fit?</b>
                                <a href="javascript: scrollToSection('.talk-to-us', -10, '<?php echo bloginfo('url'); ?>/contact-us/');">Join Us</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="listing">
                        <ul>
                            <li>
                                <div class="head">Growth</div>
                                <div class="text">Continuously learning and developing across disciplines</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Accountability</div>
                                <div class="text">Owning the business problem, beginning to end, and delivering the best solution</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Creativity</div>
                                <div class="text">Experimenting and taking calculated risks to add value</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Partnership</div>
                                <div class="text">Guiding problem-solving as a partner through inquiry</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Delivery</div>
                                <div class="text">Passionately delivering effort and quality to help the team and client succeed</div>
                                <div class="clearfix"></div>
                            </li>
                            <li>
                                <div class="head">Equality</div>
                                <div class="text">Mutually respecting and recognizing contributions</div>
                                <div class="clearfix"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            
            <div class="section modes-of-management">
                <div class="container">
                    <div class="team-container">
                        <div class="container">
                            <div class="heading"><span>Meet our Team</span></div>
                            <div class="text">
                                Get to know more about our expertise and areas of specialization by selecting a profile below.
                            </div>
<!--                            <div class="form">
                                <div class="form-field">
                                    <textarea name="message" rows="4" id="message" class="form-field message" value="" placeholder="Reach out if you would like to partner with us or have a problem that needs to be solved."></textarea>
                                    <span class="error-msg" style="display: none;">Enter your question</span>
                                </div>
                                <div class="submit">
                                    <div class="links">
                                        <a href="javascript: void(0);" class="talk-to-us-now-button" id="talk_to_us_now_button">Talk to Us</a>
                                    </div>
                                    <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="success" id="success_talk_to_us_now"></div>
                            </div>-->
                            <div class="links orange">
                                <a href="javascript: scrollToSection('.talk-to-us', -10, '<?php echo bloginfo('url'); ?>/contact-us/');">Talk to us</a>
                            </div>
                            <div class="team">
                                <div class="each-team-member right">
                                    <div class="image" style="background-image: url('<?php echo bloginfo( "template_directory" ); ?>/img/about/team-member1.jpg');"></div>
                                    <div class="name">Caroline Conway</div>
                                    <div class="designation">Founder & CEO</div>
                                    <div class="description">
                                        <div class="name">
                                            Caroline Conway
                                            <span>Founder & CEO</span>
                                        </div>
                                        <div class="email">
                                            <a href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                        </div>
                                        <div class="text">
                                            Caroline has 15+ years of retail, manufacturing, and energy experience at Global 500 enterprises and small firms.  At Walmart over the past 5 years, she developed and led an internal consulting team that delivered strategy, analytics, and process tools and solutions to all functions across the company. She holds degrees in design, business, and environmental analytics from the University of Michigan and Carnegie Mellon, bringing a multidisciplinary perspective to the team.
                                        </div>
                                        <div class="linkedin">
                                            <a href="https://www.linkedin.com/in/telecomtech/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="each-team-member right">
                                    <div class="image" style="background-image: url('<?php echo bloginfo( "template_directory" ); ?>/img/about/team-member2.jpg');"></div>
                                    <div class="name">Suresh Kumar</div>
                                    <div class="designation">CTO & Asia President</div>
                                    <div class="description">
                                        <div class="name">
                                            Suresh Kumar
                                            <span>CTO & Asia President</span>
                                        </div>
                                        <div class="email">
                                            <a href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                        </div>
                                        <div class="text">
                                            Suresh has 24+ years of telecom, transportation, and healthcare experience at Global 500 enterprises and unicorn startups.  He has worked in customer and operations roles at AT&T, Telcordia Technologies, TWA, T-Systems International, Ola Cabs, and several early stage analytics startups. Suresh has training in business and technology management and has successfully managed client relationships, networks and operations around the world.
                                        </div>
                                        <div class="linkedin">
                                            <a href="javascript: void(0);"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="each-team-member left">
                                    <div class="image" style="background-image: url('<?php echo bloginfo( "template_directory" ); ?>/img/about/team-member3.jpg');"></div>
                                    <div class="name">Caroline is Control Enter's Founder & CEO.</div>
                                    <div class="description">
                                        <div class="name">
                                            Caroline Conway
                                            <span>Founder & CEO</span>
                                        </div>
                                        <div class="email">
                                            <a href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                        </div>
                                        <div class="text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum ac metus eget vehicula. Sed vitae eleifend est. Morbi porta aliquet massa, in ultrices libero posuere sit amet. Praesent neque mi, iaculis eget consequat interdum, fringilla aliquam quam.Praesent neque mi.
                                        </div>
                                        <div class="linkedin">
                                            <a href="javascript: void(0);"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>-->
<!--                                <div class="each-team-member right">
                                    <div class="image" style="background-image: url('<?php echo bloginfo( "template_directory" ); ?>/img/about/team-member4.jpg');"></div>
                                    <div class="name">Founder & CEO.</div>
                                    <div class="description">
                                        <div class="name">
                                            Caroline Conway
                                            <span>is Control Enter's Founder & CEO</span>
                                        </div>
                                        <div class="email">
                                            <a href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                        </div>
                                        <div class="text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum ac metus eget vehicula. Sed vitae eleifend est. Morbi porta aliquet massa, in ultrices libero posuere sit amet. Praesent neque mi, iaculis eget consequat interdum, fringilla aliquam quam.Praesent neque mi.
                                        </div>
                                        <div class="linkedin">
                                            <a href="javascript: void(0);"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="each-team-member right">
                                    <div class="image" style="background-image: url('<?php echo bloginfo( "template_directory" ); ?>/img/about/team-member5.jpg');"></div>
                                    <div class="name">Caroline is Control Enter's Founder & CEO.</div>
                                    <div class="description">
                                        <div class="name">
                                            Caroline Conway
                                            <span>Founder & CEO</span>
                                        </div>
                                        <div class="email">
                                            <a href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                        </div>
                                        <div class="text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum ac metus eget vehicula. Sed vitae eleifend est. Morbi porta aliquet massa, in ultrices libero posuere sit amet. Praesent neque mi, iaculis eget consequat interdum, fringilla aliquam quam.Praesent neque mi.
                                        </div>
                                        <div class="linkedin">
                                            <a href="javascript: void(0);"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="each-team-member left">
                                    <div class="image" style="background-image: url('<?php echo bloginfo( "template_directory" ); ?>/img/about/team-member6.jpg');"></div>
                                    <div class="name">Caroline is Control Enter's Founder & CEO.</div>
                                    <div class="description">
                                        <div class="name">
                                            Caroline Conway
                                            <span>Founder & CEO</span>
                                        </div>
                                        <div class="email">
                                            <a href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                        </div>
                                        <div class="text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas interdum ac metus eget vehicula. Sed vitae eleifend est. Morbi porta aliquet massa, in ultrices libero posuere sit amet. Praesent neque mi, iaculis eget consequat interdum, fringilla aliquam quam.Praesent neque mi.
                                        </div>
                                        <div class="linkedin">
                                            <a href="javascript: void(0);"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
            $(document).ready(function() {
                $('#talk_to_us_now_button').click(function() {
                    var error = 'Error:\n';
                    if ($('#message').val() == '') { 
                        $('#message').focus();
                        $('.error-msg').show();
                        error = '';
                    }
                    else {
                        $('.error-msg').hide();
                    }
                    if (error == 'Error:\n') {
                        $('.loader').show();
                        $.post('<?php echo bloginfo( "template_directory" ); ?>/submit-about.php', $('#message').serialize(), function (response) {
                            $('#message').val('');
                            $('#success_talk_to_us_now').html(response);
                            $('#success_talk_to_us_now').slideDown(); // .delay(50000).hide('slow');
                            $('.loader').hide();
                        });
                    }
               });
            });
        </script>
    </body>
</html>