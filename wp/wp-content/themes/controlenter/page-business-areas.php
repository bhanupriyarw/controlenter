<?php $this_page = 'business-areas'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Business Areas | Control Enter</title>
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <div class="body business-areas-page">
            <div class="section banner">
                <div class="image">
                    <img class="hidden-xs" src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/control-enter-business-areas-banner.jpg" alt="About Control Enter" />
                    <img class="visible-xs" src="<?php echo bloginfo( "template_directory" ); ?>/img/responsive-imgs/control-enter-business-areas-banner.jpg" alt="About Control Enter" />
                    <div class="layer visible-md"></div>
                </div>
                <div class="container">
                    <div class="content">
                        <div class="text">
                            Our experienced team has delivered world-class solutions to Global 500 companies, unicorn startups, and small firms across multiple geographies, business functions, and industries.
                        </div>
                    </div>
                </div>
                <div class="next-section visible-xs"><a href="javascript: scrollToSection('.section.applications', 30, '');"><img src="<?php echo bloginfo( "template_directory" ) ?>/img/responsive-imgs/downarrowblue.png" alt="Down Arrow" /></a></div>
            </div>
            
            <div class="section applications">
                <div class="container">
                    <div class="heading"><span>Applications</span></div>
                    <div class="text">
                        We can help your business control performance and enter new growth areas by helping you better respond to your customers, achieve operational and supplier excellence, innovate for the future, and integrate strategies across your organization.
                    </div>
                    <div class="modes">
                        <div class="each-mode first-mode">
                            <div class="icon">
                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/applications/strategy.svg" alt="Icon" />
<!--                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="85" height="86" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 102 87" style="enable-background:new 0 0 102 87;" xml:space="preserve">
                               <style type="text/css">
                                       .st0{fill:#1F3863;stroke:#1F3863;stroke-miterlimit:10;}
                               </style>
                               <g>
                                       <path class="st0" d="M21.3,11.8l-9.9,9.9c-0.1,0.1-0.1,0.3,0,0.5c0.1,0.1,0.2,0.1,0.2,0.1s0.2,0,0.2-0.1l9.9-9.9l10.4,10.4
                                               c0.1,0.1,0.2,0.1,0.2,0.1c0.1,0,0.2,0,0.2-0.1c0.1-0.1,0.1-0.3,0-0.5L22.3,11.8L33.6,0.6c0.1-0.1,0.1-0.3,0-0.5
                                               c-0.1-0.1-0.3-0.1-0.5,0L21.8,11.4L11.4,0.9c-0.1-0.1-0.3-0.1-0.5,0c-0.1,0.1-0.1,0.3,0,0.5L21.3,11.8z"/>
                                       <path class="st0" d="M93.8,62.4c-0.1-0.1-0.3-0.1-0.5,0L82.1,73.7L71.6,63.3c-0.1-0.1-0.3-0.1-0.5,0c-0.1,0.1-0.1,0.3,0,0.5
                                               l10.4,10.4l-9.9,9.9c-0.1,0.1-0.1,0.3,0,0.5c0.1,0.1,0.2,0.1,0.2,0.1c0.1,0,0.2,0,0.2-0.1l9.9-9.9L92.5,85c0.1,0.1,0.2,0.1,0.2,0.1
                                               s0.2,0,0.2-0.1c0.1-0.1,0.1-0.3,0-0.5L82.5,74.2l11.3-11.3C93.9,62.8,93.9,62.5,93.8,62.4z"/>
                                       <path class="st0" d="M23.4,59.3V41.8h57.5c0.2,0,0.3-0.2,0.3-0.3V10.6l8.4,8.4c0.1,0.1,0.2,0.1,0.2,0.1s0.2,0,0.2-0.1
                                               c0.1-0.1,0.1-0.3,0-0.5l-9.6-9.6c-0.1-0.1-0.3-0.1-0.5,0l-9.7,9.7c-0.1,0.1-0.1,0.3,0,0.5c0.1,0.1,0.2,0.1,0.2,0.1s0.2,0,0.2-0.1
                                               L80,9.7l0.3,0.3c0,0.1-0.1,0.1-0.1,0.2v31H23.1c-0.2,0-0.3,0.2-0.3,0.3v17.8c-0.2,0-0.4,0-0.6,0c-7.7,0-14,6.2-14,13.9
                                               S14.4,87,22.1,87s14-6.2,14-13.9C36.2,65.9,30.6,59.9,23.4,59.3z M22.1,86.3c-7.4,0-13.4-5.9-13.4-13.2c0-7.3,6-13.2,13.4-13.2
                                               c0.2,0,0.4,0,0.6,0c0,0.1,0.2,0.2,0.3,0.2c0.1,0,0.2-0.1,0.3-0.2c6.8,0.6,12.1,6.3,12.1,13.1C35.5,80.4,29.5,86.3,22.1,86.3z"/>
                               </g>
                               </svg>-->
                            </div>
                            <div class="content">
                                <div class="heading">Strategy</div>
                                <div class="text">
                                    <p>Integrate and prioritize strategic action through systems thinking</p>
                                    <div class="more">More</div>
                                </div>
                                <div class="listing">
                                    <ul>
                                        <li>Understand financial performance connections and levers</li>
                                        <li>Assess strategic initiatives accurately</li>
                                        <li>Align stakeholders based on common analysis</li>
                                        <li>Build organizational decision making capabilities</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="each-mode second-mode">
                            <div class="icon">
                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/applications/customer-experience.svg" alt="Icon" />
<!--                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="107" height="87" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 102 87" style="enable-background:new 0 0 102 87;" xml:space="preserve">
                                   <style type="text/css">
                                           .st0{fill:#1F3863;}
                                   </style>
                                   <g id="customerexperience.svg">
                                           <path class="st0" d="M12.7,86.9l-0.1,0c-0.6-0.2-1.2-0.8-1.1-1.6L13,69.6H6.8c-3.6,0-6.6-3-6.6-6.6V28.5c0-3.6,3-6.6,6.6-6.6h31.5
                                                   V6.7c0-3.6,3-6.6,6.6-6.6H95c3.6,0,6.6,3,6.6,6.6v34.6c0,3.6-3,6.6-6.6,6.6h-6.2l1.6,15.7c0.1,0.7-0.3,1.4-1.1,1.5
                                                   c-0.4,0-0.8-0.1-1.2-0.4l-17-16.9h-7.6V63c0,3.6-3,6.6-6.6,6.6H30.7l-17,16.9C13.4,86.8,13.1,86.9,12.7,86.9z M6.8,22.9
                                                   c-3.1,0-5.6,2.5-5.6,5.6V63c0,3.1,2.5,5.6,5.6,5.6h7.4l-1.7,16.8c0,0.2,0.1,0.4,0.3,0.4c0.1,0,0.2-0.1,0.3-0.2l17.2-17.1H57
                                                   c3.1,0,5.6-2.5,5.6-5.6V47.9H44.9c-3.6,0-6.6-3-6.6-6.6V22.9H6.8z M63.6,46.9h8L88.9,64c0.1,0.1,0.3,0.2,0.3,0.2
                                                   c0,0,0.3-0.1,0.2-0.4l-1.8-16.9H95c3.1,0,5.6-2.5,5.6-5.6V6.7c0-3.1-2.5-5.6-5.6-5.6H44.9c-3.1,0-5.6,2.5-5.6,5.6v15.2H57
                                                   c3.6,0,6.6,3,6.6,6.6V46.9z M39.3,22.9v18.4c0,3.1,2.5,5.6,5.6,5.6h17.7V28.5c0-3.1-2.5-5.6-5.6-5.6H39.3z"/>
                                   </g>
                                </svg>-->
                            </div>
                            <div class="content">
                                <div class="heading">End Customer</div>
                                <div class="text">
                                    <p>Maximize revenue and profit by acting on customer insights</p>
                                    <div class="more">More</div>
                                </div>
                                <div class="listing">
                                    <ul>
                                        <li>Understand your customer better</li>
                                        <li>Forecast and plan accurately</li>
                                        <li>Improve customer experience</li>
                                        <li>Decide on right products, prices, promotions, and placement</li>
                                        <li>Enhance product development and R&D</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="each-mode third-mode">
                            <div class="icon">
                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/applications/operational-performance.svg" alt="Icon" />
<!--                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="76" height="87" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 76 76" style="enable-background:new 0 0 76 76;" xml:space="preserve">
                               <style type="text/css">
                                       .st0{fill:#1F3863;}
                               </style>
                               <g>
                                       <path class="st0" d="M69.1,17.4l0.1-0.1l-0.1-0.2c-2.3-3.3-5.3-6.3-8.5-8.8C54,3.3,46.2,0.7,38,0.7C17.4,0.7,0.7,17.4,0.7,38
                                               c0,20.6,16.7,37.3,37.3,37.3S75.3,58.6,75.3,38C75.3,30.6,73.2,23.5,69.1,17.4z M38,74.1C18.1,74.1,1.9,57.9,1.9,38
                                               c0-7.2,2.1-14.2,6.2-20.2l6.2,4.5l0.1-0.2c2.2-3.1,4.8-5.8,7.6-7.6c5.1-3.5,10.8-5.4,16.5-5.4c9.4,0,18.2,4.8,24,13.3l0.2,0.2
                                               l5.5-4.5c3.9,5.9,6,12.8,6,19.8C74.1,57.9,57.9,74.1,38,74.1z M62.8,20.8C56.8,12.6,47.9,8,38.5,8c-6,0-11.9,1.9-17.1,5.5
                                               c-2.7,1.9-5.2,4.3-7.4,7.2l-5.2-3.7C15.6,7.6,26.6,2.1,38.2,2.1c5.4,0,10.7,1.2,15.6,3.5c5.4,2.6,10.1,6.6,13.6,11.6L62.8,20.8z"/>
                                       <path class="st0" d="M39.8,36c-0.5-0.4-1.1-0.7-1.8-0.7c-1.6,0-2.7,1.2-2.7,2.7c0,1.6,1.2,2.7,2.7,2.7s2.7-1.2,2.7-2.7
                                               c0-0.4-0.1-0.7-0.2-1.1l14.3-10.7l0.2-0.1l-0.7-1L39.8,36z M38,39.5c-0.9,0-1.5-0.6-1.5-1.5s0.6-1.5,1.5-1.5c0.8,0,1.5,0.7,1.5,1.5
                                               C39.5,38.9,38.9,39.5,38,39.5z"/>
                               </g>
                               </svg>-->
                            </div>
                            <div class="content">
                                <div class="heading">Operational Performance</div>
                                <div class="text">
                                    <p>Transform and sustain performance throughout your organization</p>
                                    <div class="more">More</div>
                                </div>
                                <div class="listing">
                                    <ul>
                                        <li>Assess P&L performance and address untapped costs</li>
                                        <li>Improve operational effectiveness</li>
                                        <li>Optimize your supply chain</li>
                                        <li>Predict and control sourcing costs</li>
                                        <li>Improve workforce productivity at head office and in field</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="each-mode fourth-mode">
                            <div class="icon">
                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/applications/innovation.svg" alt="Icon" />
<!--                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="59" height="87" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 59 87" style="enable-background:new 0 0 59 87;" xml:space="preserve">
                               <style type="text/css">
                                       .st0{fill:#1F3863;}
                               </style>
                               <g id="innovation.svg">
                                       <path class="st0" d="M38.3,72.4H20.4c-3.3,0-5.9-2.5-5.9-5.8v-8.5l-0.7-1C8.5,49.9,1.2,40,1.2,30.2c0-7.5,3-14.6,8.3-19.9
                                               C14.8,5,21.9,2.1,29.3,2.1c0.1,0,0.2,0,0.4,0c7.4,0,14.4,2.9,19.7,8.1c5.3,5.3,8.3,12.4,8.3,19.9c0,10-8,20.6-12.7,26.9l-0.8,1.1
                                               v8.5C44.2,69.8,41.6,72.4,38.3,72.4z M15.5,58.4v8.2c0,2.7,2.2,4.8,4.9,4.8h17.9c2.7,0,4.9-2.2,4.9-4.8v-8.2H15.5z M15.3,57.4h28.2
                                               l0.7-1c4.7-6.2,12.5-16.7,12.5-26.3c0-7.2-2.8-14-8-19.1c-5.1-5.1-12-7.9-19.2-7.9c0,0,0,0,0,0c-0.1,0-0.1,0-0.2,0
                                               c-7.2,0-14,2.8-19.1,7.9c-5.1,5.1-8,11.9-8.1,19.2c0,9.5,7.2,19.2,12.4,26.3L15.3,57.4z M29,45.7c-0.1,0-0.3,0-0.4-0.1l-8.5-8.5
                                               c-0.2-0.2-0.2-0.5,0-0.7s0.5-0.2,0.7,0l8.1,8.1l8.2-8.2c0.2-0.2,0.5-0.2,0.7,0s0.2,0.5,0,0.7l-8.6,8.6C29.3,45.7,29.1,45.7,29,45.7
                                               z"/>
                               </g>
                               <g>
                                       <path class="st0" d="M39.5,75.8h-20c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h20c0.3,0,0.5,0.2,0.5,0.5S39.8,75.8,39.5,75.8z"/>
                               </g>
                               <g>
                                       <path class="st0" d="M37.7,78.2H21.3c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h16.4c0.3,0,0.5,0.2,0.5,0.5S38,78.2,37.7,78.2z"/>
                               </g>
                               <g>
                                       <path class="st0" d="M35.1,80.7H23.9c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h11.2c0.3,0,0.5,0.2,0.5,0.5S35.4,80.7,35.1,80.7z"/>
                               </g>
                               </svg>-->
                            </div>
                            <div class="content">
                                <div class="heading">Innovation</div>
                                <div class="text">
                                    <p>Discover robust future insights and big impact ideas</p>
                                    <div class="more">More</div>
                                </div>
                                <div class="listing">
                                    <ul>
                                        <li>Forecast and assess future scenarios</li>
                                        <li>Optimize future state of business to achieve specific goals</li>
                                        <li>Successfully integrate new technology and data</li>
                                        <li>Deploy innovation based on long term market forces</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            
            <div class="section modes-of-management">
                <div class="container">
                    <div class="listing">
                        <div class="text">
                            Please reach out to learn more about case studies and what we can do for your industry or function. 
                        </div>
                        <div class="links">
                            <a href="javascript: scrollToSection('.footer', -10, '')" class="talk-to-us">Talk to us</a>
                        </div>
                        <div class="industries">
                            <div class="heading"><span>Industries</span></div>
                            <ul>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/finance-insurance.png" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="41" height="45" viewBox="0 0 41 45">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="financeandinsurance.svg" class="cls-1" d="M1267.81,3288.48a12.606,12.606,0,0,0,6.19-10.79,13.012,13.012,0,0,0-24.9-4.96,14.009,14.009,0,0,0-3.12-.35,12.667,12.667,0,1,0-.42,25.33,12.965,12.965,0,0,0,25.91-.4A12.253,12.253,0,0,0,1267.81,3288.48Zm-6.73-22.67a11.838,11.838,0,0,1,6.02,22.21,0.3,0.3,0,0,0-.23.11v0.06a12.194,12.194,0,0,1-17.94-10.5,11.507,11.507,0,0,1,.82-4.27,0.053,0.053,0,0,1,.06-0.05h0A12.007,12.007,0,0,1,1261.08,3265.81Zm-27.26,19.21a12.1,12.1,0,0,1,14.99-11.54,12.446,12.446,0,0,0-.71,4.21,12.739,12.739,0,0,0,9.03,12.06A12.205,12.205,0,0,1,1233.82,3285.02Zm24.73,24.17a12.094,12.094,0,0,1-12.16-11.48,13.289,13.289,0,0,0,8.97-3.86l3.01,3.98v1.32h-2.07a0.406,0.406,0,0,0-.41.41,0.4,0.4,0,0,0,.41.4h2.07v1.27h-2.07a0.405,0.405,0,1,0,0,.81h2.07v2.77a0.4,0.4,0,0,0,.41.4,0.391,0.391,0,0,0,.41-0.4v-2.77h2.01a0.405,0.405,0,1,0,0-.81h-2.01v-1.27h2.01a0.391,0.391,0,0,0,.41-0.4,0.4,0.4,0,0,0-.41-0.41h-2.01v-1.27l3.25-4.21a0.423,0.423,0,0,0-.06-0.57,0.458,0.458,0,0,0-.59.05l-3.01,3.87-2.89-3.81a12.538,12.538,0,0,0,2.01-3.23,14.033,14.033,0,0,0,3.12.4,12.925,12.925,0,0,0,6.02-1.44,11.757,11.757,0,0,1,3.6,8.43A11.9,11.9,0,0,1,1258.55,3309.19Zm-12.57-16.9a6.554,6.554,0,0,0,2.36-.41,0.371,0.371,0,0,0,.23-0.51,0.4,0.4,0,0,0-.53-0.24,5.987,5.987,0,0,1-2.06.35,6.608,6.608,0,0,1-6.55-5.54h6.07a0.4,0.4,0,0,0,.42-0.4,0.408,0.408,0,0,0-.42-0.41h-6.13v-0.11c0-.35.06-0.69,0.06-1.04h6.07a0.4,0.4,0,0,0,.42-0.4,0.408,0.408,0,0,0-.42-0.41h-5.9a6.651,6.651,0,0,1,6.32-4.61,0.406,0.406,0,0,0,.41-0.41,0.4,0.4,0,0,0-.41-0.4,7.373,7.373,0,0,0-7.14,5.42h-1.65a0.408,0.408,0,0,0-.42.41,0.4,0.4,0,0,0,.42.4h1.47a6.05,6.05,0,0,0-.06,1.04v0.11h-1.41a0.408,0.408,0,0,0-.42.41,0.4,0.4,0,0,0,.42.4h1.47A7.443,7.443,0,0,0,1245.98,3292.29Zm14.51-8.48h0.18v1.84a0.415,0.415,0,0,0,.83,0v-1.84c2.12-.23,3.24-1.56,3.24-2.83,0.06-1.33-.82-2.88-3.24-3.46v-4.9c1.82,0.17,2.65,1.21,2.59,2.19a0.415,0.415,0,0,0,.83,0c0.06-1.33-1-2.83-3.42-3v-1.85a0.415,0.415,0,0,0-.83,0v1.85c-2.42.11-3.72,1.5-3.72,2.88-0.06,1.39.95,3.06,3.72,3.52v4.85h-0.12a3.543,3.543,0,0,1-2.3-.75,2.081,2.081,0,0,1-.65-1.44,0.4,0.4,0,0,0-.41-0.41,0.394,0.394,0,0,0-.41.41,2.64,2.64,0,0,0,.88,2.01A4.352,4.352,0,0,0,1260.49,3283.81Zm-2.77-9.12c0-.98,1.06-2.02,2.95-2.07v4.78C1258.78,3277.06,1257.66,3276.02,1257.72,3274.69Zm3.72,3.64c1.59,0.4,2.48,1.38,2.48,2.59,0,0.93-.89,1.85-2.48,2.08v-4.67Z" transform="translate(-1233 -3265)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Finance and Insurance
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/govtandngo.svg" alt="Icon" />-->
                                               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="35" height="40" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 36 39" style="enable-background:new 0 0 36 39;" xml:space="preserve">
                                               <style type="text/css">
                                                       .st0{fill:#1F3863;}
                                               </style>
                                               <g>
                                                       <g>
                                                               <path class="st0" d="M34.2,14.2H1.9c-0.2,0-0.5-0.2-0.5-0.5c-0.1-0.2,0-0.5,0.2-0.7L17.6,0.5c0.3-0.2,0.6-0.2,0.8,0l16.1,12.7
                                                                       c0.2,0.2,0.3,0.5,0.2,0.7C34.7,14.1,34.5,14.2,34.2,14.2z M1.9,13.8h32.3c0,0,0,0,0.1,0v-0.1l0,0c0,0,0-0.1-0.1-0.1L18.1,0.9
                                                                       c0,0-0.1,0-0.2,0L1.8,13.5c0,0,0,0,0,0.1l0,0L1.9,13.8C1.8,13.7,1.9,13.8,1.9,13.8z"/>
                                                       </g>
                                                       <g>
                                                               <path class="st0" d="M35.2,38.6H0.7c-0.2,0-0.5-0.2-0.5-0.5v-2.6c0-0.3,0.3-0.5,0.6-0.5H3v-0.8c0-0.3,0.3-0.5,0.6-0.5h28.8
                                                                       c0.4,0,0.6,0.2,0.6,0.5V35h2.3c0.2,0,0.4,0.1,0.4,0.3c0,0.1,0,0.2,0,0.3v2.5C35.8,38.4,35.5,38.6,35.2,38.6z M0.8,35.4
                                                                       c-0.1,0-0.2,0-0.2,0.1v2.6c0,0,0.1,0.1,0.1,0.1h34.5c0.1,0,0.2,0,0.2-0.1v-2.7h-2.7v-1.2c0,0-0.1-0.1-0.2-0.1H3.7
                                                                       c-0.1,0-0.2,0-0.2,0.1v1.2H0.8z"/>
                                                       </g>
                                                       <g>
                                                               <path class="st0" d="M8,34.1H3.5V13.8H8V34.1z M4,33.7h3.5V14.2H4V33.7z"/>
                                                       </g>
                                                       <g>
                                                               <path class="st0" d="M20.6,34.1h-5.4V13.8h5.4V34.1z M15.7,33.7h4.4V14.2h-4.4V33.7z"/>
                                                       </g>
                                                       <g>
                                                               <path class="st0" d="M32.4,34.1h-4.5V13.8h4.5V34.1z M28.4,33.7h3.5V14.2h-3.5V33.7z"/>
                                                       </g>
                                               </g>
                                               </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Government and NGO
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/consumer-goods.svg" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="38" viewBox="0 0 28 38">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="consumergoods.svg" class="cls-1" d="M1273.5,3459.36v-21.67a0.38,0.38,0,0,0-.38-0.38h-7.19v-4.27a4.921,4.921,0,1,0-9.84,0v4.27h-7.2a0.378,0.378,0,0,0-.37.38v21.67l-1.51,6.16a0.391,0.391,0,0,0,.07.33,0.384,0.384,0,0,0,.3.15h27.26a0.417,0.417,0,0,0,.3-0.15,0.429,0.429,0,0,0,.07-0.33Zm-16.66-26.32a4.166,4.166,0,1,1,8.33,0v4.27h-8.33v-4.27Zm-8.97,32.18,1.39-5.72a0.277,0.277,0,0,0,.01-0.09v-21.33h23.47v21.33a0.292,0.292,0,0,0,.02.09l1.39,5.72h-26.28Zm13.14-12.75a5.5,5.5,0,1,1,5.37-5.49A5.436,5.436,0,0,1,1261.01,3452.47Zm0-10.26a4.766,4.766,0,1,0,4.65,4.77A4.713,4.713,0,0,0,1261.01,3442.21Zm9.03,18.52h-18.06a0.367,0.367,0,0,1-.36-0.37v-0.66a6.164,6.164,0,0,1,6.09-6.23h6.6a6.164,6.164,0,0,1,6.09,6.23v0.66A0.367,0.367,0,0,1,1270.04,3460.73Zm-17.7-.74h17.34v-0.29a5.445,5.445,0,0,0-5.37-5.5h-6.6a5.438,5.438,0,0,0-5.37,5.5v0.29Z" transform="translate(-1247 -3428)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Consumer Goods
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/healthcare.svg" alt="Icon" />
<!--                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="43.97" height="45" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 44 45" style="enable-background:new 0 0 44 45;" xml:space="preserve">
                                               <style type="text/css">
                                                       .st0{fill:#1F3863;}
                                               </style>
                                               <path id="healthacre.svg" class="st0" d="M11.7,30.8c1,0.1,2,0.2,2.9,0.2c0.9,0,1.8,0.1,2.8,0.2c3.3,0.4,7.5,0.8,11.1-3.1
                                                       s2.7-7.9,2-11c-0.1-0.6-0.2-1.2-0.4-1.7c-0.2,0-0.5,0-0.7,0l0,0c-0.1,0-0.3,0-0.4,0c0.1,0.6,0.3,1.2,0.4,1.9
                                                       c0.5,1.7,0.7,3.4,0.6,5.2l-8.8-7.7c1.8-0.3,3.6-0.3,5.4,0c1,0.1,2,0.2,2.9,0.2c0.9,0,1.8,0.1,2.8,0.2c3.3,0.4,7.5,0.8,11.1-3.1
                                                       c0.2-0.2,0.2-0.5,0-0.7c-0.2-0.2-0.5-0.1-0.7,0c-0.9,1-2.1,1.8-3.4,2.3L29,5c0.4-1.3,1.1-2.5,2-3.5c0.2-0.2,0.2-0.5,0-0.7
                                                       c-0.2-0.2-0.5-0.1-0.7,0c-3.6,3.9-2.7,7.9-2,11c0.1,0.6,0.2,1.1,0.4,1.7c0.2,0,0.5,0,0.7,0l0,0c0.1,0,0.3,0,0.4,0
                                                       c-0.1-0.6-0.3-1.2-0.4-1.9c-0.5-1.8-0.7-3.7-0.6-5.5l9.1,8c-1.9,0.4-3.8,0.4-5.7,0c-1-0.1-2-0.2-2.9-0.2c-0.9,0-1.8-0.1-2.8-0.2
                                                       c-3.3-0.4-7.5-0.8-11.1,3.1s-2.7,7.9-2,11c0.1,0.6,0.2,1.2,0.4,1.7c0.2,0,0.5,0,0.7,0c0.1,0,0.3,0,0.4,0c-0.1-0.6-0.3-1.2-0.4-1.9
                                                       c-0.5-1.7-0.7-3.4-0.6-5.2l8.9,7.8c-1.8,0.3-3.6,0.3-5.4,0c-1-0.1-2-0.2-2.9-0.2c-0.9,0-1.8-0.1-2.8-0.2C8.5,29.4,4.3,29,0.8,32.9
                                                       c-0.2,0.2-0.2,0.5,0,0.7c0.2,0.2,0.5,0.1,0.7,0c0.9-1.1,2.1-1.9,3.4-2.4l10,8.8c-0.4,1.3-1.1,2.6-2.1,3.5c-0.2,0.2-0.2,0.5,0,0.7
                                                       c0.2,0.2,0.5,0.1,0.7,0c3.6-3.9,2.7-7.9,2-11c-0.1-0.6-0.2-1.1-0.4-1.7c-0.2,0-0.5,0-0.7,0c-0.1,0-0.3,0-0.4,0
                                                       c0.1,0.6,0.2,1.2,0.4,1.9c0.5,1.8,0.7,3.7,0.6,5.5l-9.1-8C7.9,30.5,9.8,30.5,11.7,30.8z M30,23.7c-0.3,1.2-0.9,2.4-1.7,3.4
                                                       L16.6,16.9c0.9-0.9,2-1.6,3.2-2L30,23.7z M14,21.3c0.4-1.4,1-2.6,2-3.7l11.7,10.3c-1,1-2.2,1.8-3.6,2.3L14,21.3z"/>
                                               </svg>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Healthcare, Pharma and Biotech
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/industrial-manufacturing.svg" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="36" height="32" viewBox="0 0 36 32">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="manufacturing.svg" class="cls-1" d="M1237,3635.22V3652h36v-32h-6.17l-1.32,13.22h-5.8v-4.83l-6.65,4.41v-4.41l-6.64,4.41v-4.41Zm13.37,15.89v-4.99h4.38v4.99h-4.38Zm-12.68-15.49,7.78-5.5v4.41l6.64-4.41v4.41l6.65-4.41v4.04h7.54l1.32-13.22h4.48v30.17h-16.4v-5.88h-6.22v5.88h-11.6Z" transform="translate(-1237 -3620)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Industrial Manufacturing
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/materialsandcommodities-01.svg" alt="Icon" />
<!--                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        width="45px" height="39px" viewBox="0 0 45 39" style="enable-background:new 0 0 45 39;" xml:space="preserve">
                                               <path id="materialsandcommodities.svg" style="&st0;" d="M22.4,8.3c0.2,0,0.4-0.2,0.4-0.4l0,0V1.7c0-0.2-0.2-0.4-0.4-0.4l0,0h-0.1
                                                       c-0.2,0-0.4,0.2-0.4,0.4v6.1C22,8.1,22.2,8.3,22.4,8.3z M12.8,10.8c0.2,0.2,0.4,0.2,0.6,0l0,0c0.2-0.2,0.2-0.4,0-0.6l0,0l-4-4
                                                       c0-0.1-0.2-0.2-0.3-0.1l0,0c-0.2,0-0.4,0.3-0.4,0.5c0,0.1,0,0.2,0.1,0.3L12.8,10.8z M32.1,10.8l4-4c0.2-0.2,0.2-0.4,0-0.6l0,0
                                                       c-0.1-0.1-0.2-0.1-0.4-0.1c-0.1,0-0.2,0-0.3,0.1l-4,4c-0.2,0.2-0.2,0.4,0,0.6l0,0C31.6,11,31.9,11,32.1,10.8z M12.3,24.5h20.4
                                                       c0.3,0,0.6-0.3,0.6-0.6v-0.1l-2.2-11.2c-0.1-0.3-0.3-0.4-0.6-0.4h-16c-0.2,0.1-0.4,0.2-0.4,0.4l-2.2,11.2
                                                       C11.8,24.1,12,24.4,12.3,24.5z M29.9,13.3l2,10.1H13l2.1-10.1 M19.1,27.2c-0.1-0.3-0.3-0.5-0.6-0.4H2.7c-0.2,0.1-0.4,0.2-0.4,0.4
                                                       L0,38.3c-0.1,0.3,0.1,0.6,0.4,0.7l0,0h20.4c0.3,0,0.6-0.2,0.6-0.6v-0.1L19.1,27.2z M18,27.8l2,10.1H1.2l2-10.1 M45,38.3l-2.2-11.2
                                                       c0-0.3-0.3-0.5-0.6-0.4H26.3c-0.2,0-0.4,0.2-0.4,0.4l-2.2,11.2c-0.1,0.3,0.1,0.6,0.4,0.7l0,0h20.4c0.3,0,0.6-0.3,0.6-0.6L45,38.3
                                                       L45,38.3z M41.7,27.8l2,10.1H24.9l2-10.1 M17,8.9c-0.2,0-0.4-0.1-0.4-0.3l-2-5.7c-0.1-0.2,0-0.5,0.3-0.6c0.2-0.1,0.5,0,0.6,0.3
                                                       l2,5.7c0.1,0.2,0,0.5-0.3,0.6C17.1,8.9,17,8.9,17,8.9L17,8.9z M28.3,8.9c-0.1,0-0.1,0-0.2,0c-0.2-0.1-0.4-0.3-0.3-0.6l0,0l2-5.7
                                                       c0.1-0.2,0.3-0.4,0.6-0.3s0.4,0.3,0.3,0.6l-2,5.7C28.6,8.8,28.5,8.9,28.3,8.9z"/>
                                               </svg>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Primary Materials and Commodities
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/professional-services.svg" alt="Icon" />-->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="36" height="26" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 36 28" style="enable-background:new 0 0 36 28;" xml:space="preserve">
                                               <style type="text/css">
                                                       .st0{fill:#1F3863;}
                                               </style>
                                               <g>
                                                       <path class="st0" d="M2.1,25.4h31.6c0.4,0,0.7-0.3,0.9-0.5l0,0v-1.3c0-8.7-6.4-16.2-14.7-17.2c0.2-0.2,0.4-0.5,0.6-0.9
                                                               c0.3-0.7,0.3-1.5,0-2.1S19.7,2.3,19,1.9c-0.7-0.3-1.5-0.3-2.1,0c-1.1,0.4-1.7,1.4-1.7,2.5c0,0.6,0.2,1.3,0.7,1.8
                                                               c-8.1,1-14.4,8.5-14.4,17.2v1.2C1.5,25,1.7,25.3,2.1,25.4z M17,6.9c0.2,0,0.3-0.1,0.3-0.3c0.1-0.2,0-0.4-0.1-0.4l0,0
                                                               c-0.7-0.4-1.1-1-1.1-1.8s0.4-1.5,1.2-1.9c0.5-0.2,1-0.2,1.5,0c0.5,0.3,0.9,0.7,1,1.1c0.2,0.5,0.2,1,0,1.5C19.5,5.6,19,6,18.6,6.2
                                                               c-0.2,0.1-0.3,0.3-0.3,0.4c0,0.2,0.2,0.3,0.3,0.3l0,0c8.4,0.5,15,7.8,15,16.6v1.2H2.3c-0.1,0-0.2,0-0.2-0.1v-1.2
                                                               C2.1,14.7,8.8,7.3,17,6.9z"/>
                                                       <path class="st0" d="M35.5,25.8H0.5c-0.2,0-0.3,0.2-0.3,0.3s0.2,0.3,0.3,0.3h35.1c0.2,0,0.3-0.2,0.3-0.3S35.8,25.8,35.5,25.8z"/>
                                               </g>
                                               </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Professional Services
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/real-estate-and-infrastructure.svg" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="41" viewBox="0 0 23 41">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="realestate.svg" class="cls-1" d="M1268.3,3903.51v37.65h-4.26v-6.27a0.425,0.425,0,0,0-.43-0.42h-5.11a0.425,0.425,0,0,0-.43.42v6.27h-9.37v-37.65h19.6Zm-5.11,37.65h-4.26v-5.85h4.26v5.85Zm-15.34-22.47v22.47h-0.42a0.42,0.42,0,1,0,0,.84h22.14a0.42,0.42,0,1,0,0-.84h-0.42v-38.07a0.425,0.425,0,0,0-.43-0.42h-6.39v-1.25a0.425,0.425,0,0,0-.85,0v1.25h-13.2a0.425,0.425,0,0,0-.43.42v14.74m5.54,19.15a0.423,0.423,0,0,0,.42-0.42v-1.67a0.425,0.425,0,0,0-.85,0v1.67A0.425,0.425,0,0,0,1253.39,3936.98Zm0-6.69a0.423,0.423,0,0,0,.42-0.42v-1.68a0.425,0.425,0,0,0-.85,0v1.68A0.425,0.425,0,0,0,1253.39,3930.29Zm5.11,0a0.431,0.431,0,0,0,.43-0.42v-1.68a0.423,0.423,0,0,0-.43-0.41,0.417,0.417,0,0,0-.43.41v1.68A0.425,0.425,0,0,0,1258.5,3930.29Zm5.11,0a0.425,0.425,0,0,0,.43-0.42v-1.68a0.425,0.425,0,0,0-.85,0v1.68A0.423,0.423,0,0,0,1263.61,3930.29Zm-10.22-6.7a0.423,0.423,0,0,0,.42-0.42v-1.67a0.425,0.425,0,0,0-.85,0v1.67A0.425,0.425,0,0,0,1253.39,3923.59Zm5.11,0a0.431,0.431,0,0,0,.43-0.42v-1.67a0.431,0.431,0,0,0-.43-0.42,0.425,0.425,0,0,0-.43.42v1.67A0.425,0.425,0,0,0,1258.5,3923.59Zm5.11,0a0.425,0.425,0,0,0,.43-0.42v-1.67a0.425,0.425,0,0,0-.85,0v1.67A0.423,0.423,0,0,0,1263.61,3923.59Zm-10.22-6.69a0.423,0.423,0,0,0,.42-0.42v-1.67a0.425,0.425,0,0,0-.85,0v1.67A0.425,0.425,0,0,0,1253.39,3916.9Zm5.11,0a0.431,0.431,0,0,0,.43-0.42v-1.67a0.431,0.431,0,0,0-.43-0.42,0.425,0.425,0,0,0-.43.42v1.67A0.425,0.425,0,0,0,1258.5,3916.9Zm5.11,0a0.425,0.425,0,0,0,.43-0.42v-1.67a0.425,0.425,0,0,0-.85,0v1.67A0.423,0.423,0,0,0,1263.61,3916.9Zm-10.22-6.7a0.414,0.414,0,0,0,.42-0.41v-1.68a0.425,0.425,0,0,0-.85,0v1.68A0.417,0.417,0,0,0,1253.39,3910.2Zm5.11,0a0.423,0.423,0,0,0,.43-0.41v-1.68a0.431,0.431,0,0,0-.43-0.42,0.425,0.425,0,0,0-.43.42v1.68A0.417,0.417,0,0,0,1258.5,3910.2Zm5.11,0a0.417,0.417,0,0,0,.43-0.41v-1.68a0.425,0.425,0,0,0-.85,0v1.68A0.414,0.414,0,0,0,1263.61,3910.2Z" transform="translate(-1247 -3901)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Real Estate and Infrastructure
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/retail.svg" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="37" height="32" viewBox="0 0 37 32">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="retail.svg" class="cls-1" d="M1238.43,3994a0.42,0.42,0,1,0,0,.84h4.81l4.63,21.36a0.453,0.453,0,0,0,.46.54h21.52a0.425,0.425,0,1,0,0-.85h-21.17l-0.92-4.21h23.81a0.45,0.45,0,0,0,.42-0.32l3.01-12.64a0.445,0.445,0,0,0-.42-0.51h-29.74l-0.83-3.88a0.43,0.43,0,0,0-.42-0.33h-5.16Zm6.6,5.05h29.01l-2.81,11.79h-23.65Zm7.6,20.21a3.371,3.371,0,1,0,3.45,3.37A3.419,3.419,0,0,0,1252.63,4019.26Zm12.91,0a3.371,3.371,0,1,0,3.45,3.37A3.419,3.419,0,0,0,1265.54,4019.26Zm-12.91.84a2.53,2.53,0,1,1-2.58,2.53A2.55,2.55,0,0,1,1252.63,4020.1Zm12.91,0a2.53,2.53,0,1,1-2.58,2.53A2.559,2.559,0,0,1,1265.54,4020.1Z" transform="translate(-1238 -3994)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Retail
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/transportation-and-logistics.svg" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="34" height="41" viewBox="0 0 34 41">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="transport.svg" class="cls-1" d="M1273.99,4103.75a0.423,0.423,0,0,0-.53-0.3l-17.62,5.12-8.23-29.94a0.442,0.442,0,0,0-.15-0.23l-6.77-5.32a0.421,0.421,0,0,0-.6.09,0.442,0.442,0,0,0,.08.61l6.66,5.23,7.65,27.85a3.769,3.769,0,0,0-2.22-.74,3.684,3.684,0,0,0-1.04.15,3.867,3.867,0,0,0-2.31,1.87,4.065,4.065,0,0,0-.34,3,3.869,3.869,0,0,0,3.69,2.87h0a3.684,3.684,0,0,0,1.04-.15,3.941,3.941,0,0,0,2.73-4.43l17.66-5.14A0.441,0.441,0,0,0,1273.99,4103.75Zm-20.92,9.27a2.788,2.788,0,0,1-.81.12h0a3.005,3.005,0,0,1-2.87-2.24,3.144,3.144,0,0,1,.27-2.33,2.936,2.936,0,0,1,1.79-1.45,2.855,2.855,0,0,1,.82-0.12,3,3,0,0,1,2.86,2.23A3.079,3.079,0,0,1,1253.07,4113.02Zm2.62-15.2a0.506,0.506,0,0,0-.04.34l1.86,6.74a0.415,0.415,0,0,0,.41.32,0.448,0.448,0,0,0,.11-0.01l14.76-4.29a0.438,0.438,0,0,0,.29-0.55l-1.85-6.74a0.454,0.454,0,0,0-.2-0.27,0.4,0.4,0,0,0-.32-0.03l-14.76,4.29A0.433,0.433,0,0,0,1255.69,4097.82Zm14.84-3.53,1.62,5.9-13.94,4.05-1.62-5.9Zm-15.8.49a0.413,0.413,0,0,0,.4.32,0.487,0.487,0,0,0,.12-0.01l14.76-4.3a0.431,0.431,0,0,0,.29-0.54l-3.7-13.49a0.47,0.47,0,0,0-.21-0.27,0.393,0.393,0,0,0-.32-0.03l-14.76,4.29a0.375,0.375,0,0,0-.25.21,0.424,0.424,0,0,0-.04.33Zm11.16-17.36,3.48,12.65-13.94,4.05-3.48-12.65Z" transform="translate(-1240 -4073)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Transportation and Logistics
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/technology-media-and-telecom.svg" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="44.97" height="45" viewBox="0 0 44.97 45">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="techmediatelecom.svg" class="cls-1" d="M1235.15,4200.83l9.91-9.92a0.441,0.441,0,0,1,.62,0l2.53,2.53,3.09-3.1-4.64-4.65a0.435,0.435,0,0,1,0-.62l7.43-7.44a0.46,0.46,0,0,1,.62,0l4.64,4.65,3.1-3.1-2.53-2.53a0.432,0.432,0,0,1-.13-0.31,0.4,0.4,0,0,1,.13-0.31l9.91-9.92a0.429,0.429,0,0,1,.61,0l5.32,5.32a0.435,0.435,0,0,1,0,.62l-4.95,4.96h0l-4.96,4.96a0.439,0.439,0,0,1-.31.13,0.457,0.457,0,0,1-.31-0.13l-2.16-2.17-3.1,3.1,4.64,4.65a0.416,0.416,0,0,1,.13.31,0.439,0.439,0,0,1-.13.31l-3.4,3.41,1.89,1.89a3.853,3.853,0,0,1,2.44-.87,3.958,3.958,0,1,1-3.06,1.49l-1.89-1.89-3.41,3.41a0.435,0.435,0,0,1-.62,0l-4.64-4.65-3.1,3.1,2.17,2.17a0.441,0.441,0,0,1,0,.62l-4.95,4.96h-0.01l-4.95,4.96a0.435,0.435,0,0,1-.62,0l-5.31-5.32A0.441,0.441,0,0,1,1235.15,4200.83Zm34.98-33.79-4.34,4.35a0.209,0.209,0,0,1,.06.04l4.65,4.65,4.33-4.34Zm-4.59,14,4.34-4.34-4.65-4.65a0.209,0.209,0,0,0-.04-0.06l-4.34,4.35,2.52,2.53h0.01Zm-2.16,17.67a3.129,3.129,0,0,0,4.33,0,3.084,3.084,0,0,0,0-4.34A3.065,3.065,0,1,0,1263.38,4198.71Zm-6.51-4.03,6.81-6.82-9.28-9.3-6.81,6.82,4.64,4.65h0Zm-8.97-.31h-0.01l-2.52-2.53-4.35,4.35a0.209,0.209,0,0,1,.06.04l4.65,4.65,4.33-4.34Zm-7.13,11.47,4.34-4.34-4.65-4.65a0.209,0.209,0,0,0-.04-0.06l-4.34,4.35Zm24.77-.97a8.335,8.335,0,0,0,8.32-8.33,0.44,0.44,0,0,1,.88,0,9.216,9.216,0,0,1-9.2,9.21A0.44,0.44,0,0,1,1265.54,4204.87Zm0,5.26a13.589,13.589,0,0,0,13.58-13.59,0.435,0.435,0,1,1,.87,0,14.459,14.459,0,0,1-14.45,14.47A0.44,0.44,0,0,1,1265.54,4210.13Z" transform="translate(-1235.03 -4166)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Technology, Media and Telecom
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                           <div class="vertical-align middle">
                                               <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/utilities.svg" alt="Icon" />-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32.97" height="49" viewBox="0 0 32.97 49">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="utilities.svg" class="cls-1" d="M1272.95,4296.9a0.375,0.375,0,0,0,.04-0.29l-1.47-3.29a0.288,0.288,0,0,0-.3-0.17h-3.91l-5.22-15.8h9.64v2.09a0.284,0.284,0,0,0,.29.29,0.293,0.293,0,0,0,.3-0.29v-2.38a0.129,0.129,0,0,0-.05-0.12,0.088,0.088,0,0,0-.08-0.09l-9.85-9.84h9.34v2.09a0.286,0.286,0,0,0,.3.29,0.278,0.278,0,0,0,.29-0.29v-2.38a0.147,0.147,0,0,0-.04-0.12,0.1,0.1,0,0,0-.08-0.09L1262,4256.34l-3.7-8.17a0.287,0.287,0,0,0-.3-0.16h-2.99a0.355,0.355,0,0,0-.29.16l-3.66,8.13-10.32,10.21a0.727,0.727,0,0,1-.08.09c0,0.04-.04.08-0.04,0.12v2.38a0.278,0.278,0,0,0,.29.29,0.286,0.286,0,0,0,.3-0.29v-2.09h9.34l-9.81,9.8-0.08.08c0,0.05-.04.09-0.04,0.13v2.38a0.284,0.284,0,0,0,.29.29,0.293,0.293,0,0,0,.3-0.29v-2.09h9.72l-5.22,15.8h-3.92a0.34,0.34,0,0,0-.29.17l-1.47,3.29a0.232,0.232,0,0,0,.04.29,0.313,0.313,0,0,0,.25.13h32.37A0.226,0.226,0,0,0,1272.95,4296.9Zm-16.88-35.22-4.47,4.42v-8.84Zm0.88,10.29,4.46-4.41v8.88Zm-0.46-.41-4.47-4.42h8.93Zm4.46-5.05h-8.93l4.47-4.42Zm-4-4.83,4.46-4.42v8.88Zm-0.88,10.29-4.47,4.42v-8.83Zm0.42,0.46,4.46,4.42h-8.93Zm5.55-4.87,9.27,9.17h-9.27v-9.17Zm0-10.3,9.27,9.17h-9.27v-9.17Zm-5.55,3.96-4.47-4.38h8.93Zm-5.52,5.17h-9.26l9.26-9.17v9.17Zm0,10.34h-9.26l9.26-9.17v9.17Zm10.02,0.71-4.5,4.66-4.47-4.66h8.97Zm0.59,0.29,4.8,14.55-9.43-9.76Zm-6.4-29.1h2.61l3.45,7.59h-9.47Zm-3.74,29.1,4.63,4.79-9.43,9.76Zm5.05,5.25,9.85,10.17h-19.7Zm-15.71,13.46,1.18-2.66h29.09l1.18,2.66h-31.45Z" transform="translate(-1240.03 -4248)"/>
                                                </svg>
                                           </div>
                                       </div>
                                   </div>
                                   <div class="text">
                                       <div class="display-table">
                                           <div class="vertical-align middle">
                                               Utilities
                                           </div>
                                       </div>
                                   </div>
                                   <div class="clearfix"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="functions">
                            <div class="heading"><span>Functions</span></div>
                            <ul>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
<!--                                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/backoffice.svg" alt="Icon" />-->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="43" height="43" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="0 0 40 40" style="enable-background:new 0 0 40 40;" xml:space="preserve">
                                                <style type="text/css">
                                                        .st0{fill:#1F3863;}
                                                </style>
                                                <path class="st0" d="M35.7,17.9c-0.5-0.5-1.2-0.7-2-0.8c-1.2,0-2,0-2.6,0v-5.6c0-4.4-5-7.9-11.1-7.9S8.9,7.2,8.9,11.6v5.6
                                                        c-0.6,0-1.4,0-2.6,0c-0.8,0-1.5,0.2-2,0.8c-1,1-1,2.5-1,2.5V27c0,2,2,2.7,3.1,2.8l3.8-0.2V17.1l-0.3,0c0,0-0.1,0-0.4,0v-5.6
                                                        c0-4,4.7-7.3,10.5-7.3c5.8,0,10.5,3.3,10.5,7.3v5.6c-0.3,0-0.4,0-0.4,0l-0.3,0v12.6l2.9,0.1c0,0.5-0.1,2.3-1.6,3.3
                                                        c-1.5,1.1-4.7,1-5.9,1c-0.2-1.9-1.7-3.3-3.6-3.3c-2,0-3.6,1.6-3.6,3.6s1.6,3.6,3.6,3.6c1.9,0,3.4-1.5,3.6-3.3c0.3,0,0.7,0,1.1,0
                                                        c1.6,0,3.9-0.2,5.2-1.1c1.7-1.2,1.9-3.2,1.9-3.8l0.3,0c1.1-0.1,3-0.9,3-2.8v-6.6C36.7,20.3,36.7,18.8,35.7,17.9z M9.7,29.2l-3.2,0.1
                                                        c0,0-2.5-0.3-2.5-2.3v-6.6c0,0,0-1.3,0.8-2.1c0.4-0.4,0.9-0.6,1.6-0.6c0,0,0,0,0,0c1.9,0,2.9,0,3.4,0V29.2z M21.6,37.5
                                                        c-1.7,0-3-1.4-3-3s1.4-3,3-3s3,1.4,3,3S23.2,37.5,21.6,37.5z M36.1,27c0,1.9-2.4,2.3-2.5,2.3l-3.3-0.1V17.7c0.5,0,1.5,0,3.4,0
                                                        c0,0,0,0,0,0c0.7,0,1.2,0.2,1.6,0.6c0.8,0.8,0.8,2.1,0.8,2.1V27z"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Back Office and Centralized Process
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/finance.svg" alt="Icon" />-->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="37" height="37" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 37 37" style="enable-background:new 0 0 37 37;" xml:space="preserve">
                                               <style type="text/css">
                                                       .st0{fill:#1F3863;}
                                               </style>
                                               <g>
                                                       <path class="st0" d="M11.8,36.2H4.2V18.4h7.5V36.2z M4.8,35.7h6.5V18.9H4.8V35.7z"/>
                                               </g>
                                               <g>
                                                       <path class="st0" d="M21.8,36.2h-7.5V12.2h7.5V36.2z M14.8,35.7h6.5V12.8h-6.5V35.7z"/>
                                               </g>
                                               <g>
                                                       <path class="st0" d="M31.8,36h-7.5V4.2h7.5V36z M24.8,35.5h6.5V4.8h-6.5V35.5z"/>
                                               </g>
                                               <g>
                                                       <path class="st0" d="M34.3,36.2H1.2C1.1,36.2,1,36.1,1,36V0.9c0-0.1,0.1-0.2,0.2-0.2s0.2,0.1,0.2,0.2v34.9h32.8
                                                               c0.1,0,0.2,0.1,0.2,0.2S34.4,36.2,34.3,36.2z"/>
                                               </g>
                                               </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Finance
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/human-resources.png" alt="Icon" />-->
                                             
                                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><defs><style>.cls-1{fill:#203965;}</style></defs><title>HR</title><path class="cls-1" d="M31,18.69a3.82,3.82,0,1,0-3.82-3.82A3.82,3.82,0,0,0,31,18.69Zm0-7a3.16,3.16,0,1,1-3.16,3.16A3.16,3.16,0,0,1,31,11.71Z"/><path class="cls-1" d="M9,18.69a3.82,3.82,0,1,0-3.82-3.82A3.82,3.82,0,0,0,9,18.69Zm0-7a3.16,3.16,0,1,1-3.16,3.16A3.16,3.16,0,0,1,9,11.71Z"/><path class="cls-1" d="M37.22,21.31a25.39,25.39,0,0,0-5.34-.58.33.33,0,1,0,0,.66,24.71,24.71,0,0,1,5.19.57,2.36,2.36,0,0,1,1.7,2.29c0,.72-.1,1.45-.16,2.17v.08c-.06.82-.12,1.65-.18,2.48a4,4,0,0,1-3.06,3.61,22.83,22.83,0,0,1-7,0,3,3,0,0,1-1.3-.56,6.2,6.2,0,0,0,2-3.91c.06-1,.14-2,.22-3.1v-.17c.06-.83.12-1.66.18-2.48a3.65,3.65,0,0,0-2.68-3.59,30.16,30.16,0,0,0-13,0,3.65,3.65,0,0,0-2.67,3.59c.06.91.13,1.83.2,2.75v.13c.07,1,.14,1.91.2,2.86a6,6,0,0,0,2,3.92,3,3,0,0,1-1.28.55,22.84,22.84,0,0,1-7,0A4,4,0,0,1,2.21,29c0-.76-.11-1.52-.16-2.28l0-.24C2,25.71,1.92,25,1.87,24.24A2.36,2.36,0,0,1,3.57,22a24.73,24.73,0,0,1,5.2-.57.33.33,0,1,0,0-.66,25.37,25.37,0,0,0-5.34.58,3,3,0,0,0-2.21,3c0,.73.1,1.47.16,2.22l0,.24c.06.75.11,1.51.16,2.27a4.69,4.69,0,0,0,3.63,4.23,25.22,25.22,0,0,0,3.59.27,25.21,25.21,0,0,0,3.6-.27,3.76,3.76,0,0,0,1.73-.79,4.47,4.47,0,0,0,1.87.78,30.6,30.6,0,0,0,4.36.33,30.71,30.71,0,0,0,4.37-.33,4.4,4.4,0,0,0,1.85-.78,3.77,3.77,0,0,0,1.74.8,23.74,23.74,0,0,0,7.19,0A4.69,4.69,0,0,0,39.1,29c.05-.82.11-1.65.18-2.47v-.09c.05-.73.11-1.45.16-2.17A3,3,0,0,0,37.22,21.31ZM24.59,32.48a27.78,27.78,0,0,1-8.51,0,4.88,4.88,0,0,1-3.75-4.41V28h0c-.06-.94-.13-1.88-.2-2.81v-.12c-.07-.92-.14-1.84-.2-2.74A2.9,2.9,0,0,1,14,19.5a29.38,29.38,0,0,1,12.64,0,2.9,2.9,0,0,1,2.09,2.81c-.05.82-.12,1.64-.18,2.46l0,.41c-.07.95-.14,1.9-.2,2.88C28.19,29.94,26.65,32.16,24.59,32.48Z"/><path class="cls-1" d="M20.29,16.23a4.84,4.84,0,0,0,3.48-1.46,4.69,4.69,0,0,0,1.37-3.39,4.85,4.85,0,1,0-4.85,4.85Zm0-9a4.19,4.19,0,0,1,4.19,4.19,4,4,0,0,1-1.18,2.92,4.19,4.19,0,1,1-3-7.11Z"/></svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Human Resources
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
<!--                                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/marketingsales.svg" alt="Icon" />-->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="43" height="41" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="2 -1 44 42" style="enable-background:new 2 -1 44 42;" xml:space="preserve">
                                                <style type="text/css">
                                                        .st0{fill:#1F3863;}
                                                </style>
                                                <path id="marketingsales.svg" class="st0" d="M24.1,26.6V28h-0.6v-1.4c-1.4,0.1-2.8-0.5-3.7-1.4l0.3-0.5c0.9,0.9,2.2,1.4,3.5,1.4V20
                                                        l-0.7-0.2c-1.6-0.5-3.1-1.2-3.1-3.2c0-1.9,1.5-3.2,3.8-3.3V12h0.6v1.4c1.4,0,2.7,0.5,3.6,1.5l-0.3,0.4c-0.8-0.9-2-1.4-3.3-1.4v5.8
                                                        h0.1c1.8,0.5,4,1.3,4,3.6C28.2,25.2,26.6,26.5,24.1,26.6L24.1,26.6z M23.6,19.5v-5.6c-1.9,0.1-3.2,1.2-3.2,2.8
                                                        c0,1.7,1.4,2.3,2.8,2.8L23.6,19.5z M27.6,23.3c0-1.6-1.4-2.4-3.4-3.1v5.9C26.2,26,27.6,24.9,27.6,23.3z"/>
                                                <g>
                                                        <g>
                                                                <path class="st0" d="M17.2,40.8l-0.1-0.1l-3.5-6.5l-7.4-1.3l1.1-7.4L2.3,20l5.2-5.4L6.4,7.2l7.4-1.3l3.5-6.6L24,2.6l6.7-3.3
                                                                        l0.1,0.1l3.5,6.5l7.4,1.3l0,0.1l-1,7.2l5.2,5.4l-5.2,5.4l1,7.4l-0.1,0l-7.2,1.3l-3.5,6.6L24,37.4L17.2,40.8z M6.6,32.6l7.2,1.3
                                                                        l0,0.1l3.4,6.5l6.6-3.3l0.1,0l6.5,3.1l3.5-6.4l0.1,0l7.2-1.3l-1-7.2l0,0l5.1-5.3l-5.2-5.4l0-0.1l1-7.2L34,6.1l0-0.1l-3.4-6.5
                                                                        L24,2.8l-0.1,0l-6.6-3.1l-3.5,6.4l-0.1,0L6.6,7.4l1.2,7.2l0,0L2.7,20l5.1,5.4l0,0.1L6.6,32.6z"/>
                                                        </g>
                                                </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Marketing and Sales
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/operations.png" alt="Icon" />-->
                                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40"><defs><style>.cls-1{fill:#203965;}</style></defs><title>operations</title><path class="cls-1" d="M38.44,23.95l0-.2L36,23.89a9.74,9.74,0,0,0-1-2.29L36.77,20l-.11-.17A12.07,12.07,0,0,0,34,17l-.16-.12-1.74,1.64a9.7,9.7,0,0,0-2.23-1.12l.27-2.31V15L30,14.93a12,12,0,0,0-3.84-.46h-.2l0,.39V13.82a5.24,5.24,0,0,0,1.09-.4l.82,1,.17-.1a6.45,6.45,0,0,0,1.57-1.34l.13-.15-.82-1a5.24,5.24,0,0,0,.57-1l1.24.21.06-.19a6.41,6.41,0,0,0,.35-2V8.6l-1.24-.21a5.29,5.29,0,0,0-.21-1.14l1-.6.06,0-.07-.19a6.49,6.49,0,0,0-1-1.78l-.13-.15-1.09.64a5.2,5.2,0,0,0-.89-.74l.42-1.19-.18-.1a6.44,6.44,0,0,0-1.94-.69l-.2,0-.42,1.19a5.25,5.25,0,0,0-1.16,0l-.44-1.18-.2,0a6.46,6.46,0,0,0-1.94.72l-.17.1.44,1.18a5.29,5.29,0,0,0-.88.75l-1.1-.62-.13.16a6.43,6.43,0,0,0-1,1.79l-.07.19,1.1.62a5.24,5.24,0,0,0-.19,1.14l-1.24.23v.2c0,.15,0,.3,0,.46a9.32,9.32,0,0,0-2-2.11l-.16-.12L14.73,8.36a7.61,7.61,0,0,0-1.64-.83L13.3,5.7l-.19-.05a9.3,9.3,0,0,0-3-.35h-.2L9.73,7.13a7.47,7.47,0,0,0-1.79.42L7,6.07l0-.05-.18.08A9.36,9.36,0,0,0,4.26,7.73l-.15.14,1,1.54A7.62,7.62,0,0,0,4,10.87l-1.76-.53-.09.18a9.35,9.35,0,0,0-.86,2.85l0,.2L3,14.1a7.57,7.57,0,0,0,.1,1.84l-1.63.7-.06,0,0,.19A9.31,9.31,0,0,0,2.68,19.6l.11.17L4.47,19a7.52,7.52,0,0,0,1.26,1.34L4.9,22l.16.12a9.3,9.3,0,0,0,2.66,1.34l.19.06.83-1.64a7.45,7.45,0,0,0,1.83.22L11,23.91h.2a9.31,9.31,0,0,0,2.9-.68l.19-.08-.42-1.79a7.51,7.51,0,0,0,1.54-1l1.3,1-.82-.25-.09.18a12.08,12.08,0,0,0-1.12,3.7l0,.2,2.29.69a9.75,9.75,0,0,0,.14,2.49l-2.2.94,0,.19A12,12,0,0,0,16.45,33l.11.17,2.2-.94a9.73,9.73,0,0,0,1.71,1.82l-1,2.08,0,.06.16.12A12,12,0,0,0,23,38.05l.19.06L24.26,36a9.75,9.75,0,0,0,2.48.3l.53,2.26,0,.06h.2a12.11,12.11,0,0,0,3.76-.88l.18-.08-.55-2.33A9.79,9.79,0,0,0,33,33.94l1.91,1.43.14-.14a12.06,12.06,0,0,0,2.31-3.09l.09-.18-1.91-1.43a9.7,9.7,0,0,0,.72-2.39L38.63,28l0-.2A12.06,12.06,0,0,0,38.44,23.95Zm-2.6,3.74,0,.19A9.25,9.25,0,0,1,35,30.5l-.08.17,1.9,1.42a11.58,11.58,0,0,1-2,2.66L33,33.37l-.05,0-.14.13A9.22,9.22,0,0,1,30.51,35l-.17.08.54,2.31a11.65,11.65,0,0,1-3.23.76l-.54-2.31h-.19a9.21,9.21,0,0,1-2.72-.32l-.19,0-1.07,2.12A11.59,11.59,0,0,1,20,36l1.07-2.12-.15-.12a9.27,9.27,0,0,1-1.87-2l-.11-.16-2.18.93a11.51,11.51,0,0,1-1.31-3l2.18-.93,0-.19a9.25,9.25,0,0,1-.15-2.73l0-.19-2.27-.68a11.61,11.61,0,0,1,1-3.17l2.21.67.06,0,.09-.17A9.31,9.31,0,0,1,20.13,20l.14-.14-1.3-2A11.65,11.65,0,0,1,21.74,16L23,18,23,18l.18-.07a9.28,9.28,0,0,1,2.67-.63l.19,0,.28-2.35a11.58,11.58,0,0,1,3.29.39l-.27,2.29v.06l.18.06A9.26,9.26,0,0,1,32,19l.16.11,1.73-1.62a11.59,11.59,0,0,1,2.27,2.42l-1.68,1.58,0,0,.1.16a9.2,9.2,0,0,1,1.08,2.52l0,.19L38,24.24a11.61,11.61,0,0,1,.19,3.31Zm-16-18.84V8.66a4.77,4.77,0,0,1,.24-1.39l.06-.18-1.08-.61a5.92,5.92,0,0,1,.75-1.32l1.08.61L21,5.63a4.7,4.7,0,0,1,1.07-.91l.16-.1-.43-1.16a5.94,5.94,0,0,1,1.42-.53l.43,1.16.19,0a4.79,4.79,0,0,1,1.4,0l.19,0,.42-1.17a6,6,0,0,1,1.43.51l-.4,1.11,0,.06.17.1a4.74,4.74,0,0,1,1.08.9l.13.14,1.07-.63a6,6,0,0,1,.77,1.31l-1,.59-.05,0,.06.18a4.78,4.78,0,0,1,.25,1.38v.19L30.59,9a6,6,0,0,1-.25,1.49l-1.22-.21-.07.18a4.74,4.74,0,0,1-.69,1.22l-.12.15.8.94a5.92,5.92,0,0,1-1.16,1l-.8-.94-.17.09a4.7,4.7,0,0,1-1.32.49l-.19,0v1.24a5.93,5.93,0,0,1-1.52,0V13.45l-.19,0a4.72,4.72,0,0,1-1.32-.47l-.17-.09-.79,1a6,6,0,0,1-1.17-1l.79-1-.12-.15a4.8,4.8,0,0,1-.71-1.21l-.07-.18-1.22.22a6,6,0,0,1-.27-1.49Zm-4.64,11A7,7,0,0,1,13.49,21l-.17.08.41,1.77a8.88,8.88,0,0,1-2.37.56l-.42-1.77h-.19a7.07,7.07,0,0,1-2.07-.25l-.18-.05L7.68,23a8.81,8.81,0,0,1-2.17-1.1l.82-1.62-.15-.12a7,7,0,0,1-1.43-1.52l-.11-.16L3,19.18a8.8,8.8,0,0,1-1-2.24l1.67-.72,0-.19A7.1,7.1,0,0,1,3.52,14l0-.19L1.8,13.24a8.78,8.78,0,0,1,.7-2.33l1.74.52.09-.17A7.06,7.06,0,0,1,5.58,9.6l.14-.14-1-1.52a8.81,8.81,0,0,1,2-1.33l1,1.47,0,.05.18-.07a7,7,0,0,1,2-.48l.19,0,.21-1.8A8.8,8.8,0,0,1,12.79,6l-.21,1.74v.06l.18.06a7.05,7.05,0,0,1,1.86.94l.16.11L16.1,7.72a8.84,8.84,0,0,1,1.67,1.77l-1.28,1.2,0,0,.1.16a7,7,0,0,1,.82,1.92l0,.19,1.81-.1a8.82,8.82,0,0,1,.14,2.43l-1.81.1,0,.19a7.08,7.08,0,0,1-.6,2l-.08.17,1.45,1.09a8.88,8.88,0,0,1-1.46,1.95l-1.4-1.05-.05,0Zm4.41-7.26,0-.2-1.83.1A7.49,7.49,0,0,0,17,10.82l1.21-1.14a6.43,6.43,0,0,0,.29,1.23l.07.19,1.24-.23a5.23,5.23,0,0,0,.59,1l-.76.92C19.66,12.73,19.66,12.67,19.64,12.6ZM17,21.31a9.29,9.29,0,0,0,1.79-2.38l.09-.18-1.47-1.1A7.59,7.59,0,0,0,18,15.88l1.83-.1,0-.2a9.28,9.28,0,0,0-.11-2.66l.07.08a6.46,6.46,0,0,0,1.59,1.32l.17.1.81-1a5.17,5.17,0,0,0,1.09.39v1.26l.2,0a6.41,6.41,0,0,0,2.06,0l.17,0-.21,1.78a9.66,9.66,0,0,0-2.43.57l-1.27-1.95,0-.06-.18.08a12.1,12.1,0,0,0-3.23,2.11l-.15.14,1.31,2a9.79,9.79,0,0,0-1.49,2l-1.23-.37Z"/><path class="cls-1" d="M29.48,22a5.39,5.39,0,1,0,2.41,3.35A5.35,5.35,0,0,0,29.48,22Zm-5.43,8.76a4.88,4.88,0,0,1-2.2-3.06,4.92,4.92,0,1,1,2.2,3.06Z"/><path class="cls-1" d="M12.75,11a4.18,4.18,0,1,0,1.87,2.6A4.16,4.16,0,0,0,12.75,11Zm-1.36,7.18a3.72,3.72,0,1,1,2.77-4.47A3.7,3.7,0,0,1,11.39,18.23Z"/><path class="cls-1" d="M22.55,10.84a2.93,2.93,0,1,0-.41-.51A3,3,0,0,0,22.55,10.84ZM26.36,7a2.48,2.48,0,0,1,.35.43A2.48,2.48,0,1,1,26.36,7Z"/></svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Operations
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <img class="research-and-development" src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/research-development.svg" alt="Icon" />
<!--                                                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="42" viewBox="0 0 30 42">
                                                    <defs>
                                                      <style>
                                                        .cls-1 {
                                                          fill: #1f3863;
                                                          fill-rule: evenodd;
                                                        }
                                                      </style>
                                                    </defs>
                                                    <path id="r_d.svg" data-name="r&amp;d.svg" class="cls-1" d="M1838.53,3669a1.206,1.206,0,0,0-.88.35l-0.88.87-0.91-.87a1.173,1.173,0,0,0-1.55,0l-16.94,16.52a1.05,1.05,0,0,0,0,1.5l0.88,0.88-0.92.87a1.214,1.214,0,0,0,0,1.68l1.06,1.02a1.264,1.264,0,0,0,1.72,0l0.91-.88,0.88,0.84a1.111,1.111,0,0,0,1.55,0l12.16-11.79a15.426,15.426,0,0,1-3.55,25.79h-11.77a1.229,1.229,0,0,0-1.2,1.19v2.84a1.229,1.229,0,0,0,1.2,1.19h24.56a1.229,1.229,0,0,0,1.2-1.19v-2.84a1.229,1.229,0,0,0-1.2-1.19h-3.72a19.834,19.834,0,0,0-1.62-29.61l0.92-.87a1.062,1.062,0,0,0,0-1.51l-0.92-.87,0.92-.88a1.144,1.144,0,0,0,0-1.68l-1.06-1.01h0a1.076,1.076,0,0,0-.84-0.35h0Zm0,0.91a0.224,0.224,0,0,1,.17.07l1.06,1.05a0.2,0.2,0,0,1,0,.35l-0.92.91-1.4-1.4,0.91-.91a0.267,0.267,0,0,1,.18-0.07h0Zm-3.45.07a0.155,0.155,0,0,1,.11.03l4.57,4.45a0.172,0.172,0,0,1,0,.17l-16.98,16.52a0.2,0.2,0,0,1-.21,0l-4.53-4.44a0.131,0.131,0,0,1,0-.21l16.94-16.49a0.131,0.131,0,0,1,.1-0.03h0Zm3.76,6.86a18.94,18.94,0,0,1,.95,28.94h-5.83a16.35,16.35,0,0,0,2.32-26.46l2.56-2.48h0Zm-19.92,12.04,1.44,1.4-0.92.87a0.26,0.26,0,0,1-.38,0l-1.06-1.01a0.29,0.29,0,0,1,0-.39l0.92-.87h0Zm1.37,17.85h24.56a0.239,0.239,0,0,1,.25.24v2.84a0.239,0.239,0,0,1-.25.24h-24.56a0.239,0.239,0,0,1-.25-0.24v-2.84a0.239,0.239,0,0,1,.25-0.24h0Z" transform="translate(-1817 -3669)"/>
                                                </svg>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Research, Development and Products
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/strategy.svg" alt="Icon" />-->
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="38" height="32" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    viewBox="0 0 37.7 31.6" style="enable-background:new 0 0 37.7 31.6;" xml:space="preserve">
                                           <style type="text/css">
                                                   .st0{fill:#1F3863;}
                                           </style>
                                           <path class="st0" d="M36.7,5.9c0-0.1-0.2-0.3-0.3-0.3h-0.1L35.4,5V3.9c0-0.5-0.5-0.9-1-0.9c-0.1,0-0.2,0-0.4,0.1l-3.1,1.8
                                                   c-0.2,0.1-0.4,0.4-0.4,0.8v1.6c0,0,0,0,0,0.1L29.3,8c-0.6-1-1.4-2-2.3-2.9c-2.9-2.8-6.8-4.4-10.8-4.4c-4,0-7.9,1.6-10.8,4.5L5.2,5.4
                                                   c-5.7,5.9-5.6,15.3,0.2,21c2.9,2.8,6.7,4.4,10.7,4.4c4.1,0,8-1.6,10.9-4.5l0.2-0.2c2.9-2.9,4.4-6.6,4.3-10.6
                                                   c-0.1-2.5-0.7-4.9-1.9-6.9l1.1-0.6c0,0,0.1,0.1,0.2,0.1l1.4,0.7c0.1,0.1,0.3,0.1,0.4,0.1h0.5l3.1-1.8C36.8,6.8,36.9,6.3,36.7,5.9z
                                                    M32.8,8.2l-1.2-0.7l1.6-0.9c0.1,0,0.2-0.1,0.2-0.2c0-0.1,0-0.2,0-0.3c0-0.1-0.1-0.1-0.2-0.2c-0.1,0-0.2,0-0.3,0l-1.6,0.9V5.7
                                                   c0,0,0-0.1,0.1-0.3l3-1.7h0.2c0.1,0,0.2,0.1,0.2,0.2v1.5L36,6c0,0,0,0.1,0.1,0.1v0.1l0,0c0,0,0,0,0.1,0.1L36,6.4l-3,1.8
                                                   C32.8,8.2,32.8,8.2,32.8,8.2z M26.4,25.8c-5.7,5.6-14.7,5.7-20.5,0.1c-2.7-2.6-4.2-6.1-4.2-9.9c0-3.8,1.4-7.4,4.1-10.1L6,5.7
                                                   C8.6,3,12.4,1.4,16.2,1.4c3.7,0,7.4,1.5,10.2,4.1c0.9,0.9,1.6,1.8,2.2,2.8l-2,1.2c-0.5-0.8-1.1-1.5-1.7-2.2
                                                   c-2.3-2.3-5.3-3.6-8.6-3.6C13,3.8,9.9,5,7.5,7.2L7.4,7.3c-4.6,4.8-4.6,12.5,0.1,17.1c2.2,2.2,5.4,3.5,8.6,3.5c3.4,0,6.4-1.2,8.7-3.5
                                                   l0.2-0.2c3.8-3.9,4.4-9.7,1.9-14.2l2-1.2c1.1,2,1.7,4.3,1.7,6.6c0.1,3.7-1.4,7.3-4.1,10.1L26.4,25.8z M16.1,15.5
                                                   c-0.1,0-0.2,0.1-0.2,0.2c0,0.1,0,0.2,0,0.3c0,0.2,0.2,0.2,0.3,0.2h0.1l3.5-2c0.3,0.5,0.4,1.1,0.5,1.7c0,1-0.4,2-1.1,2.7l-0.1,0.1
                                                   c-1.6,1.5-4.1,1.5-5.7,0c-0.8-0.8-1.2-1.7-1.2-2.7c0-1.1,0.4-2.1,1.1-2.8l0.1-0.1c0.7-0.7,1.8-1.2,2.8-1.2c0.7,0,1.9,0.2,2.9,1.3
                                                   c0.1,0.1,0.2,0.3,0.3,0.4L16.1,15.5z M20,13.2c-0.1-0.2-0.3-0.4-0.5-0.6c-0.9-0.9-2.1-1.4-3.3-1.4c-1.2,0-2.4,0.5-3.3,1.4l-0.1,0.1
                                                   c-0.8,0.9-1.2,2.1-1.2,3.2c0,1.2,0.5,2.3,1.3,3.2c0.9,0.8,2.2,1.3,3.3,1.3c1.2,0,2.4-0.5,3.3-1.3l0.1-0.1c1.4-1.5,1.6-3.5,0.7-5.2
                                                   l3-1.7c0.6,1.1,0.9,2.3,0.9,3.6c0,2.1-0.8,4.1-2.3,5.6l-0.1,0.1c-1.5,1.5-3.5,2.3-5.6,2.3c-2.2,0-4.2-0.8-5.8-2.3
                                                   C9,19.9,8.1,18,8.1,15.9c0-2.1,0.8-4.1,2.3-5.6l0.1-0.2C12,8.6,14,7.8,16.2,7.8c2.2,0,4.2,0.8,5.7,2.4c0.4,0.4,0.8,0.9,1.1,1.3
                                                   L20,13.2z M23.6,11.2c-0.3-0.5-0.7-1.1-1.2-1.5c-1.6-1.6-3.8-2.6-6.2-2.6c-2.3,0-4.6,0.9-6.2,2.5L9.9,9.8c-1.6,1.7-2.5,3.9-2.5,6.1
                                                   c0,2.2,0.9,4.4,2.5,6c1.6,1.6,3.9,2.5,6.2,2.5c2.4,0,4.6-0.9,6.2-2.5l0.1-0.1c1.6-1.7,2.5-3.9,2.5-6.1c0-1.4-0.4-2.7-1-3.9l2.3-1.3
                                                   c0.9,1.6,1.3,3.3,1.3,5.2c0,3-1.1,5.9-3.2,8.1l-0.2,0.2c-4.4,4.4-11.7,4.4-16.3,0c-4.4-4.4-4.5-11.6-0.1-16L8,7.8
                                                   c2.2-2.1,5.1-3.3,8.2-3.3c3.2,0,6,1.1,8.1,3.3C25,8.4,25.5,9.1,26,9.9L23.6,11.2z"/>
                                           </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Strategy
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
<!--                                                <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industries-functions/supplychain.svg" alt="Icon" />-->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="43" height="40" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                         viewBox="-934 523 51 36" style="enable-background:new -934 523 51 36;" xml:space="preserve">
                                                <style type="text/css">
                                                        .st0{fill:#1F3863;}
                                                </style>
                                                <g>
                                                        <g transform="translate(360.000000, 225.000000)">
                                                                <path class="st0" d="M-1260.8,314c0,0.5-0.4,0.8-0.8,0.8h-13.7c-0.5,0-0.8-0.4-0.8-0.8v-13.1c0-0.5,0.4-0.8,0.8-0.8h4.7v3.1
                                                                        c0,0.2,0.1,0.4,0.3,0.5c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3-0.1l1.4-1.2l1.4,1.2c0.2,0.1,0.4,0.2,0.5,0.1c0.2-0.1,0.3-0.3,0.3-0.5
                                                                        v-3.1h4.7c0.5,0,0.8,0.4,0.8,0.8V314z M-1270.1,299.5h3.3v0.3v3.2l-1.5-1.3c-0.1-0.1-0.3-0.1-0.4,0l-1.5,1.3v-3.2V299.5z
                                                                         M-1261.6,299.5h-4.7v-0.3c0-0.2-0.1-0.3-0.3-0.3h-3.8c-0.2,0-0.3,0.1-0.3,0.3v0.3h-4.7c-0.8,0-1.4,0.6-1.4,1.4V314
                                                                        c0,0.8,0.6,1.4,1.4,1.4h13.7c0.8,0,1.4-0.6,1.4-1.4v-13.1C-1260.2,300.1-1260.9,299.5-1261.6,299.5L-1261.6,299.5z"/>
                                                        </g>
                                                        <g>
                                                                <g transform="translate(360.000000, 225.000000)">
                                                                        <path class="st0" d="M-1269.8,331.6c0,0.5-0.4,0.8-0.8,0.8h-13.7c-0.5,0-0.8-0.4-0.8-0.8v-13.1c0-0.5,0.4-0.8,0.8-0.8h4.7v3.1
                                                                                c0,0.2,0.1,0.4,0.3,0.5c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3-0.1l1.4-1.2l1.4,1.2c0.2,0.1,0.4,0.2,0.5,0.1c0.2-0.1,0.3-0.3,0.3-0.5
                                                                                v-3.1h4.7c0.5,0,0.8,0.4,0.8,0.8V331.6z M-1279.1,317.1h3.3v0.3v3.2l-1.5-1.3c-0.1-0.1-0.3-0.1-0.4,0l-1.5,1.3v-3.2V317.1z
                                                                                 M-1270.6,317.1h-4.7v-0.3c0-0.2-0.1-0.3-0.3-0.3h-3.8c-0.2,0-0.3,0.1-0.3,0.3v0.3h-4.7c-0.8,0-1.4,0.6-1.4,1.4v13.1
                                                                                c0,0.8,0.6,1.4,1.4,1.4h13.7c0.8,0,1.4-0.6,1.4-1.4v-13.1C-1269.2,317.8-1269.8,317.1-1270.6,317.1L-1270.6,317.1z"/>
                                                                </g>
                                                                <g transform="translate(360.000000, 225.000000)">
                                                                        <path class="st0" d="M-1251.9,331.6c0,0.5-0.4,0.8-0.8,0.8h-13.7c-0.5,0-0.8-0.4-0.8-0.8v-13.1c0-0.5,0.4-0.8,0.8-0.8h4.7v3.1
                                                                                c0,0.2,0.1,0.4,0.3,0.5c0.1,0,0.1,0,0.2,0c0.1,0,0.2,0,0.3-0.1l1.4-1.2l1.4,1.2c0.2,0.1,0.4,0.2,0.5,0.1c0.2-0.1,0.3-0.3,0.3-0.5
                                                                                v-3.1h4.7c0.5,0,0.8,0.4,0.8,0.8V331.6z M-1261.2,317.1h3.3v0.3v3.2l-1.5-1.3c-0.1-0.1-0.3-0.1-0.4,0l-1.5,1.3v-3.2V317.1z
                                                                                 M-1252.7,317.1h-4.7v-0.3c0-0.2-0.1-0.3-0.3-0.3h-3.8c-0.2,0-0.3,0.1-0.3,0.3v0.3h-4.7c-0.8,0-1.4,0.6-1.4,1.4v13.1
                                                                                c0,0.8,0.6,1.4,1.4,1.4h13.7c0.8,0,1.4-0.6,1.4-1.4v-13.1C-1251.4,317.8-1252,317.1-1252.7,317.1L-1252.7,317.1z"/>
                                                                </g>
                                                        </g>
                                                </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Supply Chain and Sourcing
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/sustainability-and-engagement.svg" alt="Icon" />-->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="32.94" height="44.44" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 32.9 44.4" style="enable-background:new 0 0 32.9 44.4;" xml:space="preserve">
                                               <style type="text/css">
                                                       .st0{fill:#1F3863;}
                                               </style>
                                               <g>
                                                       <g>
                                                               <path class="st0" d="M14.7,43.3c-0.3,0-0.6,0-0.9-0.1c-2.3-0.3-4.3-2.1-4.9-4.3c-0.5-1.8-0.2-3.7,0.9-5.1c1-1.4,2.6-2.3,4.3-2.5
                                                                       V16.5c0-3.2,1.2-6.2,3.4-8.5V7c0-3.4,2.8-6.2,6.2-6.2h0c0.4,0,0.7,0.3,0.8,0.7v1.3c0,3.3-2.6,6-5.8,6.2c-1.9,2-3,4.7-3,7.5v14.9
                                                                       c2.8,0.4,4.9,2.7,5.1,5.6c0.1,1.8-0.6,3.6-1.9,4.8C17.6,42.8,16.2,43.3,14.7,43.3z M23.7,1.3C20.6,1.3,18,3.8,18,7v1.3l-0.1,0.1
                                                                       c-2.1,2.2-3.3,5.1-3.3,8.2v15.3l-0.2,0c-1.7,0.1-3.2,0.9-4.2,2.3c-1,1.4-1.3,3.1-0.8,4.7c0.6,2.1,2.3,3.6,4.4,3.9
                                                                       c1.7,0.2,3.4-0.3,4.6-1.4s1.9-2.7,1.8-4.4c-0.2-2.7-2.2-4.8-4.9-5.1l-0.2,0V16.5c0-3,1.1-5.8,3.2-7.9l0.1-0.1l0.1,0
                                                                       C21.5,8.4,24,5.9,24,2.9V1.5C23.9,1.4,23.8,1.3,23.7,1.3z"/>
                                                       </g>
                                                       <g>
                                                               <path class="st0" d="M11.2,19.1l-0.3,0C6,18.8,2.2,14.7,2.2,9.8V7.7l0.3,0c4.9,0.3,8.7,4.3,8.7,9.2V19.1z M2.7,8.2v1.6
                                                                       c0,4.6,3.5,8.3,8,8.7v-1.6C10.7,12.4,7.2,8.6,2.7,8.2z"/>
                                                       </g>
                                                       <g>
                                                               <path class="st0" d="M18.5,27.9v-3.1c0-7.2,5.6-13,12.8-13.3l0.3,0v3.1c0,7.2-5.6,13-12.8,13.3L18.5,27.9z M31.1,11.9
                                                                       c-6.8,0.4-12,6-12.1,12.8v2.6c6.8-0.4,12.1-6,12.1-12.8V11.9z"/>
                                                       </g>
                                                       <g>
                                                               <path class="st0" d="M10.6,27.8c-0.5,0-1-0.2-1.4-0.5c-0.6-0.4-0.9-1.1-0.9-1.8c0-1.3,1-2.3,2.3-2.3h0c0.7,0,1.4,0.3,1.8,0.9
                                                                       c0.4,0.6,0.6,1.3,0.4,2c-0.2,0.8-0.8,1.4-1.6,1.6C11,27.8,10.8,27.8,10.6,27.8z M10.6,23.8c-1,0-1.8,0.8-1.8,1.8
                                                                       c0,0.6,0.3,1.1,0.7,1.4c0.4,0.3,1,0.5,1.6,0.3c0.6-0.2,1.1-0.6,1.2-1.2c0.1-0.6,0-1.1-0.3-1.6C11.7,24,11.2,23.8,10.6,23.8z"/>
                                                       </g>
                                               </g>
                                               <g>
                                                       <path class="st0" d="M14.9,35.7c-0.2,0-0.4,0.2-0.4,0.4v1h-1c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4h1v1
                                                               c0,0.2,0.2,0.4,0.4,0.4c0.2,0,0.4-0.2,0.4-0.4c0,0,0,0,0,0v-1h1c0.2,0,0.4-0.2,0.4-0.4c0-0.2-0.2-0.4-0.4-0.4h-1v-1
                                                               C15.2,35.9,15.1,35.7,14.9,35.7L14.9,35.7z"/>
                                               </g>
                                               </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Sustainability and Engagement
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                <!--<img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/svgs/technology.svg" alt="Icon" />-->
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" width="46" height="46" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                        viewBox="0 0 46 46" style="enable-background:new 0 0 46 46;" xml:space="preserve">
                                               <style type="text/css">
                                                       .st0{fill:#1F3863;}
                                               </style>
                                               <g>
                                                       <path class="st0" d="M31.1,12.4c-0.4-0.3-0.9-0.6-1.5-0.6H13.8c-0.4,0-0.8,0.4-0.8,0.8v20.8c0,0.4,0.4,0.8,0.8,0.8h20.7
                                                               c0.4,0,0.8-0.4,0.8-0.8V17.3c0-0.6-0.3-1.2-0.7-1.7L31.1,12.4z M34.5,33.4L34.5,33.4H13.8l0-20.8h15.8c0.4,0,0.7,0.1,1,0.4l3.4,3.2
                                                               c0.3,0.3,0.5,0.7,0.5,1.1V33.4z"/>
                                                       <rect x="19.3" y="17.8" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="23.1" y="17.8" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="27" y="17.8" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="19.3" y="22" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="23.1" y="22" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="27" y="22" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="19.3" y="25.9" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="23.1" y="25.9" class="st0" width="2.1" height="2.1"/>
                                                       <rect x="27" y="25.9" class="st0" width="2.1" height="2.1"/>
                                                       <path class="st0" d="M42.9,19.8c0.2,0,0.4-0.2,0.4-0.4S43.1,19,42.9,19h-3.5v-6.4h3.5c0.2,0,0.4-0.2,0.4-0.4s-0.2-0.4-0.4-0.4h-3.5
                                                               v-1.4c0-1.4-1.2-2.6-2.6-2.6h-1.5V4.3c0-0.2-0.2-0.4-0.4-0.4s-0.4,0.2-0.4,0.4v3.5h-6.4V4.3c0-0.2-0.2-0.4-0.4-0.4
                                                               s-0.4,0.2-0.4,0.4v3.5H21V4.3c0-0.2-0.2-0.4-0.4-0.4s-0.4,0.2-0.4,0.4v3.5h-6.4V4.3c0-0.2-0.2-0.4-0.4-0.4s-0.4,0.2-0.4,0.4v3.5
                                                               h-1.5C10.2,7.8,9,9,9,10.4v1.4H5.5c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4H9V19H5.5c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4H9v6.4
                                                               H5.5c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4H9v6.4H5.5c-0.2,0-0.4,0.2-0.4,0.4s0.2,0.4,0.4,0.4H9v1.5c0,1.4,1.2,2.6,2.6,2.6h1.5
                                                               v3.5c0,0.2,0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4v-3.5h6.4v3.5c0,0.2,0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4v-3.5h6.4v3.5
                                                               c0,0.2,0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4v-3.5h6.4v3.5c0,0.2,0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4v-3.5h1.4c1.3,0,2.4-1,2.5-2.2h0.1
                                                               v-1.9h3.5c0.2,0,0.4-0.2,0.4-0.4s-0.2-0.4-0.4-0.4h-3.5v-6.4h3.5c0.2,0,0.4-0.2,0.4-0.4s-0.2-0.4-0.4-0.4h-3.5v-6.4H42.9z
                                                                M38.5,35.2v0.4c0,1-0.8,1.8-1.8,1.8h-1.5c-0.1-0.1-0.2-0.1-0.3-0.1s-0.2,0-0.3,0.1h-6.6c-0.1-0.1-0.2-0.1-0.3-0.1s-0.2,0-0.3,0.1
                                                               h-6.7c-0.1-0.1-0.2-0.1-0.3-0.1s-0.2,0-0.3,0.1h-6.6c-0.1-0.1-0.2-0.1-0.3-0.1s-0.2,0-0.3,0.1h-1.6c-1,0-1.8-0.8-1.8-1.8V34
                                                               c0.1-0.1,0.1-0.2,0.1-0.3s0-0.2-0.1-0.3v-6.7c0.1-0.1,0.1-0.2,0.1-0.3s0-0.2-0.1-0.3v-6.6c0.1-0.1,0.1-0.2,0.1-0.3s0-0.2-0.1-0.3
                                                               v-6.7c0.1-0.1,0.1-0.2,0.1-0.3s0-0.2-0.1-0.3v-1.5c0-1,0.8-1.8,1.8-1.8h1.6c0.1,0.1,0.2,0.1,0.3,0.1s0.2,0,0.3-0.1h6.6
                                                               c0.1,0.1,0.2,0.1,0.3,0.1s0.2,0,0.3-0.1h6.7c0.1,0.1,0.2,0.1,0.3,0.1s0.2,0,0.3-0.1h6.6c0.1,0.1,0.2,0.1,0.3,0.1s0.2,0,0.3-0.1h1.6
                                                               c1,0,1.8,0.8,1.8,1.8V12c-0.1,0.1-0.1,0.2-0.1,0.2s0,0.2,0.1,0.2v6.7c-0.1,0.1-0.1,0.2-0.1,0.2s0,0.2,0.1,0.2v6.6
                                                               c-0.1,0.1-0.1,0.2-0.1,0.2s0,0.2,0.1,0.2v6.7c-0.1,0.1-0.1,0.2-0.1,0.2s0,0.2,0.1,0.2L38.5,35.2L38.5,35.2z"/>
                                               </g>
                                               </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Technology
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
//            function showMore(eachMode) {
//                $('.' + eachMode).addClass("mouseover");
//                $('.' + eachMode).removeClass("mouseout");
//            }
//            function hideMore(eachMode) {
//                $('.' + eachMode).addClass("mouseout");
//                $('.' + eachMode).removeClass("mouseover");
//            }
            $(document).ready(function() {
                
            });
        </script>
    </body>
</html>