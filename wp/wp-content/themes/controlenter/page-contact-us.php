<?php $this_page = 'contact-us'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Contact | Control Enter</title>
        
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtaSazt2SCvMvF_ByrPjjYtFbRQ4RLA3o"></script>
        
        
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <div class="body contact-page">
            <div class="address-map-container">
                <div class="address-container">
                    <div class="display-table">
                        <div class="vertical-align middle">
                            <div class="each-address ireland-address">
                                <div class="address">
                                    <a href="javascript: void(0);">
                                        <div class="icon ireland active" id="map_ireland_button" onclick="showMap('ireland', 'ireland-map-container');">
                                            <img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/map-icon-small.png" alt="Map Icon" />
                                            <b>Ireland (Gobal HQ)</b>
                                            <!--<div class="see-map">see map</div>-->
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                    <div class="text">
                                        12 Fitzharris House,<br/>
                                        James Joyce Street, Dublin 1
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="phone-number">
                                    <a href="tel://35312549187"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+353 (0)1 254 9187</a>
                                    <a class="email" href="mailto:caroline@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />caroline@controlentergroup.com</a>
                                </div>
                            </div>
                            <div class="each-address india-address">
                                <div class="address">
                                    <a href="javascript: void(0);">
                                        <div class="icon india" id="map_india_button" onclick="showMap('india', 'india-map-container');">
                                            <img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/map-icon-small.png" alt="Map Icon" />
                                            <b>India (Asia Services)</b>
                                            <!--<div class="see-map">see map</div>-->
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                    <div class="text">
                                        210 Beneka Towers, 16D Main HAL II Stage, Kodihalli,<br/>
                                        Indiranagar, Bangalore, Karnataka 560008
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="phone-number">
                                    <a href="tel://917760004444"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+91 776 000 4444</a>
                                    <a class="email" href="mailto:suresh@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />suresh@controlentergroup.com</a>
                                </div>
                            </div>
                            <div class="each-address america-address">
                                <div class="address">
                                    <a href="javascript: void(0);">
                                        <div class="icon america" id="map_america_button" onclick="showMap('america', 'america-map-container');">
                                            <img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/map-icon-small.png" alt="Map Icon" />
                                            <b>United States (Americas Services)</b>
                                            <!--<div class="see-map">see map</div>-->
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                    <div class="text">
                                        2711  Centerville Road, Suite 400<br/>
                                        Wilmington, Delaware 19808
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="phone-number">
                                    <a href="tel://16173987978"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/phone-icon.png" alt="Phone Icon" />+1 617 398 7978</a>
                                    <a class="email" href="mailto:info@controlentergroup.com"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/email.png" alt="Email Icon" />info@controlentergroup.com</a>
                                </div>
                            </div>
                            <div class="links">
                                <a href="javascript: scrollToSection('.footer', -10, '')" class="talk-to-us">Talk to us</a>
                                <a href="javascript: scrollToSection('.talk-to-us-section', -10, '');" class="join-our-team join-us">Join us</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="map-container">
                    <div class="map ireland-map-container">
                        <div id="map_ireland" style="height: 100%; width: 100%;"></div>
                        <!--<div class="layer"  onclick="window.open('https://goo.gl/maps/4NUAHXQP9my', '_blank');"></div>-->
                        <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.1590900413107!2d77.64434581430456!3d12.96166989086238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14041ae4539b%3A0x64a6b0a9386f8e97!2sKodihalli+Main+Rd%2C+HAL+2nd+Stage%2C+Kodihalli%2C+Bengaluru%2C+Karnataka+560008!5e0!3m2!1sen!2sin!4v1495387982268" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>-->
                    </div>
                    <div class="map india-map-container">
                        <div id="map_india" style="height: 100%; width: 100%;"></div>
                        <!--<div class="layer"  onclick="window.open('https://goo.gl/maps/4NUAHXQP9my', '_blank');"></div>-->
<!--                        <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.1590900413107!2d77.64434581430456!3d12.96166989086238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14041ae4539b%3A0x64a6b0a9386f8e97!2sKodihalli+Main+Rd%2C+HAL+2nd+Stage%2C+Kodihalli%2C+Bengaluru%2C+Karnataka+560008!5e0!3m2!1sen!2sin!4v1495387982268" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>-->
                    </div>
                    <div class="map america-map-container">
                        <div id="map_america" style="height: 100%; width: 100%;"></div>
                        <!--<div class="layer" onclick="window.open('https://goo.gl/maps/4NUAHXQP9my', '_blank');"></div>-->
<!--                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.1590900413107!2d77.64434581430456!3d12.96166989086238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae14041ae4539b%3A0x64a6b0a9386f8e97!2sKodihalli+Main+Rd%2C+HAL+2nd+Stage%2C+Kodihalli%2C+Bengaluru%2C+Karnataka+560008!5e0!3m2!1sen!2sin!4v1495387982268" width="600" height="450" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>-->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-container" style="display: none;">
                <div class="container">
                    <div class="text talk-to-us-text">
                        If you are looking to enhance your current strategies and make your business future ready, or if you just have a quick question, please fill in the form below and we will contact you soon.
                    </div>
                    <div class="text join-us-text">
                        We are always looking for exceptional new talent! If you want to apply for a position at Control Enter, please fill in the form below with your Linkedin profile or resume, and we will respond to you soon.
                    </div>
                    <div class="all-forms">
                        <div class="form-tabs">
                            <div class="links talk-to-us-button active" onclick="showForm('talk-to-us-button', 'talk-to-us', 'talk-to-us-text');">
                                <a href="javascript: void(0);">Talk to us</a>
                            </div>
<!--                            <div class="links work-with-us-button" onclick="showForm('work-with-us-button', 'talk-to-us');">
                                <a href="javascript: void(0);">Work with us</a>
                            </div>-->
                            <div class="links join-us-button" onclick="showForm('join-us-button', 'join-us', 'join-us-text');">
                                <a href="javascript: void(0);">Join Us</a>
                            </div>
                            
<!--                            <div class="follow-us">
                                <div class="heading">Follow us</div>
                                <div class="social-links">
                                    <a href="javascript: void(0);"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/contact-us/linkedin.png" alt="Linkedin Icon" /></a>
                                </div>
                            </div>-->
                        </div>
                        <div class="form talk-to-us" style="display: block;">
                            <div class="form-field half-width">
                                <input type="text" name="your_name" id="your_name" class="input-field" value="" placeholder="Name" />
                                <span class="enquiry-name-error-msg error-msg" style="display: none;">Enter your name</span>
                            </div>
                            <div class="form-field half-width">
                                <input type="email" name="your_email" id="your_email" class="input-field" value="" placeholder="Email" />
                                <span class="enquiry-email-error-msg error-msg" style="display: none;">Enter your email ID</span>
                                <span class="enquiry-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid email ID (Ex: example@example.com)</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-field">
                                <input type="text" name="your_subject" id="your_subject" class="input-field" value="" placeholder="Subject" />
                                <span class="talk-to-us-subject-error-msg error-msg" style="display: none;">Please enter subject</span>
                            </div>
                            <div class="form-field">
                                <textarea name="your_message" rows="15" id="your_message" class="form-field message" value="" placeholder="Message"></textarea>
                                <span class="talk-to-us-message-error error-msg" style="display: none;">Please enter a brief message</span>
                            </div>
                            
                            <div class="clearfix"></div>
                            <div class="success" id="enquiry_success"></div>
                            <div class="clearfix"></div>
                            <div class="submit">
        <!--                        <input type="button" name="submit" class="submit-button" id="submit_button" value="Submit" />-->
                                <div class="links orange">
                                    <a class="submit-button" id="enquiry_submit_button">Submit</a>
                                </div>
                                <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                            </div>
                        </div>
                        
                        <div class="form join-us">
                            <form>
                                <div class="form-field half-width">
                                <input type="text" name="join_us_name" id="join_us_name" class="input-field" value="" placeholder="Name" />
                                <span class="join-us-name-error-msg error-msg" style="display: none;">Enter your name</span>
                            </div>
                            <div class="form-field half-width">
                                <input type="email" name="join_us_email" id="join_us_email" class="input-field" value="" placeholder="Email" />
                                <span class="join-us-email-error-msg error-msg" style="display: none;">Enter your email ID</span>
                                <span class="join-us-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid email ID (Ex: example@example.com)</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-field half-width">
                                <input type="text" name="join_us_subject" id="join_us_subject" class="input-field" value="" placeholder="Subject" />
                                <span class="join-us-subject-error error-msg" style="display: none;">Please enter subject</span>
                            </div>
                            <div class="form-field half-width">
                                <input type="url" name="join_us_link" id="join_us_link" class="input-field" value="" placeholder="LinkedIn" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-field">
                                <textarea name="join_us_message" rows="15" id="join_us_message" class="form-field message" value="" placeholder="Message"></textarea>
                                <span class="join-us-message-error error-msg" style="display: none;">Please enter a brief message</span>
                            </div>
<!--                            <input type="file" accept=".doc, .docx,.pdf" />-->
                            <div class="clearfix"></div>
                            <div class="success" id="join_us_success"></div>
                            <div class="clearfix"></div>
                            <div class="submit">
        <!--                        <input type="button" name="submit" class="submit-button" id="submit_button" value="Submit" />-->
                                <div class="links orange">
                                    <a class="submit-button" id="join_us_submit_button">Submit</a>
                                </div>
                                <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                            </div>
                            </form>
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
            $(document).ready(function() {
                if($(window).width() < 768) {
                    $('.address-container .each-address.ireland-address a').attr("href", "javascript: scrollToSection('.ireland-map-container', 30, '')");
                    $('.address-container .each-address.india-address a').attr("href", "javascript: scrollToSection('.india-map-container', 30, '')");
                    $('.address-container .each-address.america-address a').attr("href", "javascript: scrollToSection('.america-map-container', 30, '')");
                }
                if($(window).width() > 767 && $(window).width() < 992) {
                    $('.address-container .each-address.ireland-address a').attr("href", "javascript: scrollToSection('.ireland-map-container', 70, '')");
                    $('.address-container .each-address.india-address a').attr("href", "javascript: scrollToSection('.india-map-container', 70, '')");
                    $('.address-container .each-address.america-address a').attr("href", "javascript: scrollToSection('.america-map-container', 70, '')");
                }
            });
            $(window).load(function() {
                $('.map-container, .map, .address-container').height($(window).height() - 90);
            });
            function showMap(mapButton, eachMap){
                $('.map').hide();
                $('.' + eachMap).show();
                $('.icon').removeClass('active');
                $('.' + mapButton).addClass('active');
            }
            function showForm(formButton, form, showText) {
                $('.contact-page .form').fadeOut();
                $('.' + form).fadeIn();
                $('.links').removeClass("active");
                $('.' + formButton).addClass("active");
                $('.form-container .text').hide();
                $('.' + showText).show();
            }
        </script>
        
        <script type="text/javascript">
            $(document).ready(function () {
               
            });
            // When the window has finished loading create our google map below
            var ireland = document.getElementById('map_ireland_button');
            var india = document.getElementById('map_india_button');
            var america = document.getElementById('map_america_button');
            google.maps.event.addDomListener(window, 'load', init1);
            google.maps.event.addDomListener(ireland, 'click', init1);
            google.maps.event.addDomListener(india, 'click', init2);
            google.maps.event.addDomListener(america, 'click', init3);
        
            function init1() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 18,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(53.351486031078025, -6.253473756197536), //Control Enter Location
                    //
                    // How you would like to style the map.
                    styles: [
                    {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                    {
                      featureType: 'administrative.locality',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'poi',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'poi.park',
                      elementType: 'geometry',
                      stylers: [{color: '#263c3f'}]
                    },
                    {
                      featureType: 'poi.park',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#6b9a76'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'geometry',
                      stylers: [{color: '#38414e'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'geometry.stroke',
                      stylers: [{color: '#212a37'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#9ca5b3'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'geometry',
                      stylers: [{color: '#746855'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'geometry.stroke',
                      stylers: [{color: '#1f2835'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#f3d19c'}]
                    },
                    {
                      featureType: 'transit',
                      elementType: 'geometry',
                      stylers: [{color: '#2f3948'}]
                    },
                    {
                      featureType: 'transit.station',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'geometry',
                      stylers: [{color: '#17263c'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#515c6d'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'labels.text.stroke',
                      stylers: [{color: '#17263c'}]
                    }
                  ]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map_ireland');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var iconPath = '<?php echo bloginfo ( "template_directory" ); ?>/img/contact-us/';
                var labels = 'HAL 2nd Stage, Kodihalli, Bengaluru, Karnataka 560008';
                var labelIndex = 0;
                
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(53.351486031078025, -6.253473756197536),
                    map: map,
                    icon: iconPath + 'map-icon-big.png',
                    title: 'Control Enter'
                });
            }
            
            function init2() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 18,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(12.963711437702326, 77.64572382223264), //Control Enter
                    //
                    // How you would like to style the map.
                    styles: [
                    {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                    {
                      featureType: 'administrative.locality',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'poi',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'poi.park',
                      elementType: 'geometry',
                      stylers: [{color: '#263c3f'}]
                    },
                    {
                      featureType: 'poi.park',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#6b9a76'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'geometry',
                      stylers: [{color: '#38414e'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'geometry.stroke',
                      stylers: [{color: '#212a37'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#9ca5b3'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'geometry',
                      stylers: [{color: '#746855'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'geometry.stroke',
                      stylers: [{color: '#1f2835'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#f3d19c'}]
                    },
                    {
                      featureType: 'transit',
                      elementType: 'geometry',
                      stylers: [{color: '#2f3948'}]
                    },
                    {
                      featureType: 'transit.station',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'geometry',
                      stylers: [{color: '#17263c'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#515c6d'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'labels.text.stroke',
                      stylers: [{color: '#17263c'}]
                    }
                  ]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map_india');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var iconPath = '<?php echo bloginfo ( "template_directory" ); ?>/img/contact-us/';
                var labels = 'HAL 2nd Stage, Kodihalli, Bengaluru, Karnataka 560008';
                var labelIndex = 0;
                
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(12.963711437702326, 77.64572382223264),
                    map: map,
                    icon: iconPath + 'map-icon-big.png',
                    title: 'Control Enter'
                });
            }
            
            function init3() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions3 = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(39.75435769716888, -75.6269431089197), //Control Enter
                    //
                    // How you would like to style the map.
                    styles: [
                    {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                    {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                    {
                      featureType: 'administrative.locality',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'poi',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'poi.park',
                      elementType: 'geometry',
                      stylers: [{color: '#263c3f'}]
                    },
                    {
                      featureType: 'poi.park',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#6b9a76'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'geometry',
                      stylers: [{color: '#38414e'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'geometry.stroke',
                      stylers: [{color: '#212a37'}]
                    },
                    {
                      featureType: 'road',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#9ca5b3'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'geometry',
                      stylers: [{color: '#746855'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'geometry.stroke',
                      stylers: [{color: '#1f2835'}]
                    },
                    {
                      featureType: 'road.highway',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#f3d19c'}]
                    },
                    {
                      featureType: 'transit',
                      elementType: 'geometry',
                      stylers: [{color: '#2f3948'}]
                    },
                    {
                      featureType: 'transit.station',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#d59563'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'geometry',
                      stylers: [{color: '#17263c'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'labels.text.fill',
                      stylers: [{color: '#515c6d'}]
                    },
                    {
                      featureType: 'water',
                      elementType: 'labels.text.stroke',
                      stylers: [{color: '#17263c'}]
                    }
                  ]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement3 = document.getElementById('map_america');

                // Create the Google Map using our element and options defined above
                var map3 = new google.maps.Map(mapElement3, mapOptions3);

                // Let's also add a marker while we're at it
                var iconPath3 = '<?php echo bloginfo ( "template_directory" ); ?>/img/contact-us/';
                var labels3 = 'HAL 2nd Stage, Kodihalli, Bengaluru, Karnataka 560008';
                var labelIndex3 = 0;
                
                var marker3 = new google.maps.Marker({
                    position: new google.maps.LatLng(39.75435769716888, -75.6269431089197),
                    map: map3,
                    icon: iconPath3 + 'map-icon-big.png',
                    title: 'Control Enter'
                });
            }
        </script>
    </body>
</html>