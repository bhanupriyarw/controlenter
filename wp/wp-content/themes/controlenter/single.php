<?php $this_page = 'insights'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        
        <title><?php the_title(); ?></title>
        
        <meta property='og:site_name' content='Control Enter' />
        <meta property="og:description" content="">
        <meta property="og:url" content="<?php the_permalink(); ?>" />
        
        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="615" />
        <meta property="og:image:height" content="499" />

        <meta property="og:title" content="<?php the_title(); ?>" />
        <meta property="og:image" content="<?php $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full'); echo $image_url[0]; ?>" />
    </head>
    
    <body>
        <?php
        
        $args = array(
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'post',
            'posts_per_page' => -1
        );
        $recent_insights = get_posts( $args );
//        echo '<pre>' . print_r($posts, true) . '</pre>'; die;
        
     ?>
        <?php include_once 'header.php'; ?>
        <div class="single-insights-page">
            <div class="banner-section">
                <div class="image">
                    <?php 
                        if(has_post_thumbnail())
                        {
                            the_post_thumbnail();
                        }
                    ?>
                </div>
            </div>
            <div class="container">
                <div class="left">
                    <div class="insights-title heading">
                        <?php the_title(); ?>
                    </div>
                    <?php ?>
                    <div class="insights-date">
                        <?php $post_date = $post->post_date; ?>
                        <?php echo date("d.m.Y", strtotime("$post_date")); ?>
                    </div>
                    
                    <div class="insights-author">
                        <?php 
                        if(have_posts()) {
                            the_post();
                            the_author(); ?>
                        
                    </div>
                    
                    <div class="insights-content">
                        <div class="content">
                            <div class="insights-text">
                                <?php 
                                    the_content();
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <div class="tags">
                        <div class="heading">Tags</div>
                        <?php
                            foreach (get_the_tags($insights_post->ID) as $tag)
                            {
                                echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>";
                            }
                        ?>
                        <div class="clearfix"></div>
                    </div>

                    <div class="recent-insights">
                        <div class="heading">Recent Insights</div>
                        <?php foreach($recent_insights as $recent_insight) { ?>
                        <a href="<?php echo get_permalink($recent_insight->ID); ?>">
                            <div class="post-title">
                                <ul>
                                    <li><?php echo $recent_insight->post_title; ?></li>
                                </ul>
                            </div>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
            $(document).ready( function () {
                
            });
        </script>
    </body>
</html>