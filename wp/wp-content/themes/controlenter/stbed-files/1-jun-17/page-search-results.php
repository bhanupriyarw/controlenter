<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Insights | Control Enter</title>
    </head>

    <body>
        <?php get_header(); ?>
        
        <?php 
            $query = $_GET['query'];
            $query = str_replace("%20","",$query);
        ?>
        <div class="body insights-page">
            <div class="container">
                <div class="static-nav">
                    <div class="container">
                        <div class="search-box">
                            <div class="form-field">
            <!--                    <img src="<?php bloginfo( 'template_directory' ); ?>/img/icons/insights/search.svg" alt="Search Icon" />-->
                                <input type="text" name="query" id="search_query" class="input-field" value="" placeholder="Search" />
                            </div>
                        </div>
                        <div class="filters-button">
                            <img class="filters-open-button" src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/tags-options.png" alt="Filter Icon" />
                            <img class="filters-close-button" src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/tags-options-close.png" alt="Close Icon" />
                        </div>
                        <div class="subscribe">
                            <div class="form">
                                <div class="form-field">
                                    <input type="email" name="subscriber_email" id="subscriber_email" class="input-field" value="" placeholder="Enter your email id and keep in touch with our latest thinking !" />
                                    <span class="subscriber-email-error-msg error-msg" style="display: none;">Enter your Email Id</span>
                                    <span class="subscriber-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid Email Id (Ex: example@example.com)</span>
                                </div>
                                <div class="submit">
                                    <div class="links">
        <!--                                    <a href="javascript: void(0);"><input type="button" name="subscribe" class="subscribe-button" id="subscribe_button" value="Subscribe Now" /></a>-->
                                        <a href="javascript: void(0);" class="subscribe-button" id="subscribe_button">Subscribe</a>
                                    </div>
                                    <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="success" id="success_subscriber"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="filters-section">
                    <div class="filter-container">
                        <div class="content">
                            <div class="text">
                                Select your areas of interest and we will show you content tailored your interests.
                            </div>
                            <div class="clear-all links">
                                <a href="javascript: void(0);">Clear All</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tabs">
                            <div class="each-tab subject-areas active" onclick="showFilters('subject-areas-list', 'subject-areas');">Subject Areas <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-down.png" alt="Arrow Icon" /></div>
                            <div class="each-tab industries" onclick="showFilters('industries-list', 'industries');">Industries <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-right.png" alt="Arrow Icon" /></div>
                            <div class="each-tab authors" onclick="showFilters('authors-list', 'authors');">Authors <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-right.png" alt="Arrow Icon" /></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="each-list subject-areas-list">
                            <ul>
                            <?php
                            foreach (get_tags() as $tag)
                                { ?>
                                <li class="<?php echo $tag->slug; ?>" onclick="showFiltersResults('<?php echo $tag->slug; ?>')"><?php echo $tag->name; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                            <?php } ?>
                                <li class="clearfix"></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="each-list industries-list">
                            <ul>
                                <?php
                                foreach (get_categories(array('hide_empty' => false, 'exclude' => 1)) as $category)
                                { ?>
                                <li class="<?php echo $category->slug; ?>" onclick="showFiltersResults('<?php echo $category->slug; ?>')"><?php echo $category->cat_name; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                                <?php } ?>
                                <li class="clearfix"></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="each-list authors-list">
<!--                            wp_list_authors('exclude_admin=0');-->
<!--                            <ul>
                                <?php foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                                <?php $author_slug = get_the_author();
                                $author_slug = strtolower($author_slug);
                                $author_slug = str_replace(" ","-",$author_slug);
                                ?>
                                <li class="<?php echo $author_slug; ?>" onclick="showFiltersResults('<?php echo $author_slug; ?>')"><?php echo get_the_author(); ?></li>
                                <?php } wp_reset_postdata(); ?>
                                <li class="clearfix"></li>
                            </ul>-->
                            <?php $authors = get_users();
//                             echo '<pre>' . print_r($authors, true) . '</pre>';;
                            foreach($authors as $author) { 
                                $author_name = $author->display_name;
                                $author_name = strtolower($author_name);
                                $author_name = str_replace(" ","-",$author_name);
                                ?>
                                <li class="<?php echo $author_name; ?>" onclick="showFiltersResults('<?php echo $author_name; ?>');"><?php echo $author->nickname; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                        <div class="go-button links orange">
                            <a href="javascript: voic(0);">Go</a>
                        </div>
                    </div>
                </div>
                
                <div class="insights">
                    <div class="heading">You are browsing: <?php echo $query; ?></div>
                    <?php 
                    $i = 1;
                    if ($insights_posts = $wpdb->get_results( "SELECT * FROM " . $table_prefix . "posts where post_type = 'post' and ((post_title like '%$query%') or (post_content like '%$query%'))" )) {
//                    echo '<pre>' . print_r($insights_posts, true) . '</pre>'; die;
                        foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                    <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($insights_post->ID), 'full'); ?>
                        <div onclick="window.location='<?php echo get_the_permalink($insights_post->ID); ?>'" class="each-insight <?php echo $i; ?>" style="background-image: url('<?php echo $featured_image_url[0]; ?>');">
                            <div class="display-table">
                                <div class="vertical-align middle">
                                    <div class="content">
                                        <div class="text-container">
                                            <div class="heading"><a href="javascript: void(0);"><?php echo $insights_post->post_title; ?></a></div>
                                            <div class="insights-excerpt">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                        </div>
                                        <div class="text">
                                            <div class="tags">
                                                <?php
                                                $posttags = get_the_tags($insights_post->ID);
                                                if ($posttags) {
                                                  foreach($posttags as $tag) {
                                                    echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>";
                                                  }
                                                }
                                                
//                                                foreach (get_tags($insights_post->ID) as $tag)
//                                                {
//                                                   echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>"; 
//                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="author-and-date">
                                                <div class="author-name"><a href="javascript: void(0);"><?php the_author(); ?></a></div>
                                                <div class="insights-date">
                                                    <?php $post_date = $insights_post->post_date; ?>
                                                    <?php echo date("d.m.Y", strtotime("$post_date")); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="read-more">
                                            <a href="<?php echo get_the_permalink($insights_post->ID); ?>">Read More <img src="<?php echo bloginfo("template_directory") ?>/img/insights/read-more.png" alt="Read More Icon" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay"></div>
                        </div>
                    <!--<div class="divider"></div>-->
                    <?php  $i++; } wp_reset_postdata(); } else { ?>
                        <div class="not-found">
                            <h3>Nothing Found</h3>
                            <p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
    </body>
</html>