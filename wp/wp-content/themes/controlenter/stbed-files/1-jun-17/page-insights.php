<?php $this_page = 'insights'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Insights | Control Enter</title>
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <?php
        $args = array(
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'post',
            'posts_per_page' => -1
        );
        $insights_posts = get_posts( $args );
//        echo '<pre>' . print_r($insights_posts, true) . '</pre>'; die;
        ?>
        <div class="body insights-page">
            <div class="container">
                <div class="static-nav">
                    <div class="container">
                        <div class="search-box">
                            <div class="form-field">
                                <input type="text" name="query" id="search_query" class="input-field" value="" placeholder="Search" />
                            </div>
                        </div>
                        <div class="filters-button">
                            <img class="filters-open-button" src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/tags-options.png" alt="Filter Icon" />
                            <img class="filters-close-button" src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/tags-options-close.png" alt="Close Icon" />
                        </div>
                        <div class="subscribe">
                            <div class="form">
                                <div class="form-field">
                                    <input type="email" name="subscriber_email" id="subscriber_email" class="input-field" value="" placeholder="Enter your email to stay in touch on our latest thinking!" />
                                    <span class="subscriber-email-error-msg error-msg" style="display: none;">Enter your Email Id</span>
                                    <span class="subscriber-email-valid-error-msg error-msg" style="display: none;">You have entered an invalid Email Id (Ex: example@example.com)</span>
                                </div>
                                <div class="submit">
                                    <div class="links">
        <!--                                    <a href="javascript: void(0);"><input type="button" name="subscribe" class="subscribe-button" id="subscribe_button" value="Subscribe Now" /></a>-->
                                        <a href="javascript: void(0);" class="subscribe-button" id="subscribe_button">Subscribe</a>
                                    </div>
                                    <div class="loader"><img src="<?php echo bloginfo( "template_directory" ); ?>/img/loader.gif" alt="Loader" /></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="success" id="success_subscriber"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="filters-section">
                    <div class="filter-container">
                        <div class="content">
                            <div class="text">
                                Select your areas of interest and we will show you content tailored your interests.
                            </div>
                            <div class="clear-all links">
                                <a href="javascript: void(0);">Clear All</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tabs">
                            <div class="each-tab subject-areas active" onclick="showFilters('subject-areas-list', 'subject-areas');">Subject Areas <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-down.png" alt="Arrow Icon" /></div>
                            <div class="each-tab industries" onclick="showFilters('industries-list', 'industries');">Industries <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-right.png" alt="Arrow Icon" /></div>
                            <div class="each-tab authors" onclick="showFilters('authors-list', 'authors');">Authors <img src="<?php echo bloginfo( "template_directory" ) ?>/img/insights/arrow-right.png" alt="Arrow Icon" /></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="each-list subject-areas-list">
                            <ul>
                            <?php
                            foreach (get_tags() as $tag)
                                { ?>
                                <li class="<?php echo $tag->slug; ?>" onclick="showFiltersResults('<?php echo $tag->slug; ?>')"><?php echo $tag->name; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                            <?php } ?>
                                <li class="clearfix"></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="each-list industries-list">
                            <ul>
                                <?php
                                foreach (get_categories(array('hide_empty' => false, 'exclude' => 1)) as $category)
                                { ?>
                                <li class="<?php echo $category->slug; ?>" onclick="showFiltersResults('<?php echo $category->slug; ?>')"><?php echo $category->cat_name; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                                <?php } ?>
                                <li class="clearfix"></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="each-list authors-list">
<!--                            wp_list_authors('exclude_admin=0');-->
<!--                            <ul>
                                <?php foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                                <?php $author_slug = get_the_author();
                                $author_slug = strtolower($author_slug);
                                $author_slug = str_replace(" ","-",$author_slug);
                                ?>
                                <li class="<?php echo $author_slug; ?>" onclick="showFiltersResults('<?php echo $author_slug; ?>')"><?php echo get_the_author(); ?></li>
                                <?php } wp_reset_postdata(); ?>
                                <li class="clearfix"></li>
                            </ul>-->
                            <?php $authors = get_users();
//                             echo '<pre>' . print_r($authors, true) . '</pre>';;
                            foreach($authors as $author) { 
                                $author_name = $author->display_name;
                                $author_name = strtolower($author_name);
                                $author_name = str_replace(" ","-",$author_name);
                                ?>
                                <li class="<?php echo $author_name; ?>" onclick="showFiltersResults('<?php echo $author_name; ?>');"><?php echo $author->nickname; ?><img src="<?php echo bloginfo("template_directory") ?>/img/insights/tag-selected.png" alt="Arrow Icon" /></li>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                        <div class="go-button links orange">
                            <a href="javascript: voic(0);">Go</a>
                        </div>
                    </div>
                </div>
                
                <div class="insights">
                    <?php foreach ($insights_posts as $insights_post) { setup_postdata($insights_post); ?>
                    <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($insights_post->ID), 'full'); ?>
                    <?php 
                        $author_slug = get_the_author();
                        $author_slug = strtolower($author_slug);
                        $author_slug = str_replace(" ","-",$author_slug);
                    ?>
                    <div class="each-insight-container <?php foreach (get_the_tags($insights_post->ID) as $tag) { echo "$tag->slug"." "; } ?> <?php foreach (get_the_category($insights_post->ID) as $category) { echo "$category->slug"." "; } ?> <?php echo $author_slug; ?>">
                        <div onclick="window.location='<?php echo get_the_permalink($insights_post->ID); ?>'" class="each-insight" style="background-image: url('<?php echo $featured_image_url[0]; ?>');">
                            <div class="display-table">
                                <div class="vertical-align middle">
                                    <div class="content">
                                        <div class="text-container">
                                            <div class="heading"><a href="<?php echo get_the_permalink($insights_post->ID); ?>"><?php echo $insights_post->post_title; ?></a></div>
                                            <div class="insights-excerpt">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                            <div class="read-more">
                                                <a href="<?php echo get_the_permalink($insights_post->ID); ?>">Read More <img src="<?php echo bloginfo("template_directory") ?>/img/insights/read-more.png" alt="Read More Icon" /></a>
                                            </div>
                                        </div>
                                        <div class="text">
                                            <div class="tags">
                                                <?php
                                                foreach (get_the_tags($insights_post->ID) as $tag)
                                                {
                                                    echo "<a href='".get_tag_link($tag->term_id)."'>".$tag->name."</a>";
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="author-and-date">
                                                <div class="author-name"><a href="<?php echo get_the_permalink($insights_post->ID); ?>"><?php the_author(); ?></a></div>
                                                <div class="insights-date">
                                                    <?php $post_date = $insights_post->post_date; ?>
                                                    <?php echo date("d.m.Y", strtotime("$post_date")); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="share-button">
                                            <img src="<?php echo bloginfo("template_directory") ?>/img/insights/share.png" alt="Book Mark Icon" /> Share
                                        </div>
<!--                                        <div class="share-container">
                                            <a href="<?php get_the_permalink($insights_post->ID); ?>" target="_blank">
                                                <?php echo do_shortcode('[ssba]'); ?>
                                            </a>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="overlay"></div>
                        </div>
                        
                        <!--<div class="divider"></div>-->
                    </div>
                        
                    <?php } wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
//            function showFilters(showList, filterButton) {
//                $('.each-list').hide();
//                $('.' + showList).show();
//                $('.each-tab').removeClass("active");
//                $('.' + filterButton).addClass("active");
//                $('.each-tab img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-right.png");
//                $('.each-tab.active img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-down.png");
//            } 
//            function showFiltersResults(keyWord) {
//                $('.each-insight-container').addClass("inactive");
//                $('.each-insight-container.' + keyWord).fadeIn();
//                $('.each-insight-container.' + keyWord).addClass("active");
//                $('.' + keyWord).addClass("active");
//            }
            $(document).ready(function() {
//                $('.filters-open-button').click(function(){
//                    $('.filters-section').show();
//                    $('.filters-open-button').hide();
//                    $('.filters-close-button').show();
//                });
//                $('.filters-close-button').click(function(){
//                    $('.filters-section').hide();
//                    $('.filters-open-button').show();
//                    $('.filters-close-button').hide();
////                    $('.each-tab').removeClass("active");
////                    $('.each-tab.subject-areas').addClass("active");
////                    $('.each-tab img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-right.png");
//                    $('.each-tab.subject-areas img').attr("src", "<?php echo bloginfo("template_directory") ?>/img/insights/arrow-down.png");
////                    $('.each-list').css("display", "none");
////                    $('.each-list.subject-areas-list').css("display", "block");
//                });
//                $('.clear-all').click(function() {
//                    $('.each-insight-container').removeClass("inactive");
//                    $('.each-insight-container').fadeIn();
//                    $('.each-insight-container').removeClass("active");
//                    $('.each-list li').removeClass("active");
//                });
                $(".share-button").hover(function() {
                    $(".share-container").addClass("animated fadeIn");
                    $(".share-button").css("display", "none");
                }, function() {
                    $(".share-container").removeClass("animated fadeIn");
                    $(".share-button").css("display", "block");
                });
            });
        </script>
    </body>
</html>