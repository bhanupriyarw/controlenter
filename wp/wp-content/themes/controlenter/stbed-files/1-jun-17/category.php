<?php $this_page = 'blog'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        
        <title>The Serai</title>
        
        <meta name="keywords" content="Luxury, resorts, India, travel, inspire, stay, unforgettable, romantic, business, corporate affairs, wildlife, destination, holiday, weekend getaway, vacation, spa, experience, comfort, The Serai">
        <meta name="description" content="Enjoy your stay at any of our four dream destinations in India. Our inspired resorts offer luxurious experiences for business and leisure.">
        
        <meta property='og:site_name' content='The Serai' />
        <meta property="og:description" content="Enjoy your stay at any of our four dream destinations in India. Our inspired resorts offer luxurious experiences for business and leisure.">
        <meta property="og:url" content="http://www.theserai.in" />

        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="615" />
        <meta property="og:image:height" content="499" />

        <meta property="og:title" content="The Serai" />
        <meta property="og:image" content="<?php bloginfo( 'template_directory' ); ?>/img/about/banner-img.jpg" />
    
        <link type="text/css" rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/sliders/bxslider/css/jquery.bxslider.css" />

        <script type="text/javascript" src="<?php bloginfo( 'template_directory' ); ?>/sliders/bxslider/js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="<?php bloginfo( 'template_directory' ); ?>/sliders/bxslider/js/jquery.bxslider.min.js"></script>
    </head>
    
    <body>
        <?php
        
        
        $args = array(
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => 'post',
            'posts_per_page' => -1
        );
        $recent_posts = get_posts( $args );
    //    $query = new WP_Query( $args );
        $count = count($recent_posts);
//        echo '<pre>' . print_r($posts, true) . '</pre>'; die;
        
        ?>
        <?php get_header(); ?>
        <div class="inner-page blog-page all-pages">
            <div class="heading-container">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1 class="heading">The Serai Blog</h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="blog-container">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="left">
                                <div class="banner-container">
                                    <ul class="home-bxslider">
                                        <?php 
                                        foreach ($recent_posts as $recent_post) {
                                            $posttags = wp_get_post_tags($recent_post->ID);
                                        foreach ($posttags as $posttag) {
                                            if($posttag->name == "Slider" ) {
                                            ?>
                                            
                                        <li>
                                            <div class="item">
                                                <div class="slider-image">
                                                    <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($recent_post->ID), 'full'); ?>
                                                    <img src="<?php echo $featured_image_url[0]; ?>" alt="The Serai Blog" />
                                                </div>
                                                
                                                <div class="slider-content">
                                                    <div class="category-date">
                                                        <div class="slider-category">
                                                            <?php $categories = get_the_category(); ?>
                                                            <?php foreach($categories as $category) { ?>
                                                            <?php // if($category->slug != "uncategorized") { ?>
                                                                <?php echo $category->slug; ?>
                                                            <?php // } ?>
                                                            <?php } ?>
                                                        </div>
                                                        
                                                        <div class="slider-date">
                                                            <?php $post_date = $recent_post->post_date; ?>
                                                            <?php echo date("Y-m-d", strtotime("$post_date")); ?>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    
                                                    <div class="slider-title">
                                                        <?php echo $recent_post->post_title; ?>
                                                    </div>
                                                    
<!--                                                    <div class="slider-content">
                                                        <?php echo strip_tags(get_the_excerpt()); ?>
                                                    </div>-->
                                                </div>
                                            </div>
                                        </li>
                                        <?php } } } ?>
                                    </ul>
                                </div>

                                <div class="posts-container">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <?php $i = 1;   if( have_posts() ) : while (have_posts() ) : the_post(); ?>
                                            <div class="col-sm-6">
                                            <span id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                            
                                                <div class="blog-content">
                                                    <a href="<?php echo get_permalink(get_the_ID()); ?>">
                                                        <div class="blog-image">
                                                            <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
                                                            <img src="<?php echo $featured_image_url[0]; ?>" alt="The Serai Blog" />
                                                        </div>
                                                    </a>

                                                    <div class="content">
                                                        <div class="blog-title">
                                                            <a href="<?php echo get_permalink(get_the_ID()); ?>">
                                                                <?php echo $post->post_title; ?>
                                                            </a>
                                                        </div>

                                                        <div class="blog-text">
                                                            <!--We, at The Serai, aspire to please our guests, by giving them the best of luxury and hospitality. Luckily for us, it seems to be working as more of our guests love to come back to us,...-->
                                                            <?php echo strip_tags(get_the_excerpt()); ?>
                                                        </div>

                                                        <div class="blog-category">
                                                            <?php $categories = get_the_category(); ?>
                                                            <?php foreach($categories as $category) { ?>
                                                            <?php // if($category->slug != "uncategorized") { ?>
                                                                <?php echo $category->slug; ?>
                                                            <?php // } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </span>
                                                <?php
                                                    if ($i % 2 == 0) {
                                                        echo "</div></div>";
                                                        if ($count != $i) {      
                                                            echo "<div class='row'>";
                                                            $i++;
                                                            continue;
                                                        }
                                                    }
                                                ?>
                                            </div>
                                            <?php $i++;  endwhile; endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="right">
                                <div class="slider-container">
                                    <div class="caption">#INSPIREDRESORTS</div>

                                    <ul class="image-bxslider">
                                        <li>
                                            <div class="item item1"></div>
                                        </li>

                                         <li>
                                            <div class="item item2"></div>
                                        </li>

                                         <li>
                                            <div class="item item3"></div>
                                        </li>

                                         <li>
                                            <div class="item item4"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="categories">
                                    <div class="head">Categories</div>
                                    <?php $categories = get_terms('category', array('hide_empty' => 0)); ?>
                                    <?php foreach($categories as $category) { ?>
                                    <?php // if($category->slug != "uncategorized") { ?>
                                    <?php echo '<div class="category-name"> <a href="'.get_bloginfo('url').'/category/'.$category->slug.'/"> '. $category->name .'</a> </div>'; ?>
                                        <!--<div class="category-name"><a href="<?php get_bloginfo('template_directory') ?>/category/<?php echo $category->slug ?>"><?php echo $category->slug; ?></a></div>-->
                                    <?php // } ?>
                                    <?php } ?>
                                    
                                    
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div class="recent-posts">
                                    <div class="head">Recent Posts</div>
                                    <?php foreach($recent_posts as $recent_post) { ?>
                                    <a href="<?php echo get_permalink($recent_post->ID); ?>">
                                        <div class="recent-posts-content">
                                            <div class="post-image">
                                                <?php $featured_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($recent_post->ID), 'full'); ?>
                                                <img src="<?php echo $featured_image_url[0]; ?>" alt="The Serai Chikmagalur" />
                                            </div>

                                            <div class="post-title">
                                                <?php echo $recent_post->post_title; ?>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                    <?php } ?>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
            $(document).ready( function () {
                $('.date').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
                
                $('.image-bxslider').bxSlider({
                    auto: true,
                    speed: 800
                });
                
                $('.home-bxslider').bxSlider({
                    auto: true,
                    speed: 800
                });
            });
            
        </script>
    </body>
    
</html>