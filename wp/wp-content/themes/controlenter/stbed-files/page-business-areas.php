<?php $this_page = 'business-areas'; ?>
<!DOCTYPE html>
<html>
    <head>
        <?php include_once 'head.php'; ?>
        <title>Business Areas | Control Enter</title>
    </head>

    <body>
        <?php include_once 'header.php'; ?>
        
        <div class="body business-areas-page">
            <div class="section banner">
                <div class="image">
                    <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/control-enter-business-areas-banner.png" alt="About Control Enter" />
                </div>
                <div class="container">
                    <div class="content">
                        <div class="text">
                            Our experienced team has delivered world-class solutions to Global 500 companies, unicorn startups, and SMEs across multiple geographies, business functions, and industries.
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="section modes-of-management">
                <div class="container">
<!--                    <div class="introduction-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur.Lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur.Lorem ipsum.
                    </div>-->
                    <div class="heading">Modes of Engagement</div>
                    <div class="text">
                        We partner and apply our problem solving capabilities in the context of your current business need to ensure relevant results.
                    </div>
                    <div class="modes">
                        <div class="each-mode">
                            <div class="heading">
                                <div class="icon">
                                    <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/strategic-engagement.png" alt="Icon" />
                                </div>
                                <div class="text">
                                    <div class="display-table">
                                        <div class="vertical-align middle">
                                            Strategic Engagement
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <p>
                                Develop, roadmap, and implement transformational and capability-building strategies.
                                In a strategic engagement, we help your business solve big-picture needs.  Examples include:
                                <ul>
                                    <li>Assessing future changes to your customer or market and developing a complete design of a new service or product model anticipating that change.</li>
                                    <li>Evaluating a cross-functional issue that impacts multiple stakeholders and developing a full set of solutions based on robust analysis.</li>
                                    <li>Quantifying financial levers and interconnections to assess the individual and collective impact of various strategic initiatives.</li>
                                    <li>Transforming an underperforming business through a combination of measurable and structured improvements.</li>
                                    <li>Building capability and technology roadmaps to enable long-term programs.</li>
                                </ul>
                            </p>
                        </div>
                        <div class="each-mode">
                            <div class="heading">
                                <div class="icon">
                                    <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/problem-solving-engagement.png" alt="Icon" />
                                </div>
                                <div class="text">
                                    <div class="display-table">
                                        <div class="vertical-align middle">
                                            Problem Solving Engagement
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <p>
                                Analyze, build and deploy custom solutions to specific business problems, including analytical tools, processes, and trainings.
                                In a problem solving engagement, we help you resolve a more specific business need.  Examples include:
                            <ul>
                                <li>Defining new ways to predict and enhance revenue as well as the supporting technology, processes and organizational changes needed to capture value.</li>
                                <li>Evaluating the root causes behind an operational issue and deploying a mix of product, process, and organizational solutions to reduce cost.</li>
                                <li>Assessing organizational and functional efficiency and deploying process, technology, and organizational changes to drive improvement.</li>
                                <li>Performing training and change management to build capabilities.</li>
                            </ul>
                            </p>
                        </div>
                        <div class="each-mode">
                            <div class="heading">
                                <div class="icon">
                                    <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/product-engagement.png" alt="Icon" />
                                </div>
                                <div class="text">
                                    <div class="display-table">
                                        <div class="vertical-align middle">
                                            Product Engagement
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <p>
                                Deliver well-designed, highly customized digital products for repeatable analyses, work processes, and decision making.
                                In a product engagement, the work is centered on delivering a tool to embed process and decision making permanently in the business.  Examples include:
                                <ul>
                                    <li>Developing custom analytical tools for improved decision making.</li>
                                    <li>Developing custom process workflow tools for more consistent, accurate, and efficient performance.</li>
                                    <li>Developing digital tools for customer, suppliers, field staff, and other extended users.</li>
                                    <li>Developing data warehousing and supporting infrastructure to link, optimize, and enhance existing systems.</li>
                                </ul>
                            </p>
                        </div>
                        <div class="each-mode">
                            <div class="heading">
                                <div class="icon">
                                    <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/service-engagement.png" alt="Icon" />
                                </div>
                                <div class="text">
                                    <div class="display-table">
                                        <div class="vertical-align middle">
                                            Service Engagement
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <p>
                                Support ongoing activities through multi-project or continuous staff augmentation at competitive rates.
                                In a service engagement, we provide ongoing support to your business.  Examples include:
                                <ul>
                                    <li>Working on multiple interrelated projects over an extended period.</li>
                                    <li>Providing staff augmentation for general problem solving and capability building support.</li>
                                    <li>Providing ongoing back office services that incorporate analytics, technology, and process redesign to maximize effectiveness and minimize cost.</li>
                                    <li>Support tools that your organization does not wish to bring in-house.</li>
                                </ul>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="links">
                        <a href="javascript: void(0);">Talk to Us</a>
                    </div>
                    <div class="listing">
                        <div class="industries">
                            <div class="heading">Industries</div>
                            <ul>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/finance-and-insurance.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Finance and Insurance
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/government-and-ngo.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Government and NGO
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/consumer-goods.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Consumer Goods
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/healthcare-pharma-and-biotech.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Healthcare, Pharma and Biotech
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/industrial-manufacturing.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Industrial Manufacturing
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/primary-materials-and-commodities.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Primary Materials and Commodities
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/professional-services.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Professional Services
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/real-estate-and-infrastructure.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Real Estate and Infrastructure
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/retail.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Retail
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/transportation-and-logistics.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Transportation and Logistics
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/technology-media-and-telecom.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Technology, Media and Telecom
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/utilities.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Utilities
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="functions">
                            <div class="heading">Functions</div>
                            <ul>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/back-office-and-centralized-process.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Back Office and Centralized Process
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/finance.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Finance
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/human-resources.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Human Resources
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/marketing-and-sales.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Marketing and Sales
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/operations.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Operations
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/research-development-and-products.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Research Development and Products
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/strategy.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Strategy
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/supply-chain-and-sourcing.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Supply Chain and Sourcing
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/sustainability-and-engagement.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Sustainability and Engagement
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="icon">
                                        <img src="<?php echo bloginfo( "template_directory" ); ?>/img/business-areas/technology.png" alt="Icon" />
                                    </div>
                                    <div class="text">
                                        <div class="display-table">
                                            <div class="vertical-align middle">
                                                Technology
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_footer(); ?>
        
        <script type="text/javascript">
            $(document).ready(function() {
                
            });
        </script>
    </body>
</html>