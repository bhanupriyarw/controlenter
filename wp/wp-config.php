<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'controlenter_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^xo~x%$l39AExioAzQI6mw{iga490m8mHqTn+f$<X4yp2e+XZX%svQPHhXAB~(ds');
define('SECURE_AUTH_KEY',  '(l?1~f|}pYL7Er)FeZT&eGflq]`VJ2[4rm,sZ@~I17;V&JT=d*x3A*%DTs>967eu');
define('LOGGED_IN_KEY',    ';Oj!eY6s Q-{W9}@aH>+~jC]y)qI7X,gh?t;|ENkYdU{I>TZ!dn2IkB$#3)V/8pc');
define('NONCE_KEY',        '9>n~6ciWfj^l%:SdN?#ayQYzzT{7S{S=v>0}V+B$aiM2R@=/Jwq`wm8+=qB?e#B6');
define('AUTH_SALT',        'b,U.u(2}c7MaFTGh@Dbha!bf8]r6S2r/~u279J~B,a}$xoh3+^E]EAL)7HXTS7jb');
define('SECURE_AUTH_SALT', 'tO]55szS[HxYm1cUK=;!WCl~lMvihb.Hm|9DR=2ypjnL!d9OW7~B1V8#/*+H+b`4');
define('LOGGED_IN_SALT',   'd d8O!J <4a{Q}L5s#`cQ2G=azc=g=#I&J2K.@2CuZdH&BQQEV5>`(IF{br&Q.=.');
define('NONCE_SALT',       '+-)/<NVka|G`O@/zWUmJm_c[^UtXeQ=u?$F-:GCJ?E{Ug(X?Gn`Nf}7|9CZ0fi9)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
